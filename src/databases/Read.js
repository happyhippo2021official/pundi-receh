import { onSnapshot, collection,query,where, doc, getDocs, orderBy, limit, documentId} from 'firebase/firestore';
import React, { useState, useEffect } from 'react';
import md5 from 'md5'
import db from './firebase'


function ReadMemberUser (){
    const name = []
    onSnapshot(collection(db,process.env.REACT_APP_USERMEMBERSHIPDB),(snapshot)=>{
        snapshot.docs.map((doc)=>{
            name.push(doc.data())
            // console.log(doc.data())
        })
    })
    return name
}

async function CheckLogin(param){
    const q = query(collection(db,process.env.REACT_APP_USERMEMBERSHIPDB),where('email','==',param.email))
    const docSnap = await getDocs(q)
    let result = false
    docSnap.forEach((doc)=>{
        if(doc.get('password')===md5(param.password)){
            result = doc.data()
        }
    })
    return (result!==false?result:null)
}

async function CheckLoginAdmin(param){
    const q = query(collection(db,process.env.REACT_APP_ADMINMEMBERSHIPDB),where('username','==',param.username),where('password','==',md5(param.password)), where('active','==',true))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        docSnap.forEach((doc)=>{
            result = doc.data()
        })
        return result
    }else{
        return null
    }
}

async function GetMonthlyAnalysis(param){
    const q = query(collection(db,process.env.REACT_APP_MONTHLYANALYSISDB))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        let arrResult = [];
        docSnap.forEach((doc)=>{
            const data = doc.data()
            const id = doc.id
            result = {...data,id}
            // result = doc.data()
            arrResult.push(result);
        })
        return arrResult
    }else{
        return ['a']
    }
}

async function GetOneMAnalysis(param){
    const q = query(collection(db,process.env.REACT_APP_MONTHLYANALYSISDB))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        let arrResult = [];
        docSnap.forEach((doc)=>{
            const data = doc.data()
            const id = doc.id
            if(id==param){
                result = {...data,id}
                // result = doc.data()      
                arrResult.push(result);
            }
        })
        return arrResult
    }else{
        return []
    }
}

async function GetWeeklyAnalysis(param){
    const q = query(collection(db,process.env.REACT_APP_WEEKLYANALYSISDB))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        let arrResult = [];
        docSnap.forEach((doc)=>{
            const data = doc.data()
            const id = doc.id
            result = {...data,id}
            // result = doc.data()
            arrResult.push(result);
        })
        return arrResult
    }else{
        return ['a']
    }
}

async function GetAllPods(){
    const q = query(collection(db,'PundiPods'), orderBy('timestamp','desc'))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        let arrResult = [];
        docSnap.forEach((doc)=>{
            const data = doc.data()
            const id = doc.id
            // console.log(doc.id)
            result = {...data,id}
            // result = doc.data()
            arrResult.push(result);
        })
        return arrResult
    }else{
        return []
    }
}

async function GetAllComics(){
    const q = query(collection(db,'PundiComic'), orderBy('timestamp','desc'))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        let arrResult = [];
        docSnap.forEach((doc)=>{
            const data = doc.data()
            const id = doc.id
            // console.log(doc.id)
            result = {...data,id}
            // result = doc.data()
            arrResult.push(result);
        })
        return arrResult
    }else{
        return []
    }
}

async function GetBlog(id){
    const q = query(collection(db,'PundiBlog'),where(documentId(),'==',id))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        let arrResult = [];
        docSnap.forEach((doc)=>{
            const data = doc.data()
            const id = doc.id
            result = {...data,id}
            // result = doc.data()
            arrResult.push(result);
        })
        return arrResult[0]
    }else{
        return []
    }
}

async function GetAllBlogs(){
    const q = query(collection(db,'PundiBlog'), orderBy('timestamp','desc'))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        let arrResult = [];
        docSnap.forEach((doc)=>{
            const data = doc.data()
            const id = doc.id
            // console.log(doc.id)
            result = {...data,id}
            arrResult.push(result)
        })
        return arrResult
    }else{
        return []
    }
}

async function GetOneAnalysis(param){
    const q = query(collection(db,process.env.REACT_APP_WEEKLYANALYSISDB))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        let arrResult = [];
        docSnap.forEach((doc)=>{
            const data = doc.data()
            const id = doc.id
            if(id===param){
                result = {...data,id}
                // result = doc.data()
                arrResult.push(result);
            }
        })
        return arrResult
    }else{
        return []
    }
}

async function GetAdminLevel(){
    const q = query(collection(db,'AdminLevel'))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        let arrResult = [];
        docSnap.forEach((doc)=>{
            const data = doc.data()
            const id = doc.id
            result = {...data,id}
            // result = doc.data()
            arrResult.push(result);
        })
        return arrResult
    }else{
        return []
    }
}

async function GetAllAdmin(){
    const q = query(collection(db,'MembersAdminData'))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        let arrResult = [];
        docSnap.forEach((doc)=>{
            const data = doc.data()
            const id = doc.id
            result = {...data,id}
            // result = doc.data()
            arrResult.push(result);
        })
        return arrResult
    }else{
        return []
    }
}

async function GetAllUsers(){
    const q = query(collection(db,'MembersUserData'))
    const docSnap = await getDocs(q)
    if(docSnap){
        let result = false
        let arrResult = [];
        docSnap.forEach((doc)=>{
            const data = doc.data()
            const id = doc.id
            result = {...data,id}
            // result = doc.data()
            arrResult.push(result);
        })
        return arrResult
    }else{
        return []
    }
}



export {
    ReadMemberUser, 
    CheckLogin, 
    CheckLoginAdmin, 
    GetWeeklyAnalysis,
    GetMonthlyAnalysis, 
    GetOneAnalysis, 
    GetOneMAnalysis,
    GetAllBlogs,
    GetAllComics, 
    GetAllPods, 
    GetAdminLevel,
    GetAllAdmin,
    GetAllUsers,
    GetBlog
}