const MemberUser = {
    email:'',
    verified:false,
    level:'NZF',
    password:'',
    status:'active',
    username:{
        first:'',
        middle:'',
        last:''
    }
}

export {MemberUser}