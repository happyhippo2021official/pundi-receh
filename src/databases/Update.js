import { onSnapshot, collection,query,where, doc, getDocs, setDoc,serverTimestamp} from 'firebase/firestore';
import React, { useState, useEffect } from 'react';
import md5 from 'md5'
import db from './firebase'

async function UpdateAnalysis(id,data){
    console.log(doc(db,'WeeklyAnalysis',id))
    const docRef = await setDoc(doc(db,'WeeklyAnalysis',id),data)
    return docRef
}

async function ActivateAdmin(id,data){
    console.log(doc(db,'MembersAdminData',id))
    const docRef = await setDoc(doc(db,'MembersAdminData',id),data)
    return docRef
}

async function UpdateUserData(id,data){
    console.log(doc(db,'MembersUserData',id))
    const docRef = await setDoc(doc(db,'MembersUserData',id),data)
    return docRef
}

async function UpdateBlog(id,data){
    console.log(doc(db,'PundiBlog',id))
    const docRef = await setDoc(doc(db,'PundiBlog',id),data)
    return docRef
}



export {UpdateAnalysis, UpdateBlog, ActivateAdmin, UpdateUserData}