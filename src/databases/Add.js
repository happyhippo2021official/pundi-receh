import { onSnapshot, collection, doc, setDoc, addDoc, serverTimestamp } from 'firebase/firestore';
import React, { useState, useEffect } from 'react';
import db, {storage} from './firebase'
import {ref, uploadBytes, getDownloadURL} from 'firebase/storage'
import {v4} from 'uuid'
import md5 from 'md5'

const MemberUser = {
    email:'',
    level:'NZF',
    status:'active',
    password:'',
    username:{
        firstname:'',
        middlename:'',
        lastname:''
    }
}

async function AddMember2(){
    const docRef = await addDoc(collection(db,process.env.REACT_APP_USERMEMBERSHIPDB),{
        email:'aaa',
        level:'NZF',
        status:'active',
        password:'aaa',
        username:{
            firstname:'aaa',
            middlename:'aaa',
            lastname:'aaa'
        }
    })
    return docRef
}

const AddMemberUser = async (obj)=>{
    MemberUser.email=obj.email
    MemberUser.password=md5(obj.password)
    MemberUser.username.firstname=obj.firstname
    MemberUser.username.lastname=obj.lastname
    obj.timestamp = serverTimestamp()
    console.log(MemberUser)
    const docRef = await addDoc(collection(db,process.env.REACT_APP_USERMEMBERSHIPDB),MemberUser)
    return docRef
}


// const AddMemberUser = async (email, pass,first,mid,last) => {
//     // edit memberuser
//     MemberUser.email='Test@test.com'
//     MemberUser.pass='jakljaklja'
//     MemberUser.username.firstname='lala'
//     MemberUser.username.middlename='lili'
//     MemberUser.username.lastname='lolo'
//     console.log(MemberUser)
//     const docRef = doc(db,'MembersUserData')
//     const result = setDoc(docRef, MemberUser)
//     return result
// }

const addWeeklyAnalysis = async (obj)=>{
    console.log(obj)
    obj.timestamp = serverTimestamp()
    const docRef = await addDoc(collection(db,'WeeklyAnalysis'),obj)
    return docRef
}

const addMonthlyAnalysis = async (obj)=>{
    console.log(obj)
    obj.timestamp = serverTimestamp()
    const docRef = await addDoc(collection(db,process.env.REACT_APP_MONTHLYANALYSISDB),obj)
    return docRef
}

const addBlog = async (obj)=>{
    console.log(obj)
    obj.timestamp = serverTimestamp()
    const docRef = await addDoc(collection(db,'PundiBlog'),obj)
    return docRef
}

async function addImage(image,path){
    const iRef = ref(storage, `images/${path}/${v4()}`)
    const response = await uploadBytes(iRef, image)
    const url = await getDownloadURL(response.ref)
    return url
}

const addComic = async (obj)=>{
    obj.timestamp = serverTimestamp()
    const docRef = await addDoc(collection(db,'PundiComic'),obj)
    return docRef
}

const addPods = async (obj)=>{
    obj.timestamp = serverTimestamp()
    const docRef = await addDoc(collection(db,'PundiPods'),obj)
    return docRef
}

const addAdmin = async (obj)=>{
    const newObj = {...obj,
        password:md5(obj.password),
        timestamp: serverTimestamp()
    }
    console.log(newObj)
    const docRef = await addDoc(collection(db,'MembersAdminData'),newObj)
    return docRef
}





export {AddMemberUser, AddMember2, addBlog, addWeeklyAnalysis, addMonthlyAnalysis, addImage, addComic, addPods, addAdmin}