import { onSnapshot, collection,query,where, doc, deleteDoc, setDoc} from 'firebase/firestore';
import React, { useState, useEffect } from 'react';
import md5 from 'md5'
import db from './firebase'

async function DeleteWeeklyAnalysis(id){
    
    const docRef = await deleteDoc(doc(db,'WeeklyAnalysis',id))
    return docRef
}

async function deletePods(id){
    
    const docRef = await deleteDoc(doc(db,'PundiPods',id))
    return docRef
}

async function deleteBlog(id){
    
    const docRef = await deleteDoc(doc(db,'PundiBlog',id))
    return docRef
}

async function deleteAdmin(id){
    
    const docRef = await deleteDoc(doc(db,'MembersAdminData',id))
    return docRef
}

async function deleteComic(id){
    
    const docRef = await deleteDoc(doc(db,'PundiComic',id))
    return docRef
}



export {DeleteWeeklyAnalysis, deletePods, deleteBlog, deleteComic, deleteAdmin}