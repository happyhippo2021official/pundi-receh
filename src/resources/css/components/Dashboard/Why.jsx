import { makeStyles } from '@material-ui/core/styles'
import color from '../../Color'


const useStyles = makeStyles((theme)=>{
    return{
        textContainer:{
            fontSize:'3.125rem',
            color: color.grey,
            padding:'0px 20px'
        },
        name:{
            position:'relative',
            top:'-1vh',
            height:'75px',
            aspectRatio:'1:1',
        },
        dataTextContainer:{
            margin:'8vh 0',
        },
        text:{
            color:color.goldyellow,
            fontSize:'1.875rem'
        },
        subText:{
            fontSize:'2vmin',
            color: color.grey
        }
    }
})

export default useStyles;