import { makeStyles } from '@material-ui/core/styles'
import color from '../../Color'


const useStyles = makeStyles((theme)=>{
    return{
        title:{
            fontSize:'3.125rem',
            color:color.goldyellow
        },
        subtitle:{
            fontSize:'1.5rem',
            color:color.fontGrey
        },
        formContainer:{
            padding:'5vh 2vw',
            color:color.white
        },
        pictContainer:{
            height:'100%',
            maxHeight:'50vh',
            position:'relative',
        },
        laptopPict:{
            position:'relative',
            left:'-5vw',
            height:'100%',
            maxHeight:'50vh',
            aspectRatio: '1:1',
        },
        hpPict:{
            position:'relative',
            top:'-35vh',
            left:'-7vw',
            height:'100%',
            maxHeight:'40vh',
            aspectRatio: '1:1',
        },
        itemWrap:{
            color:color.goldyellow,
            fontSize:'2em'
        },
        formItem:{
            margin:'2vh 0',
            color:color.darkGrey,
            minHeight:'20px',
            height:'5vh',
            width:'100%',
            fontSize:'2em'

        },
        button:{
            color:color.goldyellow,
            margin:'2vh 0',
            fontSize:'1.5rem',
            transition:'all .2s ease-in-out',
            cursor: 'pointer',
            '&:hover':{
                transition:'all .2s ease-in-out',
                color:color.brightGreen
            }
        },
        textbox:{

        }
    }
})

export default useStyles;