import { makeStyles } from '@material-ui/core/styles'
import color from '../../Color'


const useStyles = makeStyles((theme)=>{
    return{
        title:{
            color: color.goldyellow,
            fontSize:'10vmin'
        },
        subtitle:{
            fontSize:'5vmin'
        },
        points:{
            margin:'5vh'
        },
        point:{
            height:'100%',
        },
        iconContainer:{
            margin:'2vh 0'
        },
        iconCircle:{
            height:'10vh',
            width:'10vh',
            backgroundColor:color.hotRed,
            borderRadius:'50%'
        },
        icon:{
            height:'6vmin',
            width:'6vmin',
            color:color.white
        },
        pointsTitle:{
            fontSize:'4vmin',
            color:color.goldyellow,
            textAlign:'center'
        },
        pointsSubtitle:{
            fontSize:'2vmin',
            textAlign:'center'
        },
        button:{
            backgroundColor:color.hotRed,
            borderRadius:'10px',
            color:color.white,
            padding:'3vmin',
            minWidth:'15vw',
            fontSize:'2.5vmin',
            width:'auto',
            cursor:'pointer'
        }
    }
})

export default useStyles;