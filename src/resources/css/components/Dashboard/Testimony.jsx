import { makeStyles } from '@material-ui/core/styles'
import color from '../../Color'


const useStyles = makeStyles((theme)=>{
    return{
        container:{
            padding: '5vh 15%',
        },
        title:{
            color:color.goldyellow,
            fontSize:'3rem',
            textTransform: 'uppercase',
        },
        subtitle:{
            fontSize:'1.25rem',
        },
        items:{
            padding:' 2vh 0',
        },
        boxOuter:{
            padding:'1vh 1vw',
            height:'100%'
        },
        box:{
            padding:'1vh 1vw',
            backgroundColor: color.darkgrey26,
            borderRadius:'2vw',
            boxShadow: `0px 5px ${color.darkGrey}`,
            color: color.grey,
            minHeight:'200px',
        },
        content:{
            padding:'.5vh 0',
        },
        pictContainer:{
            padding:'1vh 1vw',
        },
        pict:{
            maxWidth:'100%',
            aspectRatio:'1/1',
            borderRadius:'50%',
            borderStyle:'solid',
            borderWidth:'1px',
        },
        name:{
            color:color.goldyellow,

        },
        status:{
            color:color.goldyellow,
        },
        text:{
            padding:' 1vh 0',
            fontSize:'2vmin'
        }
    }
})

export default useStyles;