import { makeStyles } from '@material-ui/core/styles'
import color from '../../Color'


const useStyles = makeStyles((theme)=>{
    return{
        container:{
            padding: '5vh 15%',
            color:color.grey
        },
        title:{
            color:color.goldyellow,
            fontSize:'3rem',
            textTransform: 'uppercase',
        },
        subtitle:{
            fontSize:'1.5rem',
        },
        items:{
            padding:'2vh 0',
        },
        spacing:{
            padding:'0 1vw 2vh 0',
        },
        box:{
            padding:'2vh 1vw',
            backgroundColor: color.darkgrey26,
            borderRadius:'2vw',
            boxShadow: '0px 5px #888888',

        },
        texts:{
            padding:'.5vh 1vw'
        },
        itemtitle:{
            fontSize:'1.5rem',
            fontWeight:700,
        },
        itemvalue:{
            fontSize:'3rem',
            color:color.goldyellow,
        },
        itemdetails:{
            fontSize:'1rem',
            fontWeight:300,
        },
        footertext:{
            textAlign:'center',
            fontSize:'1.25rem',
        }
    }
})

export default useStyles;