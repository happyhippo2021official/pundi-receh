import { makeStyles } from '@material-ui/core/styles'
import color from '../../../resources/css/Color'


const useStyles = makeStyles((theme)=>{
    return{
        bold1:{
            fontSize:'2em',
            color:color.goldyellow,
            paddingBottom:'1vmin',
            fontWeight:'bold'
        },
        bold2:{
            fontSize:'1.3em',
            paddingBottom:'1vmin',
            fontWeight:'400'
        },
        image:{
            maxWidth: '80vw',
            paddingBottom:'2vmin'
        },
        paragraph:{
            fontSize:'1em',
            paddingBottom:'1vmin'
        },

    }
})

export default useStyles;