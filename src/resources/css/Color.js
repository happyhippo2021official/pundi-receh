import { grey } from '@material-ui/core/colors'

const color = {
    hotRed:'#980C0C',
    white:'#FFFFFF',
    darkGrey:'#3C3A3A',
    black:'#000000',
    goldyellow: '#E5AE40',
    lightGrey:'#c8c8c8',
    darkgrey26:'#262626',
    grey:'#b5b5b5',
    fontGrey:'#9696A0',
    lightGreyRGBA06:'rgba(190, 190, 190, 0.6)',
    brightGreen:'#00c224',
    brightblue:'#0096FF',
}

export default color;