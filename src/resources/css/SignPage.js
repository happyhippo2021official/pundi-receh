import { makeStyles } from '@material-ui/core/styles'
import color from './Color'


const useStyles = makeStyles((theme)=>{
    return {
        outsideContainer:{
            padding:'2vmin'
        },
        warnings:{
            fontSize:'.8em',
            backgroundColor:color.lightGreyRGBA06,
            color:color.hotRed
        },
        signupButton:{
            marginTop:'2vmin'
        }
    }
})

export default useStyles;