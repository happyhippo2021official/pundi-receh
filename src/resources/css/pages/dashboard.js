import { makeStyles } from '@material-ui/core/styles'
import color from '../Color'


const useStyles = makeStyles((theme)=>{
    return{
        dashboardContainer:{
            backgroundColor:color.black,
            color: color.white,
            fontSize:'2vmin'
        },
        sectionContainer:{
            padding:'10vh 0'
        }
    }
})

export default useStyles;