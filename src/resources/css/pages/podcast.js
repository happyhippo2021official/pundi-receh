import { makeStyles } from '@material-ui/core/styles'
import color from '../Color'


const useStyles = makeStyles((theme)=>{
    return{
        container:{
            color:color.white,
            whiteSpace:'pre-line',
            cursor:'default',
            minHeight:'50vh'
        },
        title:{
            fontSize:'10vmin',
            margin:'2vmin 0'
        },
        intro:{
            color:color.fontGrey,
            fontSize:'2vmin',
            textAlign: 'center'
        },



        itemContainer:{
            // height:'100%',
            margin:'5vh 0',
            color:color.white,
        },
        heightContainer:{
            height:'100%'
        },
        dateContainer:{
            // height:'100%',
            padding:'2vmin',
            width:'100%',
            aspectRatio:'1/1',
            // backgroundColor:color.brightGreen,
            borderColor: color.brightGreen,
            borderRadius:'50%',
            borderStyle:'solid',
            borderWidth: '2px'
        },
        dateDate:{
            fontSize:'8vmin',
            // fontWeight:700,
        },
        dateMonth:{
            margin:'0',
            fontWeight:400
        },
        titleText:{
            padding:'2vmin',
            fontSize:'2.5vmin',
        },
        linkText:{
            margin:'-2vh 0 0 0',
            padding:'2vmin',
            fontSize:'1.75vmin',
        },
        link:{
            textDecoration:'none',
            color:color.white,
            cursor:'pointer',
            transition: 'all .25s ease-in-out',
            '&:hover':{
                color:color.brightGreen,
                transition: 'all .25s ease-in-out',
            }
        }
    }
})

export default useStyles;