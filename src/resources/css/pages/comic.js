import { makeStyles } from '@material-ui/core/styles'
import color from '../Color'


const useStyles = makeStyles((theme)=>{
    return{
        container:{
            color:color.white,
            whiteSpace:'pre-line',
            cursor:'default',
            minHeight:'50vh'
        },
        title:{
            fontSize:'10vmin',
            margin:'2vmin 0'
        },
        intro:{
            color:color.fontGrey,
            fontSize:'3vmin',
            textAlign: 'center',
            margin:'0 0 10vmin 0'
        },
        oneItem:{
            margin:'10px 0'
        },
        pict:{
            width:'100%',
            aspectRatio: '1/1',
            maxWidth:'400px',
        },
        itemTitle:{
            height:'100%',
            padding:'20px 0'
        },
        line:{
            borderStyle:'solid',
            borderWidth:'0 0 2px 0',
            borderColor:color.white
        }
    }
})

export default useStyles;