import { makeStyles } from '@material-ui/core/styles'
import color from './Color'


const useStyles = makeStyles((theme)=>{
    return {
        headerStyle:{
            minHeight:'8vh',
            '&.scrolled':{
                opacity: '0',
                transition: 'all 1s ease',
                '&:hover':{
                    opacity: '1',
                }
            },
            background: color.darkGrey
        },
        effect:{
            transition: 'all .5s ease-out'
        },
        signButton:{
            minHeight:'6vh',
            backgroundColor:color.hotRed,
            cursor:'pointer',
            color:color.white,
            borderRadius:'5px',
            boxShadow: '3px 3px 5px black'
        },
        headMenuContainer:{

        },
        menuButton:{
            minHeight:'6vh',
            backgroundColor:color.darkGrey,
            cursor:'pointer',
            color:color.white,
            borderRadius:'5px',
            boxShadow: '3px 3px 5px black'
        },
        itemMenuButton:{
            minHeight:'6vh',
            backgroundColor:color.darkGrey,
            cursor:'pointer',
            color:color.white,
            borderStyle:'solid',
            borderColor:color.black,
            borderWidth:'0 0 2px 0'
        },
        headMenus:{
            position:'relative'
        },
        itemMenusClosed:{
            top:'0px',
            position:'absolute',
            zindex:'1',
            opacity:0,
            pointerEvents:'none',
            maxWidth:'90%'
        },
        itemMenusOpened:{
            top:'8vh',
            position:'absolute',
            zindex:'1',
            opacity:1,
            pointerEvents:'auto',
            maxWidth:'90%'
        },
        imageLogo:{
            height: '100%',
            width: '75px',
            margin: '5px 10px 5px 10px',
        },
    }
})

export default useStyles;