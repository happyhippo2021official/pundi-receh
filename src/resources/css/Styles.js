import { makeStyles } from '@material-ui/core/styles'
import { grey } from '@material-ui/core/colors'
import color from './Color'


import globalBG from '../images/BG2.png'
import topBG from '../images/topBG.jpg'
import toppict from '../images/toppict.png'
import toppict2 from '../images/topPlaceholder.jpg'
import signpageimg from '../images/login_bg.jpg'


const useStyles = makeStyles((theme)=>{
    return {
        '@global': {
            '*::-webkit-scrollbar': {
                width: '0.2em'
            },
            '*::-webkit-scrollbar-track': {
                backgroundColor: 'white'
            },
            '*::-webkit-scrollbar-thumb': {
                backgroundColor: 'black'
            }
        },
        headerStyle:{
            minHeight:'8vh',
            '&.scrolled':{
                opacity: '0',
                transition: 'all 1s ease',
                '&:hover':{
                    opacity: '1',
                }
            },
            background: grey[900]
        },
        headerButtonGroupBig:{
            [theme.breakpoints.down('sm')]: {
                display: 'none',
            },
        },
        mobileMenuItem:{
            color: 'black',
            width: '20vw',
            minWidth: '200px',
            maxWidth: '350px',
            height: '10vh',
        },
        imageLogo:{
            height: '100%',
            width: '75px',
            margin: '5px 10px 5px 10px',
        },
        toolBarTitle:{
            flexGrow:1,
        },
        success:{
            backgroundColor: theme.palette.success.main,
            color: theme.palette.success.color,
            '&:hover':{
                backgroundColor: theme.palette.success.hover
            }
        },
        root: {
            transition: "transform 0.15s ease-in-out",
        },
        gradientBackground:{
            background: 'linear-gradient(to right bottom, #63004c, #310142 20%, #0c0036 50% ,#13002b)',
        },
        lighterBackground:{
            paddingTop: '20px',
            paddingBottom: '20px',
            background: '#1f1f1f',
        },
        darkerBackground:{
            paddingTop: '20px',
            paddingBottom: '20px',
            background: '#141414',
        },
        topLayerEffect:{
            maxWidth: '100vw',
            color: grey[50],
            fontSize:'2.5vmin'
        },
        aboveFooter:{
            maxWidth: '100vw',
            backgroundColor:grey[600],
            color: grey[200]
        },
        topInput:{
            background: 'white',
        },
        fullContainer: {
            width: '100vw',
            backgroundColor: 'white',
            margin: '0',
            color:'black',
            minHeight: '80vh',
        },
        sectionContainer:{
            width: '100vw',
            minHeight: '30vh',
            backgroundColor: 'white',
            margin: '0',
        },
        imgSection:{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        },
        textSection:{
            // backgroundColor: 'green',
        },
        cardMedia:{
            height: 'auto',
            maxHeight: '80%',
            minHeight: '20vh',
            maxWidth: '100%',
        },
        cardItemMedia: {
            height: '80%',
            width: 'auto',
            maxHeight: '100px',
        },
        potentialContainer:{
            height: '20vh',
            maxHeight: '150px',
            backgroundColor: 'white',
            color: 'black',
            boxShadow: '12px 12px 2px 1px rgb(0 0 0 / 4%)'
        },
        fullWide: {
            width : '100%',
            height : '100%'
        },
        fullWidth: {
            width : '100%',
            wordBreak : 'break-word'
        },
        footer: {
            paddingTop: '20px',
            borderStyle: 'solid none none none',
            borderColor: grey[400],
            backgroundColor:grey[900],
            color: grey[50],
            minHeight:'30vh'
        },
        copyright: {
            color: 'white',
            margin: '20px 0px',
            textAlign: 'center',
        },
        signOutsideContainer: {
            minHeight: '55vh',
            backgroundImage: `url(${signpageimg})`,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
        },
        signContainer:{
            minHeight:  '55vh'
        },
        signFormBackground:{
            marginTop:'2vmin',
            backgroundColor: 'rgba(255,255,255,0.8)',
        },
        gradientButtonStyle1:{
            border: '5px solid #999999',
            borderRadius: '5px',
            borderImage: 'linear-gradient(to right bottom, #03fc73, #03c6fc)',
            borderImageSlice: '1',
            background: 'linear-gradient(to right bottom, #2d1240, #240422)',
        },
        gradientButtonStyle2:{
            border: '5px solid #999999',
            borderRadius: '5px',
            borderImage: 'linear-gradient(to right bottom, #c502d6, #f28ded)',
            borderImageSlice: '1',
            background: 'linear-gradient(to right bottom, #2d1240, #240422)',
        },
        gradientText:{
            backgroundColor: '#999999',
            backgroundSize: '100%',
            WebkitBackgroundClip: 'text',
            MozBackgroundClip: 'text',
            WebkitTextFillColor: 'transparent',
            MozTextFillColor: 'transparent'
        },
        gradientTextColor1:{
            backgroundImage: 'linear-gradient(to right bottom, #03fc73, #03c6fc)',
        },
        gradientTextColor2:{
            backgroundImage: 'linear-gradient(to right bottom, #c502d6, #f28ded)',
        },
        gradientTextColor3:{
            backgroundImage: 'linear-gradient(to right bottom, #e0c56e, #ffffff, #d4af37, #b08f26, #e0c56e)',
        },
        subTitle:{
        },
        aboutOutsideContainer: {
            minHeight: '55vh',
        },
        heatmapOutsideContainer: {
            minHeight: '55vh',
        },
        outsideContainer: {
            minHeight: '55vh',
            '&.big':{
                minHeight: '70vh'
            }
        },
        aboutPictContainer:{
            height: '50vh',
            minHeight: '150px',
            maxHeight: '300px',
        },
        aboutPict:{
            height:'100%',
            borderRadius:'15px'
        },
        LatoHeadline:{
            fontFamily: 'LatoBold',
        },
        Roboto:{
            fontFamily: 'Roboto !important',
        },
        topSectionContainer:{
            minHeight: '85vh',
            backgroundImage:`url(${topBG})`,
            backgroundSize:'cover',
        },
        topSectionPictureContainer:{
            position:'relative'
        },
        laptopPict:{
            maxHeight:'50vh',
            transform:'scale(1.5)'
        },
        phonePictContainer:{
            maxHeight:'45vh',
            position:'absolute'
        },
        phonePict:{
            
        },

        headerRowBG1: {
            background: color.black,
            border: 'solid 2px #c0c56e'
        },


        // new
        globalContainer:{
            backgroundColor:color.black
        },

        textColor1: {
            color:color.grey
        },

        followContainer: {
            height:'100%',
            width:'100%'
        },

        planContainer: {
            width: '2em',
            height: '2em'
        },
        featuresPlanItem: {
            margin: '0.2em'
        },
        middleVert: {
            display: 'flex',
            height: '100%',
            alignItems: 'center'
        },
        spacingImageTextRow: {
            margin: '0em 0em 0em 0.2em'
        },
        itemDistance: {
            margin: '0.5em 0em'
        },
        capitalized: {
            textTransform:'capitalize'
        },
        myInputBox: {
            padding: '1em',
            background: '#FFFFFF'
        },
        packageWrapper: {
            width:'100%',
            display: 'flex',
            justifyContent: 'space-between'
        },
        wrapperSubscriptions: {
            backgroundColor:"#F2E4D9",
            border: 'solid 2px #c0c56e',
            display: 'flex',
            flexDirection: 'column',
            margin: '1em 0em',
            position: 'relative'
        },
        paypalItemSubscription: {
        },
        paypalSubscriptionItem_name: {
            display: 'flex',
            alignContent: 'center',
            justifyContent: 'center',
            fontSize: '1em',
            margin: '0.3em',
            padding: '1.3em 0em',
            textAlign: 'center'
        },
        paypalSubscriptionItem_price: {
            width: 'fit-content',
            margin: '0 auto',
            fontSize: '1em',
            background: 'rgba(0,0,0,0.75)',
            marginTop: '-1.3em',
            padding: '0.3em 2em',
            color: '#FFFFFF'
        },
        paypalSubscriptionItemAccess: {
            width: 'fit-content',
            margin: '1em auto',
            fontSize: '1em',
            display: 'flex',
            alignItems: 'center'
        },
        paypalSubscriptionItem_access_availability: {
            width: "2em",
            height: "2em",
            marginRight: '1.5em',
            color: '#000000'
        },
        paypalSubscriptionItem_access_text: {
            color: '#000000',
            fontSize: '1.3em',
            width: '7em'
        },
        subscriptionBtn: {
            background: '#D7E1D5',
            width: 'calc(100% - 10px)',
            height: '4em',
            color: 'black',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            fontSize: '1.3em',
            border: 'solid 5px #FFFFFF'

        },
        paypalSubscriptionItemLink: {
            width: '100%',
            height: '5.5em',
            display: 'flex',
            alignItems: 'flex-end',
            justifyContent: 'center',
            background: '#D7E1D5'
        }
    }

})

export default useStyles;