import { makeStyles } from '@material-ui/core/styles'
import color from '../Color'


const useStyles = makeStyles((theme)=>{
    return {
        outsideContainer:{
            minHeight:'20vh',
            backgroundColor:color.black,
            color: color.white,
            margin:'3vmin 0'
        },
        sectionContainer:{
            padding:'0 5vw',
        },
        partSections:{
            padding:'1vh 0',
        },
        pict:{
            height:'5vmin',
            minHeight:'50px',
            aspectRatio: '1:1',
            margin:'0px 5px'
        },
        headPart:{
            fontSize:'1.5rem',
            color:color.goldyellow
        },
        textPart:{
            fontSize:'.875rem',
            margin:'1vmin 1vmin 0 0',
        },
        center:{
            textAlign:'center'
        },
        right:{
            textAlign:'right'
        },
        copyright:{
            fontSize:'.75rem',
            margin:'1vh 0'
        },
        link:{
            color:`${color.brightGreen} !important`,
            textDecoration:'none',
        },
        footlink:{
            margin:'1vmin 1vmin 0 0',
            color: color.white,
            transition: 'all .5s ease-in-out',
            '&:hover':{
                color: color.brightGreen,
                transition: 'all .5s ease-in-out',
            }
        }
    }
})

export default useStyles;