import { makeStyles } from '@material-ui/core/styles'
import color from '../Color'


const useStyles = makeStyles((theme)=>{
    return {
        headerContainer:{
            minHeight:'15vh',
            backgroundColor:color.black,
            padding:'0 2vw',
            position:'relative',
            top:'0px',
            color:color.white
        },
        logoNameContainer:{
            color: color.goldyellow,
        },
        logoContainer:{
            margin:'0 1vw'
        },
        logo:{
            height: '10vh'
        }, 
        name:{
            height:'5vh',
        },
        menuButtonsContainer:{
            height:'15vh',
            color:color.goldyellow
        },
        menuButton:{
            padding:'0 1vw',
            position: 'relative',
        },
        menuButtonText:{
            cursor: 'pointer',
        },
        subMenuContainer:{
            position: 'absolute',
            width:'10vw',
            top:'6vh',
            left:'-1vw',
        },
        subMenuPanel:{
            cursor: 'pointer',
            position: 'relative',
            backgroundColor:color.black,
            borderRadius: '5px',
            boxShadow:`3px 3px 15px ${color.lightGrey}`,
            zIndex:1,
        },
        subMenuItem:{
            padding:'1vh 2vw',
            borderBottom: '1px solid',
            borderColor:color.lightGrey,
            '&:hover':{
                backgroundColor: color.lightGrey
            }
        },
        menuItem:{
            color:color.goldyellow,
            width:'100%',
        },
        topMenu:{
            borderTop: '0px solid',
            borderRadius:'5px 5px 0 0 '
        },
        botMenu:{
            borderBottom: '0px solid',
            borderRadius:'0 0 5px 5px'
        },
        signLink:{
            color:`${color.brightGreen} !important`
        },
        show:{
            opacity:1
        },
        hide:{
            opacity:0,
            pointerEvents: 'none',
        }

    }
})

export default useStyles;