import { makeStyles } from '@material-ui/core/styles'
import color from './Color'


const useStyles = makeStyles((theme)=>{
    return {
        headerHeight:{
            height:'8vh'
        },
        imageLogo:{
            height:'7vh',
        },
        menuWidthin:{
            width:'14%'
        },
        menuWidthout:{
            width:'16%'
        }
    }
})

export default useStyles;