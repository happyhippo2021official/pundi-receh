import React from 'react'
import {Grid } from '@material-ui/core'
import useStyle from '../resources/css/pages/dashboard'

import Welcome from '../components/Dashboard/Welcome'
import Why from '../components/Dashboard/Why'
import Performance from '../components/Dashboard/Performance'
import Testimony from '../components/Dashboard/Testimony'
import Potential from '../components/Dashboard/Potential'
import Cookies from 'universal-cookie';


export default function Dashboard() {
    const cookies = new Cookies()
    const logindicator = cookies.get('Loggedin')
    console.log(logindicator)
    const classes = useStyle()
    return (
        <Grid container justifyContent='center' className={classes.dashboardContainer}>
            {cookies.get('Loggedin')?null:(
                <Grid item xs={11}>
                    <Grid container className={classes.sectionContainer}>
                        <Welcome />
                    </Grid>
                </Grid>
            )}
            <Grid item xs={11}>
                <Grid container className={classes.sectionContainer}>
                    <Why />
                </Grid>
            </Grid>
            {/* <Grid item xs={11}>
                <Grid container className={classes.sectionContainer}>
                    <Performance />
                </Grid>
            </Grid> */}
            <Grid item xs={11}>
                <Grid container className={classes.sectionContainer}>
                    <Testimony />
                </Grid>
            </Grid>
            <Grid item xs={11}>
                <Grid container className={classes.sectionContainer}>
                    <Potential />
                </Grid>
            </Grid>
        </Grid>
    )
}
