import React, {useEffect, useState} from 'react'
import {Grid } from '@material-ui/core'
import {Link,useParams} from 'react-router-dom'
import useStyle from '../resources/css/pages/blog'
import {GetAllBlogs,GetBlog} from '../databases/Read'
import Renderer from '../components/Blog/RenderComponent'

export default function Blog() {
    const {id} = useParams()
    console.log(id)
    const classes = useStyle()
    const [lists, setLists] = useState([])
    const [mainArtcl, setMainArtcl] = useState(null)
    const text = {
        title:'Blog',
        intro:'Pilih bahan bacaanmu hari ini'
    }

    function ChangeArticle(){

    }

    useEffect(async ()=>{
        if(id){
            console.log(id)
            const main = await GetBlog(id)
            setMainArtcl(main)
        }else{
            setMainArtcl(lists[0])
        }
    },[id])

    useEffect(async()=>{
        const result = await GetAllBlogs()
        setMainArtcl(result[0])
        setLists([...result])
    },[])
    return (
        <Grid container>
            <Grid container justifyContent='center' className={classes.container}>
                <Grid item xs={11}>
                    <Grid container justifyContent='center' className={classes.title}>
                        {text.title}
                    </Grid>
                    <Grid container justifyContent='center' className={classes.intro}>
                        {text.intro}
                    </Grid>
                    <Grid container justifyContent='space-between' direction='row-reverse'>
                        <Grid item md={2} xs={12}>
                            <Grid container className={classes.titleLists}>
                                Other article you might interested in:
                            </Grid>
                            {lists.map((item,index)=>(
                                <Grid container key={`ListItem${index}`} onClick={ChangeArticle()} className={classes.articleLink}>
                                    <Link to={{pathname:`/blog/${item.id}`}} className={classes.links}>
                                        {item.title}
                                    </Link>
                                </Grid>
                            ))}
                        </Grid>
                        <Grid item md={9} xs={12}>
                            <Grid container>
                                Latest Topic
                            </Grid>
                            <Grid container>
                                <Grid container className={classes.mainTitle}>
                                    <h2>{mainArtcl?mainArtcl.title:null}</h2>
                                </Grid>
                                <Grid container className={classes.mainSubTitle}>
                                    By: {mainArtcl?mainArtcl.by:null}, {mainArtcl?mainArtcl.time:null}
                                </Grid>
                                <Grid container className={classes.mainContent}>
                                    {mainArtcl?mainArtcl.items.map((item,index)=>(
                                        <Renderer 
                                        key={index}
                                        type={item.itemType} 
                                        value={item.value}
                                        lang={'en'} /> // change language here to change the language loaded
                                    )):null}
                                </Grid>

                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}
