import React, {useState, useEffect} from 'react'
import {Grid } from '@material-ui/core'
import useStyle from '../resources/css/pages/comic'
import {GetAllComics} from '../databases/Read'
import {useTheme} from '@mui/material/styles'
import useMediaQuery from '@mui/material/useMediaQuery'

export default function Dashboard() {
    const theme = useTheme()
    const smallScreen =useMediaQuery(theme.breakpoints.down('md'));
    const classes = useStyle()
    const [Comics, setComics] = useState([])
    useEffect(async()=>{
        const result = await GetAllComics()
        setComics([...result])
    },[])
    const text = {
        title:'Comic',
        intro:'Yuk belajar bersama si Pundi dan si Receh dalam narasi berbentuk komik/infografis agar lebih memudahkan pehamaman seputar informasi mengenai seluk beluk finansial, investasi, dan pengelolaan keuangan'
    }
    return (
        <Grid container>
            <Grid container justifyContent='center' className={classes.container}>
            <Grid item xs={11}>
                <Grid container justifyContent='center' className={classes.title}>
                    {text.title}
                </Grid>
                <Grid container justifyContent='center' className={classes.intro}>
                    {text.intro}
                </Grid>
                <Grid container justifyContent='space-between'>
                    {Comics.map((datum,index)=>{
                        return(
                            <Grid container key={index} 
                                direction={smallScreen?'row':(index%2>0?'row-reverse':'row')} 
                                justifyContent='center' className={`${classes.oneItem} ${smallScreen?classes.line:null}`}
                            >
                                <Grid item md={6} xs={12}>
                                    <Grid container justifyContent='center'>
                                        <img src={datum.link} className={classes.pict}/>
                                    </Grid>
                                </Grid>
                                <Grid item md={6} xs={12}>
                                    <Grid container alignItems='center' 
                                        justifyContent={smallScreen?'center':(index%2>0?'flex-end':'flex-start')} 
                                        className={classes.itemTitle}
                                    >
                                        {datum.title}
                                    </Grid>
                                </Grid>
                            </Grid>
                        )
                    })}
                </Grid>
            </Grid>
        </Grid>
        </Grid>
    )
}
