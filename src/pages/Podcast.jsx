import React, {useState, useEffect} from 'react'
import {Grid } from '@material-ui/core'
import {Link} from  'react-router-dom'
import useStyle from '../resources/css/pages/podcast'
import {GetAllPods} from '../databases/Read'

export default function Dashboard() {
    const defaultData = {
        date:'',
        title:'',
        link:''
    }
    const [data, setData] = useState([])
    const classes = useStyle()

    useEffect(async ()=>{
        const result = await GetAllPods()
        setData([...result])
    },[])
    // const newValues = 
    // setData([...data,...newValues])

    const text = {
        title:'Podcast',
        intro:'Selain blog, kami juga mengeluarkan konten berbasis podcast yang bisa menemani kamu kamu semua di waktu santai, atau di dalam perjalanan kalian menuju ke kantor atau sekolah sekalian belajar tentang keuangan dan investasi. \n Kamu bisa mendengar episode episode Podcast kita dari link di bawah ini:'
    }
    return (
        <Grid container justifyContent='center' className={classes.container}>
            <Grid item xs={11}>
                <Grid container justifyContent='center' className={classes.title}>
                    {text.title}
                </Grid>
                <Grid container justifyContent='center' className={classes.intro}>
                    {text.intro}
                </Grid>
                <Grid container justifyContent='space-between'>
                    {data.map((datum, index)=>{
                        const date = datum.date.split(' ')
                        return(
                            <Grid item key={index} md={6} xs={11}>
                                <Grid container className={classes.itemContainer}>
                                    <Grid item sm={3} xs={4}>
                                        <Grid container alignItems='center' className={classes.heightContainer}>
                                            <Grid container className={classes.dateContainer}>
                                                <Grid container justifyContent='center' alignItems='center'className={classes.dateDate}>
                                                    {date[0]}
                                                </Grid>
                                                <Grid container justifyContent='center' alignItems='flex-start' className={classes.dateMonth}>
                                                    {`${date[1]} ${date[2]}`}
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item sm={9} xs={8}>
                                        <Grid container alignItems='center' className={classes.heightContainer}>
                                            <Grid container justifyContent='flex-start' alignItems='center'  className={classes.titleText}>
                                                {datum.title}
                                            </Grid>
                                            <Grid container justifyContent='flex-start' alignItems='center'  className={classes.linkText}>
                                                Link: &nbsp;<a href={datum.link} className={classes.link}>Click Here~</a>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        )
                    })}
                </Grid>
            </Grid>
        </Grid>
    )
}
