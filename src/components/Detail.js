import React from 'react'
import {useParams} from 'react-router-dom'

export default function Detail(){
    let {id} =useParams();
    return(
    <h2>Detail Page, {id}</h2>
    )
}