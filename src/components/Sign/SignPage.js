import React from 'react'
import { useState } from 'react';
import { useLocation } from 'react-router-dom';
import {Grid, Card, Typography, CardHeader, Avatar, ThemeProvider} from '@material-ui/core'
import { ThumbUpAlt, AttachMoney, Money, AccountBalance}from '@material-ui/icons'
import { grey, blue, cyan } from '@material-ui/core/colors'
import Styles from '../../resources/css/Styles'
import Signin from './SignIn'
import Signup from './SignUp'
import {useTranslation} from "react-i18next"

const ShowSection = (props) => {
    const useStyle = Styles() 
    const { t } = useTranslation()
    const location = useLocation()
    const data = location.state? location.state.data:null
    console.log(location)
    return (
        <Grid container className={useStyle.signOutsideContainer}>
            {props.page=='in' ? 
                <Signin token={props.token} setToken={props.setToken}/> 
                :
                <Signup data={data?data:null}token={props.token} setToken={props.setToken}/>
            }
        </Grid>
    );
  };
  
export default ShowSection;