import React, {useState} from 'react'
import {Grid, Typography, TextField, Button} from '@material-ui/core'
import {useHistory} from 'react-router-dom'
import useStyles from '../../resources/css/Styles'
import {AddMember2} from '../../databases/Add'
import {CheckLogin} from '../../databases/Read'
import Cookies from 'universal-cookie';

const SignIn = (props) => {
    const history = useHistory()
    const [login,setLogin] = useState({
        email:'',
        password:''
    })
    const useStyle = useStyles()
    function Login(){
        CheckLogin(login).then((result)=>{
            if(result!=null){
                const cookieData = {
                    email: result.email,
                    call: result.username.firstname,
                    fullname: `${result.username.firstname} ${result.username.middlename} ${result.username.lastname}`,
                    level: result.level
                }
                const cookies = new Cookies()
                cookies.set('Loggedin', cookieData, { path: '/' })
                props.setToken(cookieData)
                history.push('/myprofile')
            }
        })
    }
    function handleChange(e){
        const value = e.target.value
        setLogin({
            ...login,
            [e.target.name]:value
        })
    }
    return (
        <>
        <Grid item xl={6} lg={6} md={6}>
        {/* Empty */}
        </Grid>
        <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
            <Grid container justifyContent='center' alignContent='center' alignItems="center" className={useStyle.signContainer}>
                <Grid item xl={8} lg={8} md={8} sm={8} xs={8}>
                    <Grid container style={{gap:'2vh'}}justifyContent='flex-start' alignContent='center' alignItems='center'>
                        {/* <Grid item xl={8} lg={8} md={8} sm={8} xs={10} className={useStyle.SignFormBackground}>
                            <TextField fullWidth type='input' label='First Name'/>
                        </Grid>
                        <Grid item xl={8} lg={8} md={8} sm={8} xs={10} className={useStyle.SignFormBackground}>
                            <TextField fullWidth type='input' label='Last Name'/>
                        </Grid> */}
                        <Grid item xl={8} lg={8} md={8} sm={8} xs={10} className={useStyle.signFormBackground}>
                            <TextField fullWidth type='input' label='Email' name='email' value={login.email} onChange={handleChange}/>
                        </Grid>
                        <Grid item xl={8} lg={8} md={8} sm={8} xs={10} className={useStyle.signFormBackground}>
                            <TextField fullWidth type='password' label='Password' name='password' value={login.password} onChange={handleChange}/>
                        </Grid>
                        <Grid item xl={8} lg={8} md={8} sm={8} xs={10}>
                            <Grid container justifyContent='center'>
                                <Button variant='contained' color='primary' onClick={()=>Login()}>Login</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
        </>
    );
  };
  
export default SignIn;