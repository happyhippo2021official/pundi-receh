const { AddToQueueSharp } = require('@material-ui/icons');
const nodemailer = require('nodemailer');


exports.sendConfirmationEmail = function ({toUser, hash}) {
    return new Promise((response, reject) => {
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user:process.env.REACT_APP_GOOGLE_USER,
                pass:process.env.REACT_APP_GOOGLE_PASSWORD
            }
        })
        const message = {
            from: process.env.REACT_APP_GOOGLE_USER,
            to: process.env.REACT_APP_GOOGLE_USER,
            subject: 'Your Onlyfans - Activate Account',
            html:
            `
            <h3>Hello Kevin</h3>
            <p>Thank you for signing up to our website. Just one more step...</p>
            <p>To activate your account please follow this link: <a target="_" href="${process.env.REACT_APP_GOOGLE_DOMAIN}/api/activate/user" >Activate Link</a></p>
            <p>Cheers,</p>
            <p>OnlyFans</p>
            `
        }
        transporter.sendMail(message, function (error,info) {
            if (error) {
                reject(error)
            } else {
                response(info)
            }
        })
    })
}