import React, {useState,useEffect, useRef} from 'react'
import {Grid, Typography, TextField, Button} from '@material-ui/core'
import {useHistory} from 'react-router-dom'
import useStyles from '../../resources/css/Styles'
import useNewStyle from '../../resources/css/SignPage'
import {AddMemberUser} from '../../databases/Add'
import {CheckLogin} from '../../databases/Read'
import Cookies from 'universal-cookie'
import emailjs from "@emailjs/browser";


const SignUp = (props) => {
    const generateRandomValue = () => {
        const finalCode = Math.random().toString(36).slice(2);
        setRandomCode(finalCode);
    };
    const generateBaseUrl = () => {
        const url = window.location.href;
        const matches = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
        let base_url = matches[0].slice(0, -1) === "/" ? matches[0].slice(0, -1) : matches[0];
        console.log(base_url);
        setBaseUrl(base_url);
    }
    // emailjs stuff
    const form = useRef();
    const SendConfirmationEmail = (data) => {
        // process.env.REACT_APP_GOOGLE_USER
        // const userData = {
        //     to_name:data.firstname,
        //     from_name:'Pundi Receh',
        //     to_email:data.email,
        //     user_email:'Happyhippo2021official@gmail.com',
        //     message:'Hello'
        // };
        // let formUser = document.createElement("form");
        // formUser.setAttribute("to_name", data.firstname);
        // formUser.setAttribute("from_name", "Pundi Receh");
        // formUser.setAttribute("to_email", data.email);
        // formUser.setAttribute("user_email", "Happyhippo2021official@gmail.com");
        // formUser.setAttribute("id", "emailjsform");
        // document.body.appendChild(formUser);
        // formUser.submit();
        emailjs.sendForm(process.env.REACT_APP_EMAILJS_SERVICE_ID, process.env.REACT_APP_EMAILJS_TEMPLATE_ID, form.current, process.env.REACT_APP_EMAILJS_PUBLIC_KEY)
          .then((result) => {
              console.log(result.text);
          }, (error) => {
              console.log(error.text);
          });

    };
    // end of emailjs
    const [randomCode,setRandomCode] = useState(null)

    const [baseUrl,setBaseUrl] = useState(null)

    const [data,setData] = useState({
        firstname:'',
        lastname:'',
        email:'',
        password:''
    })
    const [warnings,setWarning] = useState({
        firstname:false,
        lastname:false,
        email:false,
        password:false
    })
    const history = useHistory()

    const validateEmail = (email) => {
        console.log(email)
        return String(email)
          .toLowerCase()
          .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          )
    }
    const validateName = (name) => {
        return String(name)
          .toLowerCase()
          .match(
            /^[0-9a-zA-Z ]{2,30}$/
          )
    }
    const validatePass = (pass) => {
        return String(pass)
          .toLowerCase()
          .match(
            /^[0-9a-zA-Z]{2,30}$/
          )
    }

    function ValidateInput(){
        let update = {
            firstname:false,
            lastname:false,
            email:false,
            password:false
        }
        let ok = true
        if(!validateEmail(data.email)){
            ok = false
            update.email = true
        }
        if(!validateName(data.firstname)){
            ok = false
            update.firstname = true
        }
        if(!validateName(data.lastname)){
            ok = false
            update.lastname = true
        }
        if(!validatePass(data.password)){
            ok = false
            update.password = true
        }
        if(ok==true){
            AddUser()
        }
        setWarning(update)
    }

    useEffect(()=>{
        console.log(warnings)
    },[warnings])

    async function AddUser(){
        const result = await AddMemberUser(data)
        console.log(result)
        const login = {
            email:data.email,
            password:data.password
        }
        CheckLogin(login).then((result)=>{
            if(result!=null){
                const cookieData = {
                    email: result.email,
                    call: result.username.firstname,
                    fullname: `${result.username.firstname} ${result.username.middlename} ${result.username.lastname}`,
                    level: result.level
                }
                const cookies = new Cookies()
                cookies.set('Loggedin', cookieData, { path: '/' })
                props.setToken(cookieData)
                history.push('/myprofile')
            }
        })
        SendConfirmationEmail(data);
    }
    const useStyle = useStyles()
    const classes = useNewStyle()
    function changeData(prop, value){
        const newData = {...data,
            [prop]:value
        }
        console.log(newData)
        setData(newData)
    }
    useEffect(()=>{
        if(props.data){
            const newData = {...data, ...props.data}
            console.log(newData)
            setData(newData)
        }
        generateRandomValue();
        generateBaseUrl();
    },[])
    return (
        <>
        <Grid item md={6} xs={12}>
            <Grid container justifyContent='center' alignContent='center' alignItems="center" className={[useStyle.signContainer,classes.outsideContainer]}>
                <Grid item xs={8}>
                    <form ref={form} >
                    <Grid  container justifyContent='flex-start' alignContent='center' alignItems='center'>
                        <Grid item sm={8} xs={10} className={useStyle.signFormBackground}>
                            <TextField fullWidth value={data.firstname} name="to_name" onChange={(e)=>{changeData('firstname',e.target.value)}} type='input' label='First Name' required/>
                        </Grid>
                        <Grid item xs={8} className={classes.warnings}>
                            {warnings.firstname?'this field is required':null}
                        </Grid>
                        <Grid item sm={8} xs={10} className={useStyle.signFormBackground}>
                            <TextField fullWidth value={data.lastname} onChange={(e)=>{changeData('lastname',e.target.value)}} type='input' label='Last Name' required/>
                        </Grid>
                        <Grid item xs={8} className={classes.warnings}>
                            {warnings.lastname?'this field is required':null}
                        </Grid>
                        <Grid item sm={8} xs={10} className={useStyle.signFormBackground}>
                            <TextField fullWidth value={data.email} name="user_email" onChange={(e)=>{changeData('email',e.target.value)}} type='email' label='Email' required/>
                        </Grid>
                        <Grid item xs={8} className={classes.warnings}>
                            {warnings.email?'this field is required or email format is wrong':null}
                        </Grid>
                        <Grid item sm={8} xs={10} className={useStyle.signFormBackground}>
                            <TextField fullWidth value={data.password} onChange={(e)=>{changeData('password',e.target.value)}} type='password' label='Password' required/>
                        </Grid>
                        <Grid item xs={8} className={classes.warnings}>
                            {warnings.password?'this field is required':null}
                        </Grid>
                        <input type="hidden" name="base_url" value={baseUrl} />
                        <input type="hidden" name="email_slug" value={randomCode} />
                        <Grid item sm={8} xs={10}>
                            <Grid container justifyContent='center' className={classes.signupButton}>
                                <Button variant='contained' color='primary' onClick={()=>ValidateInput()}>Sign Me Up!</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                    </form>
                </Grid>
            </Grid>
        </Grid>
        <Grid item md={6}>
        {/* Empty */}
        </Grid>
        </>
    );
  };
  
export default SignUp;