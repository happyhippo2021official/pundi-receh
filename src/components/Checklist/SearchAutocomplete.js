import React, { useState } from "react";
import { List, WindowScroller  } from "react-virtualized";
import { Grid } from '@material-ui/core';
import 'font-awesome/css/font-awesome.min.css';
import Autocomplete from "react-autocomplete";
import 'react-virtualized/styles.css'; // only needs to be imported once

const rowRenderer = (data, onSelect) => ({ key, index, style }) => {
  const client = data[index];
  return (
    <div className='suggestionRow' key={key} style={style} onMouseDown={onSelect.bind(null, client)}>
      <div className="suggestionRowInfoTicker">
        <div className='suggestionRowSymbol'>{client.Code}</div>
        <div className='suggestionRowName'>{client.Name}</div>
      </div>
      <div className="suggestionRowInfoExchange">
        <div className='suggestionRowInfoExchangeName'>{client.Exchange}</div>
      </div>
    </div>
  );
};

const renderMenu = (data, onSelect) => () => {
  return (
    <WindowScroller>
    {({ height, width, isScrolling, onChildScroll, scrollTop }) => (
      <div className="autocomplateWrapper">
        <List
          className="suggestionWrapper"
          width={width}
          height={height}
          isScrolling={isScrolling}
          onScroll={onChildScroll}
          autoHeight={true}
          scrollTop={scrollTop}
          rowHeight={60}
          rowCount={data.length}
          rowRenderer={rowRenderer(data, onSelect)}
        />
      </div>
    )}
  </WindowScroller>
  );
};

export default function Search(props) {
  // default value should be empty string
  const [searchTerm, setSearchTerm] = useState("");
  const {fetchEOD, clearInput} = props;
  const setSearchTermCombo = (term, country, exchange) => {
    setSearchTerm(term);
    fetchEOD(term,country,exchange);
  };
  if (smallData !== undefined) {
    // show all items by default
    // let data = smallData; 
    let data = [];
    try {
        if (searchTerm.length >= 2) {
            data = smallData.filter(item =>
              // title.toLowerCase is used for case insensitivity
              item.Code.toLowerCase().substring(0, searchTerm.length).includes(searchTerm.toLowerCase()) || item.Name.toLowerCase().substring(0, searchTerm.length).includes(searchTerm.toLowerCase())
            );
          }
    } catch(e) {

    }
    let slicedData= data.slice(0, 5);
    
    const onSelect = item => setSearchTermCombo(item.Code, item.Country, item.Exchange);
    // const onSelect = item => setSearchTermCombo(item.Code, item.Country == "USA" ? "US" : item.Exchange);
    
    const clearInputbox = () => {
      setSearchTerm("");
      clearInput();
      console.log("clearinggg");
    };


    return (
      <Grid container className="autocomplete-wrapper" justifyContent='space-between'>
        <Grid item xl={8} lg={8} md={8} sm={12} xs={12} className="autocomplete-wrapper-inside">
          <Grid item xl={7} lg={7} md={7} sm={12} xs={12} className="autocomplete-area">
          <Grid container justifyContent='space-between'>
            <Autocomplete
              items={slicedData}
              inputProps={{ className: "autocomplete-wrapper-input", placeholder: 'Please enter your ticker / code' }}
              value={searchTerm}
              getItemValue={item => item.name}
              onChange={(e, value) => setSearchTerm(value)  }
              renderMenu={renderMenu(slicedData, onSelect)}
              renderItem={(item, isHighlighted) =>
                <div className={`item ${isHighlighted ? 'selected-item' : ''}`}>
                  {item.title}
                </div>
              }
              shouldItemRender={() => false}
            />
            <div className="clearButton">
              <i className="fa fa-close" onClick={() => clearInputbox()}></i>
            </div>
          </Grid>
          </Grid>
        </Grid>
        <Grid></Grid>
      </Grid>
    );
  }
}
// https://api.twelvedata.com/stocks
// id,my,sg,us,hk,tw,chn
// JK,KLSE,SG,US,HK,TW,SHG
var listTickers =  require('./stockTickers.json').data;
listTickers = listTickers.sort((a, b) => a.Code.length - b.Code.length);
  
// const stockUS = listTickers.filter(function(ticker) {
//         return ticker.country == "United States";
// })

const smallData = listTickers;
