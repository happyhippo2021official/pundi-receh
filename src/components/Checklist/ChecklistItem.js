import React from 'react'
import { TableRow, TableCell} from '@material-ui/core'

const checkSmaller = (a,b) => {
    return a<b ? true : false;
}

const checkBigger = (a,b) => {
    return a>b ? true : false;
}

const convertPercentage = (a) => {
    return a === 0 ? 0 : (a * 100).toFixed(2);
}

const convertDecimal = (a) => {
    return (a / 100).toFixed(2);
}

const checkNoDebt = (a) => {
    return a <=0.1 ? a = "No debt" : a;
}


const ChecklistItem = (props) => {
    let statusCheck = false;
    let usingDesc;
    let toBePercentage = ['ROE', 'ROA', 'GrossMargin', 'NetMargin', 'CAGR', 'EPS'];
    let canZeroPass = ['Debt_Equity', 'InterestCoverage'];
    // let toBeDecimal = ['Debt_Equity']; this is only for fetchYahoo
    let toBeDecimal = [];
    let toBeCheckValue;
    let finalDesc;
    let finalValue;

    if (props.value && props.value != "na" || props.value == 0) {
        switch (props.operator) {
            case 'checkBigger':
                finalValue = toBeDecimal.includes(props.keyName) ? convertDecimal(props.value) : props.value;
                statusCheck = checkBigger(finalValue, props.limit)  ;
                break;
            case 'checkSmaller':
                finalValue = toBeDecimal.includes(props.keyName) ? convertDecimal(props.value) : props.value;
                statusCheck = checkSmaller(finalValue, props.limit);
                break;    
            case 'na':
                finalValue = "N/A";
                statusCheck = "na";
                usingDesc = "na";
                break;    
        }
        
    } else if (props.value == "na") {
        statusCheck = "na";
        usingDesc = "na";
        finalValue = "N/A";
    }

    if (!statusCheck){
        usingDesc = "notpass";
        statusCheck = "notpass";
    } else if (statusCheck === true) {
        usingDesc = "pass";
        statusCheck = "pass";
    }

    finalDesc = props.desc[usingDesc];
    
    if (toBePercentage.includes(props.keyName)) {
        finalValue = finalValue != "N/A" ? convertPercentage(finalValue) + "%" : finalValue;
    }

    if (canZeroPass.includes(props.keyName) && (props.operator != "na")) {
        if ((props.value == 0 || (props.value <= 0.1) )) {
            usingDesc = "pass";
            statusCheck = "pass";
            finalDesc = "No debt";
            finalValue = props.value;
        }
    }


    return (
        <TableRow key={props.name}>
            <TableCell width="10%" align="center" className={`panel-list-status ${ statusCheck}`}>
                <div className="statusCategory">
                    {props.name}
                </div>
                <div className="statusWrapper">
                    <div className='panel-list-pass'>PASS</div>
                    <div className='panel-list-notpass'>FAIL</div>
                    <div className='panel-list-notapplicable'>N/A</div>
                </div>
            </TableCell>
            <TableCell width="80%" component="th" scope="row" className={`panel-list-text`}>
                <div className='panel-list-category-title'>{props.desc['title']}</div>
                <div className='panel-list-category-desc'>{finalDesc}</div>
            </TableCell>
            <TableCell width="10%" className={`panel-list-values`}>
                <div className='panel-list-values-value'>{ finalValue }</div>
             </TableCell>
        </TableRow>
    );
  };
  
export default ChecklistItem;