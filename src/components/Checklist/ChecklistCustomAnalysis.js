import React from 'react'
import { Typography, Grid,CardMedia, Button, TextField, TableContainer, Paper, Table, TableHead,TableBody, TableRow, TableCell} from '@material-ui/core'
import useStyles from '../../resources/css/Styles';

const checkSmaller = (a,b) => {
    return a<b ? true : false;
}

const checkBigger = (a,b) => {
    return a>b ? true : false;
}

const convertPercentage = (a) => {
    return a === 0 ? 0 : (a * 100).toFixed(2);
}

const convertDecimal = (a) => {
    return (a / 100).toFixed(2);
}

const checkNoDebt = (a) => {
    return a <=0.1 ? a = "No debt" : a;
}


const ChecklistCustomAnalysis = (props) => {
    const useStyle = useStyles()
    return (
        <div className="customAnalysis rightSideChart" key={props.key}>
            <TableContainer component={Paper}>
                    <Table  aria-label="caption table">
                        <caption>This automated risk checks will flag any failed checks as potential investment risks. <br /> A company which passes all our checks, however, is not 'risk free'. </caption>
                        <TableHead>
                            <TableRow className={`${useStyle.headerRowBG1}`}>
                            <TableCell colSpan={3} className={`headerCustomAnalysis ${useStyle.gradientText} ${useStyle.gradientTextColor3}`} >
                                {props.dataObj["objName"].name}
                                </TableCell>
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {
                            Object.keys(props.dataObj).map((keyName, keyIndex) => {
                                if(keyName !== "objName") {
                                    return (
                                        <TableRow >
                                        <TableCell width="70%" align="center">
                                            {props.dataObj[keyName].name}
                                            </TableCell>
                                            <TableCell width="30%" align="center">
                                            {props.dataObj[keyName].value}
                                        </TableCell>
                                        </TableRow>
                                    )
                                }
                            })
                            
                            }
                        </TableBody>
                    </Table>
            </TableContainer>
        </div>
    );
  };
  
export default ChecklistCustomAnalysis;