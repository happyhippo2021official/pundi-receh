import React, {Component} from 'react'
import MenuIcon from '@material-ui/icons/Menu';
import Axios from 'axios';
import '../../resources/css/checkList.css'
import ChecklistItem from './ChecklistItem';
import SearchAutocomplete from './SearchAutocomplete';
import ChartBar from './ChartBar';
import { AdvancedRealTimeChart, Timeline  } from "react-ts-tradingview-widgets";
import { MarketOverview, MarketData, SymbolOverview, FundamentalData, EconomicCalendar, CompanyProfile  } from "react-ts-tradingview-widgets";
import { Typography, Grid,CardMedia, Button, TextField, TableContainer, Paper, Table, TableHead,TableBody, TableRow, TableCell} from '@material-ui/core'
import { Carousel } from 'react-responsive-carousel'
import "react-responsive-carousel/lib/styles/carousel.min.css";

class Checklist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      incomeStatement: [],
      balanceSheet: [],
      eodData: [],
      searchTicker: '',
      TVticker: '',
      exchange:'',
      country:'',
      value: "",
      typeCompany: "",
      typeFormula: "formulaValue"
    };
    this.fetchEOD = this.fetchEOD.bind(this);
    this.clearInput = this.clearInput.bind(this);
  }
  
  componentDidMount() {

  }

  setExchange(exchange) {
    this.setState({exchange: exchange});
  }

  setCountry(country) {
    this.setState({country: country});
  }

  setTVticker(TVticker) {
    this.setState({TVticker: TVticker});
  }

  choosingFormula () {
    let typeFormula;
    let dataAPI = this.state.eodData;
    if (dataAPI.Financials && dataAPI.Financials.Cash_Flow && dataAPI.Financials.Cash_Flow.yearly && dataAPI.Highlights && dataAPI.Highlights.GrossProfitTTM && dataAPI.Highlights.RevenueTTM) {
      let netIncome = dataAPI.Financials.Cash_Flow.yearly[Object.keys(dataAPI.Financials.Cash_Flow.yearly)[0]].netIncome;
      let grossProfitTTM  = dataAPI.Highlights.GrossProfitTTM;
      let revenueTTM  = dataAPI.Highlights.RevenueTTM;
      let grossMargin = grossProfitTTM/revenueTTM;
      let CAGR = dataAPI.Highlights.QuarterlyRevenueGrowthYOY;
      
      if ((CAGR>0.3) && (parseInt(netIncome) < 0)) {
        typeFormula = "formulaGrowth";
      } else if (grossMargin == 0){
        typeFormula = "formulaFinancial";
      } else {
        typeFormula =  "formulaValue";
      }
      console.log(`choosing type formula ${typeFormula} due to CAGR ${CAGR} - net income ${netIncome} - gross margin ${grossMargin} `);
    } else {
      typeFormula =  "formulaValue";
    }
    return typeFormula;
  }
 
  fetchEOD = (stockTicker, country, exchange)=>{
    this.setState({searchTicker: stockTicker});
    let eodTicker = stockTicker;
    let tvAdvanceTicker = stockTicker;
    if (exchange && exchange!=="") {
      if (country == 'USA') {
        eodTicker = `${stockTicker}.US`;
      } else  {
        eodTicker = `${stockTicker}.${exchange}`;
      }
      // // JK,KLSE,SG,US,HK,TW,SHG, SHE
      switch (exchange) {
        case 'JK':
            exchange = 'IDX';
            break;
        case 'SG':
          exchange = 'SGX';
          break;
        case 'HK':
          exchange = 'HKEX';
          break; 
        case 'TW':
          exchange = 'TWSE';
          break; 
        case 'SHG':
          exchange = 'SSE';
          break; 
        case 'KLSE':
          exchange = 'MYX';
          break; 
        case 'SHE':
          exchange = 'SZSE';
          break; 
      }
      tvAdvanceTicker = `${exchange}:${stockTicker}`;
      // this.setCountry(country);
      // this.setExchange(exchange);
      this.setTVticker(tvAdvanceTicker);
      console.log(`this is the tvAdvanceTicker ${tvAdvanceTicker}`);
    }
    var self = this;
    const API_KEY = '61d41dab3fd687.97377946';
    var options = {
    method: 'GET',
    url: `https://eodhistoricaldata.com/api/fundamentals/${eodTicker}?api_token=${API_KEY}&filter=General,Highlights,Financials,Valuation,Trend`,
    params: {modules: 'financialData,incomeStatementHistory,defaultKeyStatistics,balanceSheetHistoryQuarterly,cashflowStatementHistoryQuarterly,cashflowStatementHistory'}
    };
    Axios.request(options).then(function (response) {
        self.setState({eodData: response.data})
        if (response.data.General.Sector == "Financial Services") {
          self.setState({typeFormula:"formulaFinancial"});
        }else {
          self.setState({typeFormula:self.choosingFormula()});
        }
    }).catch(function (error) {
        console.log(`error searching for ${stockTicker} due to ${error}`);
    });
  }

  fetchStatic = (stockTicker)=>{
    this.setState({searchTicker: stockTicker});
    var staticData =  require('./test.json');
    this.setState({eodData: staticData})
    this.setState({typeFormula:this.choosingFormula()});
  }

  calcROETTM () {
    let dataIS = this.state.incomeStatement;
    let dataBS = this.state.balanceSheet;
    let roeTTM = 0;
    if (dataIS.quarterlyReports && dataIS.quarterlyReports.length>0 && dataBS.quarterlyReports && dataBS.quarterlyReports.length>0) {
      let dataQIS = this.state.incomeStatement.quarterlyReports;
      let dataYIS = this.state.incomeStatement.annualReports;
      let dataQBS = this.state.balanceSheet.quarterlyReports;
      let dataYBS = this.state.balanceSheet.annualReports;
      let roeCurQ = this.calcROE(dataQIS[0], dataQBS[0]);
      let roeLastYear = this.calcROE (dataYIS[0], dataYBS[0]);
      let roeLastQ = this.calcROE(dataQIS[4], dataQBS[4]);
      roeTTM = roeCurQ + roeLastYear - roeLastQ;
    }

    return roeTTM;
  }

  calcCAGR() {
    let dataAPI = this.state.eodData;
    let result;
    console.log(`this is data api of calccagr`);
    console.log(dataAPI);
    if (dataAPI.Highlights && dataAPI.Highlights.QuarterlyRevenueGrowthYOY ) {
      result = dataAPI.Highlights.QuarterlyRevenueGrowthYOY;
    } else  {
      result = ["na", "na"];
    }

    return result;
  }

  calcEPS() {
    let dataAPI = this.state.eodData;
    let result;
    if (dataAPI.Highlights && dataAPI.Highlights.QuarterlyEarningsGrowthYOY) {
      result = dataAPI.Highlights.QuarterlyEarningsGrowthYOY;
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcGrossMargin() {
    let dataAPI = this.state.eodData;
    let result;
    if (dataAPI.Highlights && dataAPI.Highlights.GrossProfitTTM && dataAPI.Highlights.RevenueTTM ) {
      let grossProfitTTM  = dataAPI.Highlights.GrossProfitTTM;
      let revenueTTM  = dataAPI.Highlights.RevenueTTM;
      result = grossProfitTTM/revenueTTM;
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcNetMargin() {
    let dataAPI = this.state.eodData;
    let result;
    if (dataAPI.Highlights && dataAPI.Highlights.ProfitMargin) {
      result = dataAPI.Highlights.ProfitMargin;
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcROE() {
    let dataAPI = this.state.eodData;
    let result;
    if (dataAPI.Highlights && dataAPI.Highlights.ReturnOnEquityTTM) {
      result = dataAPI.Highlights.ReturnOnEquityTTM;
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcROA() {
    let dataAPI = this.state.eodData;
    let result;
    if (dataAPI.Highlights && dataAPI.Highlights.ReturnOnAssetsTTM) {
      result = dataAPI.Highlights.ReturnOnAssetsTTM;
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcCurrentRatio() {
    let dataAPI = this.state.eodData;
    let result;
    if (dataAPI.Financials && dataAPI.Financials.Balance_Sheet && dataAPI.Financials.Balance_Sheet.yearly) {
      let totalCurrentAsset = dataAPI.Financials.Balance_Sheet.yearly[Object.keys(dataAPI.Financials.Balance_Sheet.yearly)[0]].totalCurrentAssets;
      let totalCurrentLiabilities = dataAPI.Financials.Balance_Sheet.yearly[Object.keys(dataAPI.Financials.Balance_Sheet.yearly)[0]].totalCurrentLiabilities;
      result = (totalCurrentAsset/totalCurrentLiabilities).toFixed(2);
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcQuickRatio() {
    // not used anymore
    let dataAPI = this.state.eodData;
    let result;
    if (dataAPI.financialData && dataAPI.financialData.quickRatio && dataAPI.financialData.quickRatio.raw) {
      result = Object.values(dataAPI.financialData.quickRatio);
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcDebtEquity() {
    let dataAPI = this.state.eodData;
    let result;
    if (dataAPI.Financials && dataAPI.Financials.Balance_Sheet && dataAPI.Financials.Balance_Sheet.yearly) {
      try {
        let totalEquity = dataAPI.Financials.Balance_Sheet.yearly[Object.keys(dataAPI.Financials.Balance_Sheet.yearly)[0]].totalStockholderEquity;
        let longTermDebt = dataAPI.Financials.Balance_Sheet.yearly[Object.keys(dataAPI.Financials.Balance_Sheet.yearly)[0]].longTermDebt;
        let ratio = (longTermDebt/totalEquity);
        if (ratio > 0) {
          ratio = ratio.toFixed(2);
        }
        result = ratio;
      } catch (e) {
        result = 0;
      } 
    } else  {
      result = "na";
    }
    return result;
  }

  calcInterestCoverateRatio () {
    let dataAPI = this.state.eodData;
    let result;
    let ratio = "na";

    if (dataAPI.Financials && dataAPI.Financials.Income_Statement && dataAPI.Financials.Income_Statement.yearly) {
      let interestExpense = 0;
      try {
        let ebit = dataAPI.Financials.Income_Statement.yearly[Object.keys(dataAPI.Financials.Income_Statement.yearly)[0]].ebit;
        interestExpense = dataAPI.Financials.Income_Statement.yearly[Object.keys(dataAPI.Financials.Income_Statement.yearly)[0]].interestExpense;
        if (interestExpense == null) {
          ratio = "na";
        } else {
          result = (ebit/interestExpense).toFixed(2);
          ratio = interestExpense == 0 ? 0 : (ebit / interestExpense).toFixed(2);
          ratio = Math.abs(ratio);
        }
      } catch (e) {
        ratio = 0;
      } 
    } else {
      ratio = "na";
    } 
    result = ratio;
    return result;
  }

  calcFinancialLeverage () {
    let dataAPI = this.state.eodData;
    let result;
    let ratio = "na";
    if (dataAPI.Financials && dataAPI.Financials.Balance_Sheet && dataAPI.Financials.Balance_Sheet.quarterly) {
      let totalStockholderEquity = dataAPI.Financials.Balance_Sheet.quarterly[Object.keys(dataAPI.Financials.Balance_Sheet.quarterly)[0]].totalStockholderEquity ? dataAPI.Financials.Balance_Sheet.quarterly[Object.keys(dataAPI.Financials.Balance_Sheet.quarterly)[0]].totalStockholderEquity : "na";
      let totalAssets = dataAPI.Financials.Balance_Sheet.quarterly[Object.keys(dataAPI.Financials.Balance_Sheet.quarterly)[0]].totalCurrentAssets ? dataAPI.Financials.Balance_Sheet.quarterly[Object.keys(dataAPI.Financials.Balance_Sheet.quarterly)[0]].totalCurrentAssets : "na";
      if ((totalStockholderEquity !="na") || (totalAssets !="na")){
        ratio = (totalAssets / totalStockholderEquity).toFixed(2);
      } else {
        ratio = "na";
      }
    } else {
      ratio = "na";
    }
    result = ratio;
    return result;
  }

  calcCashflowRatio () {
    let dataAPI = this.state.eodData;
    let result;
    let ratio = "na";
    if (dataAPI.Financials && dataAPI.Financials.Cash_Flow && dataAPI.Financials.Cash_Flow.yearly) {
      let netIncome = dataAPI.Financials.Cash_Flow.yearly[Object.keys(dataAPI.Financials.Cash_Flow.yearly)[0]].netIncome;
      let freeCashflow = dataAPI.Financials.Cash_Flow.yearly[Object.keys(dataAPI.Financials.Cash_Flow.yearly)[0]].freeCashFlow;
      let capex = dataAPI.Financials.Cash_Flow.yearly[Object.keys(dataAPI.Financials.Cash_Flow.yearly)[0]].capitalExpenditures;
      ratio = ((freeCashflow - capex) / parseInt(netIncome)).toFixed(2);
    } else {
      ratio = "na";
    }
    result = ratio;
    return result;
  }

  calcForwardEVRevenue () {
    let dataAPI = this.state.eodData;
    let result;
    let ratio = "na";
    if (dataAPI.Valuation && dataAPI.Valuation.EnterpriseValue && dataAPI.Trend && dataAPI.Trend.length>0) {
      let EV = dataAPI.Valuation.EnterpriseValue;
      let REA = dataAPI.Trend[Object.keys(dataAPI.Trend)[0]].revenueEstimateAvg;
      ratio = (EV / REA).toFixed(2);
    } else {
      ratio = "na";
    }
    result = ratio;
    return result;
  }

  clearInput = ()=>{
    this.setState({searchTicker: ''});
  }
  

  render() {
    let typeFormula = this.state.typeFormula != "" ? this.state.typeFormula : "";
    let typeCompany ="";
    switch (typeFormula) {
      case 'formulaValue':
          typeCompany = "Value";
          break;
      case 'formulaGrowth':
          typeCompany = "Growth";
          break;    
      case 'formulaFinancial':
          typeCompany = "Financial";
          break;    
    }
    console.log("choosing formula " + typeCompany);
    const myVars = ['CAGR', 'EPS', 'GrossMargin', 'NetMargin', 'ROE', 
    'ROA', 'CurrentRatio', 'QuickRatio', 'Debt_Equity', 'FinancialLeverage', 'InterestCoverage', 'FreeCashFlow']; 
    const formula =  require('./Formula.json');
    const CAGR = this.calcCAGR();
    const EPS = this.calcEPS();
    const GrossMargin = this.calcGrossMargin();
    const NetMargin = this.calcNetMargin();
    const ROE = this.calcROE();
    const ROA = this.calcROA();
    const CurrentRatio = this.calcCurrentRatio();
    const QuickRatio = this.calcQuickRatio();
    const Debt_Equity = this.calcDebtEquity();
    const FinancialLeverage = this.calcFinancialLeverage();
    const InterestCoverage = this.calcInterestCoverateRatio();
    const FreeCashFlow = this.calcCashflowRatio();
    const ForwardEVRevenue = this.calcForwardEVRevenue();
    
    var dictVars = {};

    dictVars['CAGR'] = CAGR ? CAGR : 0;
    dictVars['EPS'] = EPS ? EPS : 0;
    dictVars['GrossMargin'] = GrossMargin ? GrossMargin: 0;
    dictVars['NetMargin'] = NetMargin ? NetMargin : 0;
    dictVars['ROE'] = ROE ? ROE : 0;
    dictVars['ROA'] = ROA ? ROA : 0;
    dictVars['CurrentRatio'] = CurrentRatio ? CurrentRatio : 0;
    dictVars['QuickRatio'] = QuickRatio ? QuickRatio : 0;
    dictVars['Debt_Equity'] = Debt_Equity ? Debt_Equity : 0;
    dictVars['FinancialLeverage'] = FinancialLeverage ? FinancialLeverage : 0;
    dictVars['InterestCoverage'] = InterestCoverage ? InterestCoverage : 0;
    dictVars['FreeCashFlow'] = FreeCashFlow ? FreeCashFlow : 0;
    dictVars['ForwardEVRevenue'] = ForwardEVRevenue ? ForwardEVRevenue : 0;
    console.log(dictVars);
    const listTickers =  require('./stockTickers.json').data;

    const stockUS = listTickers.filter(function(ticker) {
            return ticker.country == "United States";
    })

    const categorieDescription = {
      "CAGR": {
          "name": "Revenue Growth%",
          "title": "Is the company able to grow its revenue annually?",
          "pass": "The company has positive revenue growth",
          "notpass": "The company has negative  revenue growth",
          "na" : "N/A"
      },
      "EPS": {
          "name": "EPS Growth%",
          "title": "Is the company able to grow its earning per share annually?",
          "pass": "The company has positive EPS growth",
          "notpass": "The company has negative  EPS growth",
          "na" : "N/A"
      },
      "GrossMargin": {
          "name": "Gross Margin",
          "title": "Is the company able to produce high gross margin?",
          "pass": " The company gross margin is higher than our treshold, the company could have high economic moat",
          "notpass": "The company gross margin is currently lower than our treshold",
          "na" : "N/A"
      },
      "NetMargin": {
          "name": "Net Margin",
          "title": "Is the company able to produce high net income margin?",
          "pass": "The company profitability is higher than our treshold",
          "notpass": "The company profitability is lesser than our treshold",
          "na" : "N/A"
      },
      "ROE": {
          "name": "ROE",
          "title": "Is the return of equity healthy?",
          "pass": "Return of equity going strong",
          "notpass": "Return of equity is low",
          "na" : "N/A"
      },
      "ROA": {
          "name": "ROA",
          "title": "Is the return of assets healthy?",
          "pass": "Return of assets going strong",
          "notpass": "Return of assets is low",
          "na" : "N/A"
      },
      "CurrentRatio": {
          "name": "Current Ratio",
          "title": "Is the short term asset healthy?",
          "pass": "short term asset is healthy enough to pay short term liability",
          "notpass": "short term asset is not enough to pay short term liability,it could lead to financial distress",
          "na" : "N/A"
      },
      "QuickRatio": {
          "name": "Quick Ratio",
          "title": "Is the short term asset healthy?",
          "pass": "short term asset is healthy enough to pay short term liability",
          "notpass": "short term asset is not enough to pay short term liability,it could lead to financial distress",
          "na" : "N/A"
      },
      "Debt_Equity": {
          "name": "Debt/ Equity",
          "title": "Is the company having healthy balance sheet?",
          "pass": "Balance sheet is strong and healthy",
          "notpass": "Balance sheet is unhealthy, high debt",
          "na" : "N/A"
      },
      "FinancialLeverage": {
          "name": "Financial Leverage",
          "title": "Is the company having healthy balance sheet?",
          "pass": "The company is low leverage, low debt",
          "notpass": "The company is having heavily indebt, high leverage",
          "na" : "N/A"
      },
      "InterestCoverage": {
          "name": "Interest Coverage",
          "title": "Is the interest coverage sufficient?",
          "pass": "The interest coverage is high, if NA or 0 means no debt",
          "notpass": "The interest coverage is low",
          "na" : "N/A"
      },
      "FreeCashFlow": {
          "name": "FreeCashFlow to net income",
          "title": "Is the company able to generate positive free cashflow from its net profit?",
          "pass": "Company is able to generate free cashflow from its net profit",
          "notpass": "Company is not able to generate free cashflow from its net profit",
          "na" : "N/A"
      },
      "ForwardEVRevenue": {
        "name": "Forward EV/Revenue",
        "title": "Is the company able to generate positive free cashflow from its net profit?",
        "pass": "Company is able to generate free cashflow from its net profit",
        "notpass": "Company is not able to generate free cashflow from its net profit",
        "na" : "N/A"
      }
    };

    let listChecklistItem = formula[this.state.typeFormula][0];
    
    return (
      <Grid container className="layout-content-wrapper"  style={{gap: 50}} justifyContent='center' alignContent='center' id="layout-content" >
        <Grid container className="layout-content-inside-wrapper" justifyContent='space-between' alignContent='center' >
          <Grid item xl={8} lg={8} md={8} sm={12} xs={12} className="leftSide">
              <SearchAutocomplete 
                  fetchEOD = {this.fetchEOD}
                  clearInput = {this.clearInput}
              />
              {(this.state.searchTicker.length > 0 && this.state.searchTicker != "" && this.state.typeFormula != "")
                ? 
                <Grid container className="postSearchTicker"  style={{gap: 50}} justifyContent='space-between' alignContent='center'>
                  <Grid item xl={12} lg={12} md={12} sm={12} xs={12} className="postSearchTicker_SymbolOverview">
                    <AdvancedRealTimeChart 
                      style="1"
                      symbol = {this.state.TVticker}
                      allow_symbol_change={true}
                      autosize
                      interval="1D"
                    />
                  </Grid>
                  <Grid item xl={6} lg={6} md={6} sm={12} xs={12} className="postSearchTicker_Checklist">
                    <TableContainer component={Paper}>
                      <Table sx={{ minWidth: 650 }} aria-label="caption table">
                          <caption>This automated risk checks will flag any failed checks as potential investment risks. <br /> A company which passes all our checks, however, is not 'risk free'. </caption>
                          <TableHead>
                              <TableRow>
                                  <TableCell colSpan={3}>
                                      {this.state.searchTicker} Risk Checks {typeCompany !="Value" ? `( ${typeCompany} company )` : ""}
                                  </TableCell>
                              </TableRow>
                          </TableHead>

                          <TableBody>
                              {Object.keys(listChecklistItem).map((keyName, keyIndex) => {
                                  return (<ChecklistItem keyName={keyName} name={categorieDescription[keyName].name} value={dictVars[keyName]} operator={dictVars[keyName] == "na" ? "na" : formula[this.state.typeFormula][0][keyName].operator} limit={formula[this.state.typeFormula][0][keyName].limit} desc={categorieDescription[formula[this.state.typeFormula][0][keyName].title]}/>)
                              })}
                          </TableBody>
                      </Table>
                    </TableContainer>
                  </Grid>
                  <Grid item xl={5} lg={5} md={5} sm={12} xs={12} className="postSearchTicker_Info">
                    <Carousel autoPlay showStatus={false} infiniteLoop showArrows={false} indicators={false} interval={6000}  > 
                      <div className="leftSideChart">
                        <FundamentalData symbol={this.state.searchTicker}></FundamentalData>
                      </div>
                      <div className="leftSideChart">
                        <CompanyProfile symbol={this.state.searchTicker}></CompanyProfile>
                      </div>
                    </Carousel>
                  </Grid>
                </Grid>
                : 
                <Grid container className="preTickerContent" justifyContent='space-between' alignContent='center'>
                  <Grid container className="marketArea">
                    <Grid item xl={7} lg={7} md={12} sm={12} xs={12} className="marketAreaData">
                      <MarketData showFloatingTooltip></MarketData>
                    </Grid>
                    <Grid item xl={4} lg={4} md={12} sm={12} xs={12} className="marketAreaOverview">
                      <MarketOverview  showFloatingTooltip></MarketOverview>
                    </Grid>
                  </Grid>
                </Grid>
              } 
              

          </Grid>
          <Grid item xl={3} lg={3} md={3} sm={12} xs={12} className="rightSide">
            {(this.state.searchTicker.length > 0 && this.state.searchTicker != "" && this.state.typeFormula != "")
                ?  
              <Timeline symbol={this.state.searchTicker}></Timeline>
              :
              <Carousel autoPlay  infiniteLoop showArrows={false} indicators={false} interval={6000}  > 
                <div className="chartBar rightSideChart">
                  <ChartBar />
                </div>
                <div className="economicCalendar rightSideChart">
                <EconomicCalendar />
                </div>
              </Carousel>
            }
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default Checklist;