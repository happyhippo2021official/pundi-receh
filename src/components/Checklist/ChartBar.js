import React, {Component} from 'react'
import '../../resources/css/chartBar.css'
import { StockMarket } from "react-ts-tradingview-widgets";


class ChartBar extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  componentDidMount() {
    // const script = document.createElement('script');
    // script.src = 'https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js'
    // // script.src = 'https://www.tradingview.com/symbols/NASDAQ-AAPL/';
    // script.async = false;
    // script.innerHTML = JSON.stringify({
    //   "symbol": "NASDAQ:AAPL",
    //   "interval": "D",
    //   "timezone": "Etc/UTC",
    //   "theme": "light",
    //   "style": "1",
    //   "locale": "en",
    //   "toolbar_bg": "#f1f3f6",
    //   "enable_publishing": false,
    //   "allow_symbol_change": true,
    //   "container_id": "tradingview_4b23b"
    // })
    // this.myRef.current.appendChild(script);
  }

  render() {
    return(
      <div className="chartBarGainerLoser">
        <StockMarket theme="dark" autosize></StockMarket>
      </div>
    );
  }
}

export default ChartBar;