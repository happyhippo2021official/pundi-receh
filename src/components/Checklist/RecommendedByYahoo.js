import React, {Component} from 'react'
import MenuIcon from '@material-ui/icons/Menu';
import Axios from 'axios';
import '../../resources/css/checkList.css'
import { Grid } from '@material-ui/core'

class RecommendedByYahoo extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  
  componentDidMount() {


  }

 
  fetchYahoo = (stockTicker)=>{
    this.setState({searchTicker: stockTicker});
    var self = this;
    const API_KEY = 'KKW4Op0lUfazWSWflVvbt7TwnahgV09X2r2tgwew';
    var options = {
    method: 'GET',
    url: `https://yfapi.net/v11/finance/quoteSummary/${stockTicker}`,
    params: {modules: 'financialData,incomeStatementHistory,defaultKeyStatistics,balanceSheetHistoryQuarterly,cashflowStatementHistoryQuarterly,cashflowStatementHistory'},
        headers: {
            'x-api-key': API_KEY
        }
    };
    Axios.request(options).then(function (response) {
        self.setState({yahooData: response.data.quoteSummary.result[0]})
    }).catch(function (error) {
        console.log(`error searching for ${stockTicker} due to ${error}`);
    });
  }


  

  render() {
   

    return (
      <Grid container className="recommended-yahoo-wrapper"  style={{gap: 50}} justifyContent='center' alignContent='center' id="layout-content" >
       
      </Grid>
    );
  }
}

export default RecommendedByYahoo;