import React from 'react'
import {Grid} from '@material-ui/core'
import { MarketOverview, MarketData, Timeline} from "react-ts-tradingview-widgets";

// const checkNoDebt = (a) => {
//     return a <=0.1 ? a = "No debt" : a;
// }

const CheckListPreView = (props) => {
    
    let finalValue;

    return (
        <Grid container className="preTickerContent" justifyContent='space-between' alignContent='center'>
            <Grid container className="marketArea">
            <Grid item xl={7} lg={7} md={12} sm={12} xs={12} className="marketAreaData">
                <Grid item xl={12} lg={12} md={12} sm={12} xs={12} className="marketAreaDataItems">
                    <Timeline  feedMode="market" market={"stock"} height={400} width="100%"></Timeline>
                </Grid>
                <Grid item xl={12} lg={12} md={12} sm={12} xs={12} className="marketAreaDataItems">
                    <MarketData showFloatingTooltip></MarketData>
                </Grid>
            </Grid>
            <Grid item xl={4} lg={4} md={12} sm={12} xs={12} className="marketAreaOverview">
                <MarketOverview  showFloatingTooltip></MarketOverview>
            </Grid>
            </Grid>
        </Grid>
    );
  };
  
export default CheckListPreView;