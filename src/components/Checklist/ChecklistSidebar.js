import React from 'react'
import {Grid} from '@material-ui/core'
import ChartBar from './ChartBar';
import { Timeline, EconomicCalendar, FundamentalData, CompanyProfile} from "react-ts-tradingview-widgets";
import { Carousel } from 'react-responsive-carousel'
import { Typography,CardMedia, Button, TextField, TableContainer, Paper, Table, TableHead,TableBody, TableRow, TableCell} from '@material-ui/core'
import useStyles from '../../resources/css/Styles';
import { ThemeContext } from './Checklist';


// const checkNoDebt = (a) => {
//     return a <=0.1 ? a = "No debt" : a;
// }

const ChecklistSidebar = (props) => {
    const useStyle = useStyles()
    let finalValue;
    return (
        <Grid item xl={3} lg={3} md={3} sm={12} xs={12} className="rightSide"> 
              <Carousel autoPlay showStatus={false} infiniteLoop showArrows={false} indicators={false} control={false} status={false} interval={6000}  > 
                    <div className="leftSideChart">
                      <FundamentalData symbol={props.searchTicker}></FundamentalData>
                    </div>
                    <div className="leftSideChart">
                      <CompanyProfile symbol={props.searchTicker}></CompanyProfile>
                    </div>
              </Carousel>
        </Grid>
    );
  };
  
export default ChecklistSidebar;