import React, {Component, useState, useEffect, Suspense } from 'react'
import MenuIcon from '@material-ui/icons/Menu';
import Axios from 'axios';
import '../../resources/css/checkList.css'
import CheckListPreView from './ChecklistPreView';
import ChecklistItem from './ChecklistItem';
import ChecklistSidebar from './ChecklistSidebar';
import ChecklistCustomAnalysis from './ChecklistCustomAnalysis';
import SearchAutocomplete from './SearchAutocomplete';

import { AdvancedRealTimeChart, Timeline  } from "react-ts-tradingview-widgets";
import { MarketOverview, MarketData, SymbolOverview, FundamentalData, EconomicCalendar, CompanyProfile  } from "react-ts-tradingview-widgets";
import { Typography, Grid,CardMedia, Button, TextField, TableContainer, Paper, Table, TableHead,TableBody, TableRow, TableCell} from '@material-ui/core'
import { Carousel } from 'react-responsive-carousel'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import useStyles from '../../resources/css/Styles';
export const ThemeContext = React.createContext()

const formula =  require('./Formula.json');
const choosingFormula = (eodData) => {
  let dataAPI = eodData;
  let typeFormula = '';
  if (dataAPI.Financials && dataAPI.Financials.Cash_Flow && dataAPI.Financials.Cash_Flow.yearly && dataAPI.Highlights && dataAPI.Highlights.GrossProfitTTM && dataAPI.Highlights.RevenueTTM) {
    let netIncome = dataAPI.Financials.Cash_Flow.yearly[Object.keys(dataAPI.Financials.Cash_Flow.yearly)[0]].netIncome;
    let grossProfitTTM  = dataAPI.Highlights.GrossProfitTTM;
    let revenueTTM  = dataAPI.Highlights.RevenueTTM;
    let grossMargin = grossProfitTTM/revenueTTM;
    let CAGR = dataAPI.Highlights.QuarterlyRevenueGrowthYOY;
    
    if ((CAGR>0.3) && (parseInt(netIncome) < 0)) {
      typeFormula = "formulaGrowth";
    } else if (grossMargin == 0){
      typeFormula = "formulaFinancial";
    } else {
      typeFormula =  "formulaValue";
    }
  } else {
    typeFormula =  "formulaValue";
  }
  return typeFormula;
}

const calcCAGR = (eodData) => {
  let dataAPI = eodData;
  let result;
  console.log(dataAPI.Highlights.QuarterlyRevenueGrowthYOY);
  if (dataAPI.Highlights && dataAPI.Highlights.QuarterlyRevenueGrowthYOY != null ) {
    result = dataAPI.Highlights.QuarterlyRevenueGrowthYOY;
  } else  {
    result = ["na", "na"];
  }
  return result;
}

const calcEPS = (eodData) => {
  let dataAPI = eodData;
  let result;
  if (dataAPI.Highlights && dataAPI.Highlights.QuarterlyEarningsGrowthYOY != null ) {
    result = dataAPI.Highlights.QuarterlyEarningsGrowthYOY;
  } else  {
    result = ["na", "na"];
  }
  return result;
}

const calcGrossMargin = (eodData) => {
  let dataAPI = eodData;
  let result;
  if (dataAPI.Highlights && dataAPI.Highlights.GrossProfitTTM != null && dataAPI.Highlights.RevenueTTM != null ) {
    let grossProfitTTM  = dataAPI.Highlights.GrossProfitTTM;
    let revenueTTM  = dataAPI.Highlights.RevenueTTM;
    if (grossProfitTTM == 0 || revenueTTM == 0) {
      result = 0;
    } else {
      result = grossProfitTTM/revenueTTM;
    }
  } else  {
    result = ["na", "na"];
  }
  return result;
}

const calcNetMargin = (eodData) => {
  let dataAPI = eodData;
  let result;
  if (dataAPI.Highlights && dataAPI.Highlights.ProfitMargin != null) {
    result = dataAPI.Highlights.ProfitMargin;
  } else  {
    result = ["na", "na"];
  }
  return result;
}

const calcROE = (eodData) => {
  let dataAPI = eodData;
  let result;
  if (dataAPI.Highlights && dataAPI.Highlights.ReturnOnEquityTTM != null) {
    result = dataAPI.Highlights.ReturnOnEquityTTM;
  } else  {
    result = ["na", "na"];
  }
  return result;
}

const calcROA = (eodData) => {
  let dataAPI = eodData;
  let result;
  if (dataAPI.Highlights && dataAPI.Highlights.ReturnOnAssetsTTM != null) {
    result = dataAPI.Highlights.ReturnOnAssetsTTM;
  } else  {
    result = ["na", "na"];
  }
  return result;
}

const calcCurrentRatio = (eodData) => {
  let dataAPI = eodData;
  let result;
  if (dataAPI.Financials && dataAPI.Financials.Balance_Sheet && dataAPI.Financials.Balance_Sheet.yearly ) {
    let totalCurrentAsset = dataAPI.Financials.Balance_Sheet.yearly[Object.keys(dataAPI.Financials.Balance_Sheet.yearly)[0]].totalCurrentAssets;
    let totalCurrentLiabilities = dataAPI.Financials.Balance_Sheet.yearly[Object.keys(dataAPI.Financials.Balance_Sheet.yearly)[0]].totalCurrentLiabilities;
    result = (totalCurrentAsset/totalCurrentLiabilities).toFixed(2);
  } else  {
    result = ["na", "na"];
  }
  return result;
}

const calcQuickRatio = (eodData) => {
  // not used anymore
  let dataAPI = eodData;
  let result;
  if (dataAPI.financialData && dataAPI.financialData.quickRatio && dataAPI.financialData.quickRatio.raw) {
    result = Object.values(dataAPI.financialData.quickRatio);
  } else  {
    result = ["na", "na"];
  }
  return result;
}

const calcDebtEquity = (eodData) => {
  let dataAPI = eodData;
  let result;
  if (dataAPI.Financials && dataAPI.Financials.Balance_Sheet && dataAPI.Financials.Balance_Sheet.yearly) {
    try {
      let totalEquity = dataAPI.Financials.Balance_Sheet.yearly[Object.keys(dataAPI.Financials.Balance_Sheet.yearly)[0]].totalStockholderEquity;
      let longTermDebt = dataAPI.Financials.Balance_Sheet.yearly[Object.keys(dataAPI.Financials.Balance_Sheet.yearly)[0]].longTermDebt;
      let ratio = (longTermDebt/totalEquity);
      if (ratio > 0) {
        ratio = ratio.toFixed(2);
      }
      result = ratio;
    } catch (e) {
      result = 0;
    } 
  } else  {
    result = "na";
  }
  return result;
}

const calcInterestCoverateRatio = (eodData) => {
  let dataAPI = eodData;
  let result;
  let ratio = "na";

  if (dataAPI.Financials && dataAPI.Financials.Income_Statement && dataAPI.Financials.Income_Statement.yearly) {
    let interestExpense = 0;
    try {
      let ebit = dataAPI.Financials.Income_Statement.yearly[Object.keys(dataAPI.Financials.Income_Statement.yearly)[0]].ebit;
      interestExpense = dataAPI.Financials.Income_Statement.yearly[Object.keys(dataAPI.Financials.Income_Statement.yearly)[0]].interestExpense;
      if (interestExpense == null) {
        ratio = "na";
      } else {
        result = (ebit/interestExpense).toFixed(2);
        ratio = interestExpense == 0 ? 0 : (ebit / interestExpense).toFixed(2);
        ratio = Math.abs(ratio);
      }
    } catch (e) {
      ratio = 0;
    } 
  } else {
    ratio = "na";
  } 
  result = ratio;
  return result;
}

const calcFinancialLeverage = (eodData) => {
  let dataAPI = eodData;
  let result;
  let ratio = "na";
  if (dataAPI.Financials && dataAPI.Financials.Balance_Sheet && dataAPI.Financials.Balance_Sheet.quarterly) {
    let totalStockholderEquity = dataAPI.Financials.Balance_Sheet.quarterly[Object.keys(dataAPI.Financials.Balance_Sheet.quarterly)[0]].totalStockholderEquity ? dataAPI.Financials.Balance_Sheet.quarterly[Object.keys(dataAPI.Financials.Balance_Sheet.quarterly)[0]].totalStockholderEquity : "na";
    let totalAssets = dataAPI.Financials.Balance_Sheet.quarterly[Object.keys(dataAPI.Financials.Balance_Sheet.quarterly)[0]].totalCurrentAssets ? dataAPI.Financials.Balance_Sheet.quarterly[Object.keys(dataAPI.Financials.Balance_Sheet.quarterly)[0]].totalCurrentAssets : "na";
    if ((totalStockholderEquity !="na") || (totalAssets !="na")){
      ratio = (totalAssets / totalStockholderEquity).toFixed(2);
    } else {
      ratio = "na";
    }
  } else {
    ratio = "na";
  }
  result = ratio;
  return result;
}

const calcCashflowRatio = (eodData) => {
  let dataAPI = eodData;
  let result;
  let ratio = "na";
  if (dataAPI.Financials && dataAPI.Financials.Cash_Flow && dataAPI.Financials.Cash_Flow.yearly) {
    let netIncome = dataAPI.Financials.Cash_Flow.yearly[Object.keys(dataAPI.Financials.Cash_Flow.yearly)[0]].netIncome;
    let freeCashflow = dataAPI.Financials.Cash_Flow.yearly[Object.keys(dataAPI.Financials.Cash_Flow.yearly)[0]].freeCashFlow;
    let capex = dataAPI.Financials.Cash_Flow.yearly[Object.keys(dataAPI.Financials.Cash_Flow.yearly)[0]].capitalExpenditures;
    ratio = ((freeCashflow - capex) / parseInt(netIncome)).toFixed(2);
  } else {
    ratio = "na";
  }
  result = ratio;
  return result;
}


const Checklist = () => {
  const useStyle = useStyles()
  const [searchTicker, setSearchTicker] = useState('');
  const categorieDescription = {
    "CAGR": {
        "name": "Revenue Growth%",
        "title": "Is the company able to grow its revenue annually?",
        "pass": "The company has positive revenue growth",
        "notpass": "The company has negative  revenue growth",
        "na" : "N/A"
    },
    "EPS": {
        "name": "EPS Growth%",
        "title": "Is the company able to grow its earning per share annually?",
        "pass": "The company has positive EPS growth",
        "notpass": "The company has negative  EPS growth",
        "na" : "N/A"
    },
    "GrossMargin": {
        "name": "Gross Margin",
        "title": "Is the company able to produce high gross margin?",
        "pass": " The company gross margin is higher than our treshold, the company could have high economic moat",
        "notpass": "The company gross margin is currently lower than our treshold",
        "na" : "N/A"
    },
    "NetMargin": {
        "name": "Net Margin",
        "title": "Is the company able to produce high net income margin?",
        "pass": "The company profitability is higher than our treshold",
        "notpass": "The company profitability is lesser than our treshold",
        "na" : "N/A"
    },
    "ROE": {
        "name": "ROE",
        "title": "Is the return of equity healthy?",
        "pass": "Return of equity going strong",
        "notpass": "Return of equity is low",
        "na" : "N/A"
    },
    "ROA": {
        "name": "ROA",
        "title": "Is the return of assets healthy?",
        "pass": "Return of assets going strong",
        "notpass": "Return of assets is low",
        "na" : "N/A"
    },
    "CurrentRatio": {
        "name": "Current Ratio",
        "title": "Is the short term asset healthy?",
        "pass": "short term asset is healthy enough to pay short term liability",
        "notpass": "short term asset is not enough to pay short term liability,it could lead to financial distress",
        "na" : "N/A"
    },
    "QuickRatio": {
        "name": "Quick Ratio",
        "title": "Is the short term asset healthy?",
        "pass": "short term asset is healthy enough to pay short term liability",
        "notpass": "short term asset is not enough to pay short term liability,it could lead to financial distress",
        "na" : "N/A"
    },
    "Debt_Equity": {
        "name": "Debt/ Equity",
        "title": "Is the company having healthy balance sheet?",
        "pass": "Balance sheet is strong and healthy",
        "notpass": "Balance sheet is unhealthy, high debt",
        "na" : "N/A"
    },
    "FinancialLeverage": {
        "name": "Financial Leverage",
        "title": "Is the company having healthy balance sheet?",
        "pass": "The company is low leverage, low debt",
        "notpass": "The company is having heavily indebt, high leverage",
        "na" : "N/A"
    },
    "InterestCoverage": {
        "name": "Interest Coverage",
        "title": "Is the interest coverage sufficient?",
        "pass": "The interest coverage is high, if NA or 0 means no debt",
        "notpass": "The interest coverage is low",
        "na" : "N/A"
    },
    "FreeCashFlow": {
        "name": "FreeCashFlow to net income",
        "title": "Is the company able to generate positive free cashflow from its net profit?",
        "pass": "Company is able to generate free cashflow from its net profit",
        "notpass": "Company is not able to generate free cashflow from its net profit",
        "na" : "N/A"
    },
    "ForwardEVRevenue": {
      "name": "Forward EV/Revenue",
      "title": "Is the company able to generate positive free cashflow from its net profit?",
      "pass": "Company is able to generate free cashflow from its net profit",
      "notpass": "Company is not able to generate free cashflow from its net profit",
      "na" : "N/A"
    }
  };
  const dupontObj = {"objName": {"name":"Dupont analysis"}, "Net Margin": {"name": "Net Margin", 'value':'-'}, "Asset Turnover": {"name": "Asset Turnover", 'value':'-'}, "Financial Leverage": {"name": "Financial Leverage", 'value':'-'}, "ROE": {"name": "ROE", 'value':'-'}};
  const fcfvsdObj = {"objName": {"name":"FCFVS analysis"}, "Free Cashflow/share": {"name": "Free Cashflow/share", 'value':'-'}, "Dividend/share": {"name": "Dividend/share", 'value':'-'}, "Free CF after dividend": {"name": "Free CF after dividend", 'value':'-'}, "Is FCF able to cover dividend": {"name": "Is FCF able to cover dividend", 'value':'-'}};
  const valuationObj = {"objName": {"name":"Valuation analysis"}, "EPS": {"name": "EPS", 'value':'-'}, "BVPS": {"name": "BVPS", 'value':'-'}, "SPS": {"name": "SPS", 'value':'-'}};
  const [dupontData, setDupontData] = useState(dupontObj);
  const [fcfvsdData, setFcfvsdData] = useState(fcfvsdObj);
  const [valuationData, setValuationData] = useState(valuationObj);
  const [eodData, setEodData] = useState('');
  const [TVticker, setTVticker] = useState('');
  const [exchange, setExchange] = useState('');
  const [country, setCountry] = useState('');
  const [value, setValue] = useState('');
  const [typeCompany, setTypeCompany] = useState('');
  const [typeFormula, setTypeFormula] = useState('');
  const [CAGR, setCAGR] = useState('na');
  const [listChecklistItem, setListChecklistItem] = useState([]);
  const [valueList, setValueList] = useState([]);
  var dictVars = {};

  const calculateDictVars = (dictVars) => {
    dictVars['CAGR'] = calcCAGR(eodData) != null ? calcCAGR(eodData) : "na";
    dictVars['EPS'] = calcEPS(eodData) != null? calcEPS(eodData) : "na";
    dictVars['GrossMargin'] = calcGrossMargin(eodData) != null ? calcGrossMargin(eodData) : "na";
    dictVars['NetMargin'] = calcNetMargin(eodData) != null ? calcNetMargin(eodData) : "na";
    dictVars['ROE'] = calcROE(eodData) != null ? calcROE(eodData) : "na";
    dictVars['ROA'] = calcROA(eodData) != null? calcROA(eodData) : "na";
    dictVars['CurrentRatio'] = calcCurrentRatio(eodData) != null ? calcCurrentRatio(eodData) : "na";
    dictVars['QuickRatio'] = calcQuickRatio(eodData) != null? calcQuickRatio(eodData) : "na";
    dictVars['Debt_Equity'] = calcDebtEquity(eodData) != null ? calcDebtEquity(eodData) : "na";
    dictVars['FinancialLeverage'] = calcFinancialLeverage(eodData) != null ? calcFinancialLeverage(eodData) : "na";
    dictVars['InterestCoverage'] = calcInterestCoverateRatio(eodData) != null ? calcInterestCoverateRatio(eodData) : "na";
    dictVars['FreeCashFlow'] = calcCashflowRatio(eodData) != null ? calcCashflowRatio(eodData) : "na";
    dictVars['ForwardEVRevenue'] = calcGrossMargin(eodData) != null ? calcGrossMargin(eodData) : "na";
    return dictVars;
  }
  
  const fetchEOD = (stockTicker, country, exchange) => {
    var self = this;
    let eodTicker = stockTicker;
    let tvAdvanceTicker = stockTicker;
    if (exchange && exchange!=="") {
      if (country == 'USA') {
        eodTicker = `${stockTicker}.US`;
      } else  {
        eodTicker = `${stockTicker}.${exchange}`;
      }
      // // JK,KLSE,SG,US,HK,TW,SHG, SHE
      switch (exchange) {
        case 'JK':
            exchange = 'IDX';
            break;
        case 'SG':
          exchange = 'SGX';
          break;
        case 'HK':
          exchange = 'HKEX';
          break; 
        case 'TW':
          exchange = 'TWSE';
          break; 
        case 'SHG':
          exchange = 'SSE';
          break; 
        case 'KLSE':
          exchange = 'MYX';
          break; 
        case 'SHE':
          exchange = 'SZSE';
          break; 
      }
      tvAdvanceTicker = `${exchange}:${stockTicker}`;
      setTVticker(tvAdvanceTicker);
    }
    const API_KEY = '61d41dab3fd687.97377946';
    var options = {
    method: 'GET',
    url: `https://eodhistoricaldata.com/api/fundamentals/${eodTicker}?api_token=${API_KEY}&filter=General,Highlights,Financials,Valuation,Trend,SharesStats`,
    params: {modules: 'financialData,incomeStatementHistory,defaultKeyStatistics,balanceSheetHistoryQuarterly,cashflowStatementHistoryQuarterly,cashflowStatementHistory'}
    };
    Axios.request(options).then(function (response) {
        setEodData(response.data);
        if (response.data.General.Sector == "Financial Services") {
          setTypeFormula("formulaFinancial");
        }else {
          setTypeFormula(choosingFormula(response.data));
        }
        let kindCompany ="";
        switch (typeFormula) {
          case 'formulaValue':
              kindCompany = "";
              break;
          case 'formulaGrowth':
              kindCompany = "(Growth company)";
              break;    
          case 'formulaFinancial':
              kindCompany = "(Financial company)";
              break;    
        }
        setTypeCompany(kindCompany);
        console.log(typeCompany);
        setSearchTicker(stockTicker);
    }).catch(function (error) {
        console.log(`error searching for ${stockTicker} due to ${error}`);
    });
  }

  const clearInput = () => {
    setSearchTicker('');
  }
  
  useEffect(
    () => {
      if (valueList && valueList.hasOwnProperty('CAGR')) {
        let data = valueList;
         // newDupontObj
        let assetTurnover;
        try {
          let revTTM = eodData.Highlights.RevenueTTM;
          let totalAssets = eodData.Financials.Balance_Sheet.yearly[Object.keys(eodData.Financials.Balance_Sheet.yearly)[0]].totalAssets;
          assetTurnover = revTTM/totalAssets;
        } catch (e) {
          assetTurnover = "-";
        }
        
        let newDupontObj = {"objName": {"name":"Dupont analysis"},"Net Margin": {"name": "Net Margin", 'value':valueList['NetMargin']}, "Asset Turnover": {"name": "Asset Turnover", 'value':assetTurnover}, "Financial Leverage": {"name": "Financial Leverage", 'value':valueList['FinancialLeverage']}, "ROE": {"name": "ROE", 'value':valueList['ROE']}};
        setDupontData(newDupontObj);
        
        // newFcfvsdObj
        let fcf;
        let sharesOutstanding;
        let dividendShare;
        let fcfByShares;
        let fcfAfter;
        let ableCoverFCF;
        try {
          fcf = eodData.Financials.Cash_Flow.yearly[Object.keys(eodData.Financials.Cash_Flow.yearly)[0]].freeCashFlow;
          sharesOutstanding = eodData.SharesStats.SharesOutstanding;
          fcfByShares = fcf/sharesOutstanding;
        } catch (e) {
          fcfByShares = "-";
        }

        try {
          dividendShare = eodData.Highlights.DividendShare;
        } catch (e) {
          dividendShare = "-";
        }

        try {
          if (dividendShare == "-" & fcfByShares !== "-") {
            fcfAfter = fcfByShares; 
          } else {
            fcfAfter = fcfByShares - dividendShare;
          }
        } catch (e) {
          fcfAfter = "-";
        }
        if (fcfAfter == "-") {
          ableCoverFCF = "-";
        }  else {
          ableCoverFCF = fcfAfter > 0 ? "Yes" :  "No";
        }
        let newFcfvsdObj = {"objName": {"name":"FCFVS analysis"}, "Free Cashflow/share": {"name": "Free Cashflow/share", 'value':fcfByShares}, "Dividend/share": {"name": "Dividend/share", 'value':dividendShare}, "Free CF after dividend": {"name": "Free CF after dividend", 'value':fcfAfter}, "Is FCF able to cover dividend": {"name": "Is FCF able to cover dividend", 'value':ableCoverFCF}};
        setFcfvsdData(newFcfvsdObj);

         // valuationObj
         let eps;
         let bvps;
         let sps;
         try {
           eps = eodData.Highlights.DilutedEpsTTM;
         } catch (e) {
           eps = "-";
         }

         try {
          bvps = eodData.Highlights.BookValue;
         } catch (e) {
          bvps = "-";
         }

         try {
          sps = eodData.Highlights.RevenuePerShareTTM;
         } catch (e) {
          sps = "-";
         }
         
         let newValuationObj = {"objName": {"name":"Valuation analysis"}, "EPS": {"name": "EPS", 'value':eps}, "BVPS": {"name": "BVPS", 'value':bvps}, "SPS": {"name": "SPS", 'value':sps}};
         setValuationData(newValuationObj);

      }
    },
    [valueList],
  );

  useEffect(
    () => {
      if (searchTicker && searchTicker != '' && typeFormula && typeFormula != '') {
        dictVars = calculateDictVars (dictVars);
        console.log("this is data dictVars",dictVars);
        setValueList(dictVars);
        setListChecklistItem (formula[typeFormula][0]);
      }
    },
    [searchTicker],
  );

    
  return (
    <ThemeContext.Provider value={searchTicker}>
      <Grid container className="layout-content-wrapper"  style={{gap: 50}} justifyContent='center' alignContent='center' id="layout-content" >
        <Grid container className="layout-content-inside-wrapper" justifyContent='space-between' alignContent='center' >
          <Grid item xl={12} lg={12} md={12} sm={12} xs={12} className="searchArea">
            <SearchAutocomplete 
                fetchEOD = {fetchEOD}
                clearInput = {clearInput}
            />
          </Grid>
          <Grid item xl={8} lg={8} md={8} sm={12} xs={12} className={"leftSide"}>
              {(searchTicker.length > 0 && searchTicker != "")
                ? 
                <Grid container className="postSearchTicker"  justifyContent='space-between' alignContent='center'>
                  <Grid item xl={12} lg={12} md={12} sm={12} xs={12} className="postSearchTicker_SymbolOverview">
                    <AdvancedRealTimeChart 
                      style="1"
                      symbol = {TVticker}
                      allow_symbol_change={true}
                      autosize
                      interval="1D"
                    />
                  </Grid>
                  <Grid item xl={6} lg={6} md={6} sm={12} xs={12} className="postSearchTicker_Checklist">
                    <Suspense fallback={<h1>Loading Checklist...</h1>}>
                      <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="caption table">
                            <caption>This automated risk checks will flag any failed checks as potential investment risks. <br /> A company which passes all our checks, however, is not 'risk free'. </caption>
                            <TableHead>
                                <TableRow>
                                    <TableCell colSpan={3}>
                                        {searchTicker} Risk Checks {typeCompany}
                                    </TableCell>
                                </TableRow>
                            </TableHead>

                            <TableBody>
                                {Object.keys(listChecklistItem).map((keyName, keyIndex) => {
                                    return (<ChecklistItem keyName={keyName} name={categorieDescription[keyName].name} value={valueList[keyName]} operator={valueList[keyName] == "na" ? "na" : formula[typeFormula][0][keyName].operator} limit={formula[typeFormula][0][keyName].limit} desc={categorieDescription[formula[typeFormula][0][keyName].title]}/>)
                                })}
                            </TableBody>
                        </Table>
                      </TableContainer>
                    </Suspense>
                  </Grid>
                  <Grid item xl={5} lg={5} md={5} sm={12} xs={12} className="postSearchTicker_Info">
                    <Carousel autoPlay  infiniteLoop showArrows={false} indicators={false} interval={6000}  > 
                      {/* dupont */}
                      <ChecklistCustomAnalysis dataObj={dupontData} key={"dupont"}/>
                      <ChecklistCustomAnalysis dataObj={fcfvsdData} key={"fcfvs"}/>
                      <ChecklistCustomAnalysis dataObj={valuationData} key={"valuation"} />
                    </Carousel>
                  </Grid>
                </Grid>
                : 
                <CheckListPreView />
              } 

          </Grid>
          {(searchTicker.length > 0 && searchTicker != "")
                ? 
          <ChecklistSidebar searchTicker={searchTicker} />
          :
          ""
          }
        </Grid>
      </Grid>
    </ThemeContext.Provider>
  );
};

export default Checklist;