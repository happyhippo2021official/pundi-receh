import React, {Component} from 'react'
import MenuIcon from '@material-ui/icons/Menu';
import Axios from 'axios';
import '../../resources/css/checkList.css'
import ChecklistItem from './ChecklistItem';
import SearchAutocomplete from './SearchAutocomplete';
import ChartBar from './ChartBar';
import { AdvancedRealTimeChart, Timeline  } from "react-ts-tradingview-widgets";
import { MarketOverview, MarketData, SymbolOverview, FundamentalData, EconomicCalendar, CompanyProfile  } from "react-ts-tradingview-widgets";
import { Typography, Grid,CardMedia, Button, TextField, TableContainer, Paper, Table, TableHead,TableBody, TableRow, TableCell} from '@material-ui/core'
import { Carousel } from 'react-responsive-carousel'
import "react-responsive-carousel/lib/styles/carousel.min.css";

class Checklist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      incomeStatement: [],
      balanceSheet: [],
      yahooData: [],
      searchTicker: '',
      value: "",
      typeCompany: "",
      typeFormula: ""
    };
    this.fetchYahoo = this.fetchYahoo.bind(this);
    this.clearInput = this.clearInput.bind(this);
  }
  
  componentDidMount() {

  }
  // fetch financial datas
  fetchDataIncomeStatement() {
    var self = this;
    const API_KEY = 'KW5HRA57KEDJX36H';
    let StockSymbol = 'INMD';
    const API_Call = `https://www.alphavantage.co/query?function=INCOME_STATEMENT&symbol=${StockSymbol}&apikey=${API_KEY}`;
    Axios.get(API_Call)
    .then(function (response) {
      self.setState({incomeStatement: response.data})
    })
   .catch(function (error) {
      console.log(error);
   });
  }
  fetchDataBalanceSheet() {
    var self = this;
    const API_KEY = 'KW5HRA57KEDJX36H';
    let StockSymbol = 'INMD';
    const API_Call = `https://www.alphavantage.co/query?function=BALANCE_SHEET&symbol=${StockSymbol}&apikey=${API_KEY}`;
    Axios.get(API_Call)
    .then(function (response) {
      self.setState({balanceSheet: response.data})
    })
   .catch(function (error) {
      console.log(error);
   });
  }
  fetchAxios() {
    const API_KEY = 'KW5HRA57KEDJX36H';
    let StockSymbol = 'INMD';
    // "Referer": "https://financials.morningstar.com/ratios/r.html?t=INMD&culture=en&platform=sal",
    // const API_Call = 'https://financials.morningstar.com/finan/ajax/exportKR2CSV.html?&callback=?&t=INMD&region=usa&culture=en-US&cur=&order=asc';
    // const API_Call = 'http://financials.morningstar.com/ajax/ReportProcess4CSV.html?t=XNAS:INMD&reportType=is&period=12&dataType=A&order=desc&denominatorView=raw&columnYear=5&number=3';
    const API_Call = 'https://query1.finance.yahoo.com/v10/finance/quoteSummary/INMD?formatted=true&crumb=AKV/cl0TOgz&lang=en-us&region=us&modules=defaultKeyStatistics,financialData&corsDomain=finance.yahoo.com';
    const config = {
      headers: {
        "Access-Control-Allow-Originrer": "http://localhost:3000/",
        "Access-Control-Allow-Credentials": "true"
      },
    };

    Axios.get(API_Call, config)
    .then((response) => console.log(response.data))
    .catch((error) => console.log(error.response));
  }

  choosingFormula () {
    let typeFormula;
    let dataYahoo = this.state.yahooData;
    if (dataYahoo.defaultKeyStatistics && dataYahoo.financialData) {
      let netIncome = dataYahoo.defaultKeyStatistics.netIncomeToCommon.raw;
      let grossMargin = dataYahoo.financialData.grossMargins? dataYahoo.financialData.grossMargins.raw : 0;
      let CAGR = dataYahoo.financialData.revenueGrowth.raw;
      
      if ((CAGR>0.3) && (netIncome < 0)) {
        typeFormula = "formulaGrowth";
      } else if (grossMargin == 0){
        typeFormula = "formulaFinancial";
      } else {
        typeFormula =  "formulaValue";
      }
      console.log(`choosing type formula ${typeFormula} due to CAGR ${CAGR} - net income ${netIncome} - gross margin ${grossMargin} `);
    }
    return typeFormula;
  }
 
  fetchYahoo = (stockTicker)=>{
    this.setState({searchTicker: stockTicker});
    var self = this;
    const API_KEY = 'gKbz52gWmX3Xok6pYJ0SN7r7S35RkvH2uTFwzNZ2';
    var options = {
    method: 'GET',
    url: `https://yfapi.net/v11/finance/quoteSummary/${stockTicker}`,
    params: {modules: 'financialData,incomeStatementHistory,defaultKeyStatistics,balanceSheetHistoryQuarterly,cashflowStatementHistoryQuarterly,cashflowStatementHistory'},
    headers: {
        'x-api-key': API_KEY
    }
    };
    Axios.request(options).then(function (response) {
        self.setState({yahooData: response.data.quoteSummary.result[0]})
        self.setState({typeFormula:self.choosingFormula()});
        console.log(response.data.quoteSummary.result[0]);
    }).catch(function (error) {
        console.log(`error searching for ${stockTicker} due to ${error}`);
    });
  }

  calcROETTM () {
    let dataIS = this.state.incomeStatement;
    let dataBS = this.state.balanceSheet;
    let roeTTM = 0;
    if (dataIS.quarterlyReports && dataIS.quarterlyReports.length>0 && dataBS.quarterlyReports && dataBS.quarterlyReports.length>0) {
      let dataQIS = this.state.incomeStatement.quarterlyReports;
      let dataYIS = this.state.incomeStatement.annualReports;
      let dataQBS = this.state.balanceSheet.quarterlyReports;
      let dataYBS = this.state.balanceSheet.annualReports;
      let roeCurQ = this.calcROE(dataQIS[0], dataQBS[0]);
      let roeLastYear = this.calcROE (dataYIS[0], dataYBS[0]);
      let roeLastQ = this.calcROE(dataQIS[4], dataQBS[4]);
      roeTTM = roeCurQ + roeLastYear - roeLastQ;
    }

    return roeTTM;
  }

  calcCAGRYahoo () {
    let dataYahoo = this.state.yahooData;
    let result;
    if (dataYahoo.financialData && dataYahoo.financialData.revenueGrowth && dataYahoo.financialData.revenueGrowth.raw) {
      result = Object.values(dataYahoo.financialData.revenueGrowth);
    } else  {
      result = ["na", "na"];
    }

    return result;
  }

  calcEPSYahoo () {
    let dataYahoo = this.state.yahooData;
    let result;
    if (dataYahoo.financialData && dataYahoo.financialData.earningsGrowth && dataYahoo.financialData.earningsGrowth.raw) {
      result = Object.values(dataYahoo.financialData.earningsGrowth);
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcGrossMarginYahoo () {
    let dataYahoo = this.state.yahooData;
    let result;
    if (dataYahoo.financialData && dataYahoo.financialData.grossMargins.raw) {
      result = Object.values(dataYahoo.financialData.grossMargins);
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcNetMarginYahoo () {
    let dataYahoo = this.state.yahooData;
    let result;
    if (dataYahoo.financialData && dataYahoo.financialData.profitMargins.raw) {
      result = Object.values(dataYahoo.financialData.profitMargins);
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcROEYahoo () {
    let dataYahoo = this.state.yahooData;
    let result;
    if (dataYahoo.financialData && dataYahoo.financialData.returnOnEquity && dataYahoo.financialData.returnOnEquity.raw) {
      result = Object.values(dataYahoo.financialData.returnOnEquity);
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcROAYahoo () {
    let dataYahoo = this.state.yahooData;
    let result;
    if (dataYahoo.financialData && dataYahoo.financialData.returnOnAssets && dataYahoo.financialData.returnOnAssets.raw) {
      result = Object.values(dataYahoo.financialData.returnOnAssets);
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcCurrentRatioYahoo () {
    let dataYahoo = this.state.yahooData;
    let result;
    if (dataYahoo.financialData && dataYahoo.financialData.currentRatio && dataYahoo.financialData.currentRatio.raw) {
      result = Object.values(dataYahoo.financialData.currentRatio);
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcQuickRatioYahoo () {
    let dataYahoo = this.state.yahooData;
    let result;
    if (dataYahoo.financialData && dataYahoo.financialData.quickRatio && dataYahoo.financialData.quickRatio.raw) {
      result = Object.values(dataYahoo.financialData.quickRatio);
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcDebtEquityYahoo () {
    let dataYahoo = this.state.yahooData;
    let result;
    if (dataYahoo.balanceSheetHistoryQuarterly && dataYahoo.balanceSheetHistoryQuarterly.balanceSheetStatements) {
      try {
        let longtermdebt = dataYahoo.balanceSheetHistoryQuarterly.balanceSheetStatements[0].longTermDebt.raw;
        let totalStockholderEquity = dataYahoo.balanceSheetHistoryQuarterly.balanceSheetStatements[0].totalStockholderEquity.raw;
        let ratio = longtermdebt / totalStockholderEquity;
        if (ratio > 0) {
          ratio = ratio.toFixed(2);
        }
        result = [ratio,ratio];
      } catch (e) {
        result = [0, 0];
      } 
    } else  {
      result = ["na", "na"];
    }
    return result;
  }

  calcInterestCoverateRatio () {
    let dataYahoo = this.state.yahooData;
    let result;
    let ratio = "na";
    if (dataYahoo.incomeStatementHistory && dataYahoo.incomeStatementHistory.incomeStatementHistory.length>0) {
      let interestExpense = 0;
      if (dataYahoo.incomeStatementHistory.incomeStatementHistory[0].interestExpense && dataYahoo.incomeStatementHistory.incomeStatementHistory[0].interestExpense.raw) {
        interestExpense = dataYahoo.incomeStatementHistory.incomeStatementHistory[0].interestExpense.raw;
      } 
      let ebit = dataYahoo.incomeStatementHistory.incomeStatementHistory[0].ebit.raw;
      ratio = interestExpense == 0 ? 0 : (ebit / interestExpense).toFixed(2);
      ratio = Math.abs(ratio);
    } else {
      ratio = "na";
    } 
    result = [ratio, ratio];
    return result;
  }

  calcFinancialLeverage () {
    let dataYahoo = this.state.yahooData;
    let result;
    let ratio = "na";
    if (dataYahoo.balanceSheetHistoryQuarterly && dataYahoo.balanceSheetHistoryQuarterly.balanceSheetStatements.length>0) {
      let totalStockholderEquity = dataYahoo.balanceSheetHistoryQuarterly.balanceSheetStatements[0].totalStockholderEquity ? dataYahoo.balanceSheetHistoryQuarterly.balanceSheetStatements[0].totalStockholderEquity.raw : "na";
      let totalAssets = dataYahoo.balanceSheetHistoryQuarterly.balanceSheetStatements[0].totalAssets.raw ? dataYahoo.balanceSheetHistoryQuarterly.balanceSheetStatements[0].totalAssets.raw : "na";
      if ((totalStockholderEquity !="na") || (totalAssets !="na")){
        ratio = (totalAssets / totalStockholderEquity).toFixed(2);
      } else {
        ratio = "na";
      }
    } else {
      ratio = "na";
    }
    result = [ratio, ratio];
    return result;
  }

  calcCashflowRatio () {
    let dataYahoo = this.state.yahooData;
    let result;
    if (dataYahoo.defaultKeyStatistics && dataYahoo.defaultKeyStatistics.netIncomeToCommon.raw && dataYahoo.financialData && dataYahoo.financialData.operatingCashflow.raw && dataYahoo.cashflowStatementHistory  && dataYahoo.cashflowStatementHistory.cashflowStatements.length>0 ) {
      let netIncome = dataYahoo.defaultKeyStatistics.netIncomeToCommon.raw;
      let freeCashflow = dataYahoo.financialData.operatingCashflow.raw;
      let capex = 0;
      if (dataYahoo.cashflowStatementHistory.cashflowStatements[0].capitalExpenditures.raw) {
        capex = dataYahoo.cashflowStatementHistory.cashflowStatements[0].capitalExpenditures.raw;
      }
      let ratio = ((freeCashflow - capex) / netIncome).toFixed(2);
      result = [ratio, ratio];
    }
    return result;
  }

  clearInput = ()=>{
    this.setState({searchTicker: ''});
  }
  

  render() {
    let typeFormula = this.state.typeFormula != "" ? this.state.typeFormula : "";
    let typeCompany ="";
    switch (typeFormula) {
      case 'formulaValue':
          typeCompany = "Value";
          break;
      case 'formulaGrowth':
          typeCompany = "Growth";
          break;    
      case 'formulaFinancial':
          typeCompany = "Financial";
          break;    
    }
    console.log("choosing formula " + typeCompany);
    const myVars = ['CAGR', 'EPS', 'GrossMargin', 'NetMargin', 'ROE', 
    'ROA', 'CurrentRatio', 'QuickRatio', 'Debt_Equity', 'FinancialLeverage', 'InterestCoverage', 'FreeCashFlow']; 
    const formula =  require('./Formula.json');
    const CAGR = this.calcCAGRYahoo();
    const EPS = this.calcEPSYahoo();
    const GrossMargin = this.calcGrossMarginYahoo();
    const NetMargin = this.calcNetMarginYahoo();
    const ROE = this.calcROEYahoo();
    const ROA = this.calcROAYahoo();
    const CurrentRatio = this.calcCurrentRatioYahoo();
    const QuickRatio = this.calcQuickRatioYahoo();
    const Debt_Equity = this.calcDebtEquityYahoo();
    const FinancialLeverage = this.calcFinancialLeverage();
    const InterestCoverage = this.calcInterestCoverateRatio();
    const FreeCashFlow = this.calcCashflowRatio();
    
    var dictVars = {};

    dictVars['CAGR'] = CAGR ? CAGR[0] : 0;
    dictVars['EPS'] = EPS ? EPS[0] : 0;
    dictVars['GrossMargin'] = GrossMargin ? GrossMargin[0] : 0;
    dictVars['NetMargin'] = NetMargin ? NetMargin[0] : 0;
    dictVars['ROE'] = ROE ? ROE[0] : 0;
    dictVars['ROA'] = ROA ? ROA[0] : 0;
    dictVars['CurrentRatio'] = CurrentRatio ? CurrentRatio[0] : 0;
    dictVars['QuickRatio'] = QuickRatio ? QuickRatio[0] : 0;
    dictVars['Debt_Equity'] = Debt_Equity ? Debt_Equity[0] : 0;
    dictVars['FinancialLeverage'] = FinancialLeverage ? FinancialLeverage[0] : 0;
    dictVars['InterestCoverage'] = InterestCoverage ? InterestCoverage[0] : 0;
    dictVars['FreeCashFlow'] = FreeCashFlow ? FreeCashFlow[0] : 0;
    console.log(dictVars);
    const listTickers =  require('./stockTickers.json').data;

    const stockUS = listTickers.filter(function(ticker) {
            return ticker.country == "United States";
    })
    if (this.state.typeFormula) {
      this.state.typeFormula = typeFormula
    } else {
      this.state.typeFormula = "formulaValue"
    }
    const categorieDescription = {
      "CAGR": {
          "name": "Revenue Growth%",
          "title": "Is the company able to grow its revenue annually?",
          "pass": "The company has positive revenue growth",
          "notpass": "The company has negative  revenue growth",
          "na" : "N/A"
      },
      "EPS": {
          "name": "EPS Growth%",
          "title": "Is the company able to grow its earning per share annually?",
          "pass": "The company has positive EPS growth",
          "notpass": "The company has negative  EPS growth",
          "na" : "N/A"
      },
      "GrossMargin": {
          "name": "Gross Margin",
          "title": "Is the company able to produce high gross margin?",
          "pass": " The company gross margin is higher than our treshold, the company could have high economic moat",
          "notpass": "The company gross margin is currently lower than our treshold",
          "na" : "N/A"
      },
      "NetMargin": {
          "name": "Net Margin",
          "title": "Is the company able to produce high net income margin?",
          "pass": "The company profitability is higher than our treshold",
          "notpass": "The company profitability is lesser than our treshold",
          "na" : "N/A"
      },
      "ROE": {
          "name": "ROE",
          "title": "Is the return of equity healthy?",
          "pass": "Return of equity going strong",
          "notpass": "Return of equity is low",
          "na" : "N/A"
      },
      "ROA": {
          "name": "ROA",
          "title": "Is the return of assets healthy?",
          "pass": "Return of assets going strong",
          "notpass": "Return of assets is low",
          "na" : "N/A"
      },
      "CurrentRatio": {
          "name": "Current Ratio",
          "title": "Is the short term asset healthy?",
          "pass": "short term asset is healthy enough to pay short term liability",
          "notpass": "short term asset is not enough to pay short term liability,it could lead to financial distress",
          "na" : "N/A"
      },
      "QuickRatio": {
          "name": "Quick Ratio",
          "title": "Is the short term asset healthy?",
          "pass": "short term asset is healthy enough to pay short term liability",
          "notpass": "short term asset is not enough to pay short term liability,it could lead to financial distress",
          "na" : "N/A"
      },
      "Debt_Equity": {
          "name": "Debt/ Equity",
          "title": "Is the company having healthy balance sheet?",
          "pass": "Balance sheet is strong and healthy",
          "notpass": "Balance sheet is unhealthy, high debt",
          "na" : "N/A"
      },
      "FinancialLeverage": {
          "name": "Financial Leverage",
          "title": "Is the company having healthy balance sheet?",
          "pass": "The company is low leverage, low debt",
          "notpass": "The company is having heavily indebt, high leverage",
          "na" : "N/A"
      },
      "InterestCoverage": {
          "name": "Interest Coverage",
          "title": "Is the interest coverage sufficient?",
          "pass": "The interest coverage is high, if NA or 0 means no debt",
          "notpass": "The interest coverage is low",
          "na" : "N/A"
      },
      "FreeCashFlow": {
          "name": "FreeCashFlow to net income",
          "title": "Is the company able to generate positive free cashflow from its net profit?",
          "pass": "Company is able to generate free cashflow from its net profit",
          "notpass": "Company is not able to generate free cashflow from its net profit",
          "na" : "N/A"
      }
    };

    let listChecklistItem = formula[this.state.typeFormula][0];
    
    return (
      <Grid container className="layout-content-wrapper"  justifyContent='center' alignContent='center' id="layout-content" >
        <Grid container className="autocomplete-wrapper"  alignContent='center' >
          <SearchAutocomplete 
                    fetchYahoo = {this.fetchYahoo}
                    clearInput = {this.clearInput}
          />
        </Grid>
        <Grid container className="layout-content-inside-wrapper" justifyContent='space-between' alignContent='center' >
          <Grid item xl={8} lg={8} md={8} sm={12} xs={12} className="leftSide">
              
              {(this.state.searchTicker.length > 0 && this.state.searchTicker != "" && this.state.typeFormula != "")
                ? 
              <Grid container className="postSearchTicker"  justifyContent='space-between' alignContent='center'>
                <Grid item xl={12} lg={12} md={12} sm={12} xs={12} className="postSearchTicker_SymbolOverview">
                  <AdvancedRealTimeChart 
                    style="1"
                    symbol = {this.state.searchTicker}
                    allow_symbol_change={true}
                    autosize
                    interval="1D"
                  />
                </Grid>
                <Grid item xl={12} lg={12} md={12} sm={12} xs={12} className="postSearchTicker_Checklist">
                  <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="caption table">
                        <caption>This automated risk checks will flag any failed checks as potential investment risks. <br /> A company which passes all our checks, however, is not 'risk free'. </caption>
                        <TableHead>
                            <TableRow>
                                <TableCell colSpan={3}>
                                    {this.state.searchTicker} Risk Checks {typeCompany !="Value" ? `( ${typeCompany} company )` : ""}
                                </TableCell>
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {Object.keys(listChecklistItem).map((keyName, keyIndex) => {
                                return (<ChecklistItem keyName={keyName} name={categorieDescription[keyName].name} value={dictVars[keyName]} operator={dictVars[keyName] == "na" ? "na" : formula[this.state.typeFormula][0][keyName].operator} limit={formula[this.state.typeFormula][0][keyName].limit} desc={categorieDescription[formula[this.state.typeFormula][0][keyName].title]}/>)
                            })}
                        </TableBody>
                    </Table>
                  </TableContainer>
                </Grid>
              </Grid>
                : 
                <Grid container className="preTickerContent" justifyContent='space-between' alignContent='center'>
                  <Grid container className="marketArea">
                    <Grid item xl={7} lg={7} md={12} sm={12} xs={12} className="marketAreaData">
                      <MarketData showFloatingTooltip></MarketData>
                    </Grid>
                    <Grid item xl={4} lg={4} md={12} sm={12} xs={12} className="marketAreaOverview">
                      <MarketOverview  showFloatingTooltip></MarketOverview>
                    </Grid>
                  </Grid>
                </Grid>
              } 
              

          </Grid>
          <Grid item xl={3} lg={3} md={3} sm={12} xs={12} className="rightSide">
            {(this.state.searchTicker.length > 0 && this.state.searchTicker != "" && this.state.typeFormula != "")
                ?  
              <div className="timelineChart">
                <Timeline symbol={this.state.searchTicker}></Timeline>
                <Carousel autoPlay showStatus={false} infiniteLoop showArrows={false} indicators={false} interval={6000}  > 
                    <div className="leftSideChart">
                      <FundamentalData symbol={this.state.searchTicker}></FundamentalData>
                    </div>
                    <div className="leftSideChart">
                      <CompanyProfile symbol={this.state.searchTicker}></CompanyProfile>
                    </div>
                </Carousel>
              </div>
              :
              <Carousel autoPlay  infiniteLoop showArrows={false} indicators={false} interval={6000}  > 
                <div className="chartBar rightSideChart">
                  <ChartBar />
                </div>
                <div className="economicCalendar rightSideChart">
                <EconomicCalendar />
                </div>
              </Carousel>
            }
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default Checklist;