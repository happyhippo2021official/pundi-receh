import React from 'react'
import {Grid, TableContainer, Paper, Table, TableHead,TableBody, TableRow, TableCell} from '@material-ui/core'
import { AdvancedRealTimeChart, FundamentalData, CompanyProfile} from "react-ts-tradingview-widgets";

// const checkNoDebt = (a) => {
//     return a <=0.1 ? a = "No debt" : a;
// }

const CheckListPostView = (props) => {
    
    let finalValue;

    return (
        <Grid container className="postSearchTicker"  style={{gap: 50}} justifyContent='space-between' alignContent='center'>
            <Grid item xl={12} lg={12} md={12} sm={12} xs={12} className="postSearchTicker_SymbolOverview">
            <AdvancedRealTimeChart 
                style="1"
                symbol = {this.state.TVticker}
                allow_symbol_change={true}
                autosize
                interval="1D"
            />
            </Grid>
            <Grid item xl={6} lg={6} md={6} sm={12} xs={12} className="postSearchTicker_Checklist">
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="caption table">
                    <caption>This automated risk checks will flag any failed checks as potential investment risks. <br /> A company which passes all our checks, however, is not 'risk free'. </caption>
                    <TableHead>
                        <TableRow>
                            <TableCell colSpan={3}>
                                {this.state.searchTicker} Risk Checks {typeCompany !="Value" ? `( ${typeCompany} company )` : ""}
                            </TableCell>
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {Object.keys(listChecklistItem).map((keyName, keyIndex) => {
                            return (<ChecklistItem keyName={keyName} name={categorieDescription[keyName].name} value={dictVars[keyName]} operator={dictVars[keyName] == "na" ? "na" : formula[this.state.typeFormula][0][keyName].operator} limit={formula[this.state.typeFormula][0][keyName].limit} desc={categorieDescription[formula[this.state.typeFormula][0][keyName].title]}/>)
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            </Grid>
            <Grid item xl={5} lg={5} md={5} sm={12} xs={12} className="postSearchTicker_Info">
            <Carousel autoPlay showStatus={false} infiniteLoop showArrows={false} indicators={false} interval={6000}  > 
                <div className="leftSideChart">
                <FundamentalData symbol={this.state.searchTicker}></FundamentalData>
                </div>
                <div className="leftSideChart">
                <CompanyProfile symbol={this.state.searchTicker}></CompanyProfile>
                </div>
            </Carousel>
            </Grid>
        </Grid>
    );
  };
  
export default CheckListPostView;