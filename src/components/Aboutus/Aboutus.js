import react from 'react'
import {Grid, Typography} from '@material-ui/core'
import useStyles from '../../resources/css/Styles'
import {useTranslation} from 'react-i18next'
import Bigcard from './Bigcard'
import Smallcard from './Smallcard'

export default function Aboutus(){
    const useStyle = useStyles()
    const top2data = [
        {id:'1',name:'Donny Justin', title:'Founder, Managing Director', subtitle:'Completed 3 levels of CFA Program, Financial Educator, Blogger, Investor', direction:'row', imgsrc:'./DonnyJ.jpeg'},
        {id:'2',name:'Irene Anwar', title:'CoFounder, General Manager', subtitle:'Podcaster Host, Investor, Finance Enthusiast', direction:'row-reverse', imgsrc:'./IreneA.JPG'},
    ]
    const membersdata = [
        {id:'m1',name:'Mariana', title:'Corporate Secretary', subtitle:'Finance Enthusiast', imgsrc:'./Mariana.jpeg'},
        {id:'m2',name:'Nuraziz Yosokumoro', title:'Stock Analyst Head', subtitle:'Independent stock analyst, CFA Institute Investment Foundation Certificate', imgsrc:'./NurazizY.jpeg'},
        {id:'m3',name:'Made Adi', title:'Stock Analyst Head', subtitle:'Crypto Enthusiast', imgsrc:'./made.jpg'},
        // {id:'m4',name:'Stephanus Herwinoto', title:'Business Developer and Brand Manager', subtitle:'Former Kompas Group Director', imgsrc:'./Totot.png'},
        // {id:'m5',name:'Lorraine Suteja', title:'Digital Marketing Officer', subtitle:'Digital Marketing Enthusiast', imgsrc:'./Unknown.jpeg'},
        {id:'m6',name:'Siegfried T S', title:'Head Developer', subtitle:'Pundi Receh Lead Dev, Fullstack Developer and Certified Google Analyst', imgsrc:'./stevejob.PNG'},
    ]
    const {t} = useTranslation()
    return(
        <>
            <Grid container className={`${useStyle.aboutOutsideContainer} ${useStyle.topLayerEffect} ${useStyle.darkerBackground}`} justifyContent='center'>
                <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                    <Grid container spacing={5} justifyContent='center'>
                        <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                            <Grid container justifyContent='center'>
                                <Grid item sm={11}>
                                    <Typography variant='h2' className={`${useStyle.subTitle} ${useStyle.gradientText} ${useStyle.gradientTextColor3}`} align='center'>Pundi Receh</Typography>
                                    <Typography variant='h6' align='center'>
                                        {t('aboutSubTitle')}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xl={5} lg={5} md={5} sm={11} xs={11}>
                            <Grid container justifyContent='center'>
                                <Grid item sm={11}>
                                    <Typography variant='h4' className={`${useStyle.gradientText} ${useStyle.gradientTextColor3} ${useStyle.subTitle}`} align='center'>Vision</Typography>
                                    <Typography variant='h6' align='center'>
                                        {t('aboutVision')}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xl={5} lg={5} md={5} sm={11} xs={11}>
                            <Grid container justifyContent='center'>
                                <Grid item sm={11}>
                                    <Typography variant='h4' className={`${useStyle.gradientText} ${useStyle.gradientTextColor3} ${useStyle.subTitle}`} align='center'>Mission</Typography>
                                    <Typography variant='h6' align='center'>
                                        {t('aboutMission')}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                            <Grid container className={useStyle.darkerBackground} justifyContent='center'>
                                <Grid item sm={11}>
                                    <Typography className={useStyle.subTitle} variant='h6' align='center'>
                                        {t('aboutParagraph')}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                            <Grid container spacing={2} justifyContent='center'>
                                {top2data.map((obj)=>{
                                    return(
                                        <Grid item sm={11}>
                                            <Bigcard 
                                                direction={obj.direction} 
                                                title={obj.title} 
                                                subtitle={obj.subtitle} 
                                                name={obj.name}
                                                imgsrc={obj.imgsrc}/>
                                        </Grid>
                                    )
                                })}
                            </Grid>
                        </Grid>
                        <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                            <Grid container >
                                {membersdata.map((obj)=>{
                                    return(
                                        <Grid item lg={4} md={4} sm={5} xs={5}>
                                            <Smallcard
                                                title={obj.title} 
                                                subtitle={obj.subtitle} 
                                                name={obj.name}
                                                imgsrc={obj.imgsrc}/>
                                        </Grid>
                                    )
                                })}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </>
    )
}