import React, {useState, useEffect} from 'react'
import {Grid, Typography, Button, TextField} from '@material-ui/core'
import useStyles from '../../resources/css/Styles'
import {useTranslation} from 'react-i18next'
import Bigcard from './Bigcard'
import Smallcard from './Smallcard'
import FeatureItem from './FeatureItem'
import Logo from '../../resources/images/header/logo2.png'
import iconTest1 from '../../resources/images/icon1.png';
import iconTest2 from '../../resources/images/icon2.png';
import Cookies from 'universal-cookie'
import {UpdateUserData} from '../../databases/Update'

export default function Myprofile(){
    const useStyle = useStyles()
    const cookies = new Cookies()
    const [userData, setUserData] = useState(false)
    const [editMode, setEditMode] = useState(false)
    const [email, setEmail] = useState('')
    const [verified, setVerified] = useState(false)
    const [call, setCall] = useState('')
    // const {email, name} = userData;

    useEffect(()=>{
        if(cookies.get('Loggedin')){
          console.log(`Logged in already -> ${cookies.get('Loggedin', {doNotParse: true})}`)
          setUserData(cookies.get('Loggedin'))
        }
    },[])

    useEffect(() => {
        setEmail(userData.email);
        setCall(userData.call);
        setVerified(userData.verified);
    }, [userData]);

    useEffect(() => {
        if(editMode == false) {
            console.log("this is false");
        } else {
            console.log("this is true");
        }
            
    }, [setEditMode]);

    async function saveData(){
        let id = 0;
        if(!id){

        }else{
            // UpdateUserData(id, wholeData).then((response)=>{
            //     console.log('finished update')
            // })
        }
        
    }

    const plan = [
        {id:'basic1',name:'Basic Plan', imgsrc:'./static/media/logo2.50605d16.png', features:[{"name":"Analysis pick", "imgIconSRC":"../../resources/images/icon2.png"},{"name":"Trend analysis", "imgIconSRC":"../../resources/images/icon2.png"},{"name":"Sentiment analysis", "imgIconSRC":"../../resources/images/icon2.png"}]}
    ]

    const planDetail = [
        {id:'basic1',name:'Basic Plan', imgsrc:'./static/media/logo2.50605d16.png', detail:[{"name":"Visa ending xxxxx2381", "imgIconSRC":"../../resources/images/icon2.png"},{"name":"$20 monthly", "imgIconSRC":"../../resources/images/icon2.png"},{"name":"Next payment 20 July 2022", "imgIconSRC":"../../resources/images/icon2.png"}]}
    ]
    const {t} = useTranslation()
    return(
        <>
            <Grid container className={`${useStyle.aboutOutsideContainer} ${useStyle.topLayerEffect} ${useStyle.darkerBackground}`} justifyContent='center'>
                <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                    <Grid container spacing={5} justifyContent='center'>
                        <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                            <Grid container justifyContent='center'>
                                <Grid item sm={11}>
                                    <Typography variant='h2' className={`${useStyle.subTitle} ${useStyle.gradientText} ${useStyle.gradientTextColor3}`} align='center'>{t('myprofileTitle')}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                            <Grid container justifyContent='center'>
                                <Grid item sm={11}>
                                    <Typography className={`${useStyle.capitalized}`} variant='h4'>Welcome, {call}</Typography>
                                    {editMode === false ? 
                                        <>
                                            <Button variant='contained' color='primary' onClick={()=>setEditMode(!editMode)}>Edit profile</Button>
                                        </> 
                                        :
                                        <>
                                            <Button variant='contained' color='primary' onClick={()=>setEditMode(!editMode)}>Save profile</Button>
                                        </> 
                                    }
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                            <Grid container justifyContent='center'>
                                <Grid item sm={11}>
                                    Status : {verified === true ? 'Verified': 'Not verified'}
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                            <Grid container justifyContent='center'>
                                <Grid item sm={11}>
                                    {editMode === false ? 
                                        <>
                                            Email : {email}
                                        </> 
                                        :
                                        <>
                                            <TextField className={`${useStyle.myInputBox}`} fullWidth value={email} type='input' onChange={(event)=>setEmail(event.target.value)} label='email' />
                                        </> 
                                    }
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                            <Grid container justifyContent='center'>
                                <Grid item sm={11}>
                                    <Typography variant='h4' className={`${useStyle.gradientText} ${useStyle.gradientTextColor3} ${useStyle.subTitle}`}>
                                        {t('yourPlanTitle')} - {userData.level}
                                    </Typography>
                                    <Grid container justifyContent='space-between'>
                                        <Grid item xl={3} lg={3} md={3} sm={12} xs={12} className={useStyle.itemDistance}>
                                            {plan[0].name}
                                            <Grid container className={useStyle.logoContainer}>
                                                <img src={plan[0].imgsrc} className={useStyle.logo}/>
                                            </Grid>
                                        </Grid>
                                        <Grid item xl={4} lg={4} md={4} sm={12} xs={12} className={useStyle.itemDistance}>
                                            <Typography variant='h6' className={`${useStyle.subTitle} ${useStyle.gradientText} ${useStyle.gradientTextColor3}`}>{t('includedTitle')}</Typography>
                                            {plan[0].features.map((feat,featIndex)=>{
                                                return (<FeatureItem key={featIndex} features={feat}/>)
                                            })}
                                        </Grid>
                                        <Grid item xl={4} lg={4} md={4} sm={12} xs={12} className={useStyle.itemDistance}>
                                            <Typography variant='h6' className={`${useStyle.subTitle} ${useStyle.gradientText} ${useStyle.gradientTextColor3}`}>{t('billingTitle')}</Typography>
                                            {planDetail[0].detail.map((det,detIndex)=>{
                                                return (<FeatureItem key={detIndex} features={det}/>)
                                            })}
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>

                    </Grid>
                </Grid>
            </Grid>
        </>
    )
}