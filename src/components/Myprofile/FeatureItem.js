import React from 'react'
import {Grid, Typography, Divider} from '@material-ui/core'
import useStyles from '../../resources/css/Styles'
import iconTest1 from '../../resources/images/icon2.png';

const FeatureItem = (props) => {
    const useStyle = useStyles()
    return(
        <>
            <Grid container justifyContent='center' className={useStyle.featuresPlanItem}>
                <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                    <Grid container justifyContent='flex-start' className={useStyle.middleVert}>
                        <Grid item sm={2}>
                            <Grid container justifyContent='center' className={useStyle.planContainer}>
                                <img className={useStyle.followContainer} src={iconTest1}/>
                            </Grid>
                        </Grid>
                        <Grid item sm={9}>
                            <Typography  className={`${useStyle.middleVert} ${useStyle.spacingImageTextRow}`} variant='h6'>{props.features.name}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </>
    )
}

export default FeatureItem;