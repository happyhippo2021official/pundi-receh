import React from 'react'
import {Grid, Typography, Divider} from '@material-ui/core'
import useStyles from '../../resources/css/Styles'

const Bigcard = (props) => {
    const useStyle = useStyles()
    const images = require.context('../../resources/images/about', true);
    return(
        <>
            <Grid container direction={props.direction} justifyContent='center'>
                <Grid item xl={3} lg={3} md={3} sm={6} xs={6}>
                    <Grid container justifyContent='center'>
                        <Grid item sm={12}>
                            <Grid container justifyContent='center' className={useStyle.aboutPictContainer}>
                                <img className={useStyle.aboutPict} src={images(`${props.imgsrc}`).default}/>
                            </Grid>
                        </Grid>
                        <Grid item sm={12}>
                            <Typography variant='h6' align='center' className={`${useStyle.subTitle} ${useStyle.gradientText} ${useStyle.gradientTextColor3}`}>{props.name}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xl={7} lg={7} md={7} sm={6} xs={6}>
                    {/* text */}
                    <Grid container justifyContent='center'  alignItems='center' style={{height:'100%'}}>
                        <Grid item md={8} sm={12}>
                            <Typography align='center'>{props.title}</Typography>
                            <Divider/>
                            <Typography align='center'>{props.subtitle}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </>
    )
}

export default Bigcard;