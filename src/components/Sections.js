import React from 'react'
import { Chip, Typography, Grid, Button, CardMedia} from '@material-ui/core'
import useStyles from '../resources/css/Styles'
import '../resources/css/dashboard.css'
import imgbg from '../resources/images/bnt.png'
import imgbg2 from '../resources/images/performance.png'
import vision from '../resources/images/vision.jpg'
import mission from '../resources/images/mission.jpg'

export default function Sections(){
    // to be removed after made into components
    const tags = [{value:'AAPL'},{value:'TSLA'},{value:'PLTR'}];
    const tags2 = [{value:'PREMIUM'},{value:'DAILY UPDATES'},{value:'PORTOFOLIO'}];
    const tags3 = [{value:'PREMIUM'},{value:'LONG TERM'},{value:'BLUE CHIPS'},{value:'PORTOFOLIO'}];
    const intro = [{size:'h3',text:'Pundi Receh'},
    {size:'h5',text:'All about Finance and Investment'},
    {size:'h4',text:'Vision'},
    {size:'h5',text:'Everyone has the right to get affordable financial education '},
    {size:'h4',text:'Mission'},
    {size:'h5',text:'To create opportunity for everyone to get financial education for their better future in fun, easy to understand and safer way'},]
    const useStyle = useStyles();
    return (
        <Grid container style={{gap:50}} className={useStyle.sectionContainer} className={useStyle.topLayerEffect}>
            {/* intro */}
            {/* <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                {intro.map( item => (
                    <Typography align='center' variant={item.size}>{item.text}</Typography>
                ))}
            </Grid> */}


            {/* 1st Section */}
            <Grid container justifyContent='center'>
                <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                    <Grid container style={{gap:20}} justifyContent='center'>
                        <Grid item component='div' xl={4} lg={4} md={4} sm={12} xs={12} className={useStyle.imgSection}>
                            <img src={imgbg} className={useStyle.cardMedia} />
                        </Grid>
                        <Grid item component='div' xl={5} lg={5} md={5} sm={12} xs={12} className={useStyle.textSection}>
                            <Grid container direction='column' spacing={1}>
                                <Grid item className={useStyle.fullWidth}>
                                    <Typography variant ='h2'>
                                        Recommended Picks This Week
                                    </Typography>
                                </Grid>
                                <Grid>
                                    <Grid container spacing={1} alignItems='center'>
                                        {tags.map(tag =>(
                                            <Grid item>
                                                <Chip label={tag.value} color='secondary' size='small'>
                                                </Chip>
                                            </Grid>
                                        ))}
                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <Typography variant='h6'>
                                    Setelah analisa fundamental dan analisa teknikal (Technical Analysis) pergerakan harga APPLE (NASDAQ:AAPL), dan TESLA (NYSE:TSLA) selama beberapa bulan terakhir, kami memutuskan untuk menaikkan rating mereka menjadi BUY dan memasukan ke dalam portfolio kami. Ada Elliot Wave pattern, dengan Elliot Wave ke-5 memiliki TP (Target Price) sangat tinggi
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Grid container>
                                        <Button variant='contained' className='btnMore sizeAble'>More Info</Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>

            {/* 2nd Section */}
            <Grid container justifyContent='center'>
                <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                    <Grid container style={{gap:20}} justifyContent='center'>
                        <Grid item component='div' xl={5} lg={5} md={5}  sm={12} xs={12} className={useStyle.textSection}>
                            <Grid container direction='column' spacing={1}>
                                <Grid className={useStyle.fullWidth}>
                                    <Typography variant ='h2'>
                                        Outstanding Portofolio Performance
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Grid container spacing={1} alignItems='center'>
                                        {tags2.map(tag =>(
                                            <Grid item>
                                                <Chip label={tag.value} color='secondary' size='small'>
                                                </Chip>
                                            </Grid>
                                        ))}
                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <Typography variant='h6'>
                                    Pundi Receh ada di Tipranks.com untuk membandingkan diri kami dengan analis kelas dunia dan benchmark S&P500. Hasilnya Pundi Receh masih sejajar dengan Tipranks dan S&P500 untuk Bulan September.
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Grid container>
                                        <Button variant='contained' className='btnMore sizeAble'>More Info</Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item component='div' xl={4} lg={4} md={4} sm={12} xs={12} className={useStyle.imgSection}>
                            <img src={imgbg2} className={useStyle.cardMedia}/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>

            {/* 3rd Section */}
            <Grid container justifyContent='center'>
                <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                    <Grid container style={{gap:20}} justifyContent='center'>
                        <Grid item component='div' xl={4} lg={4} md={4} sm={12} xs={12} className={useStyle.imgSection}>
                            <img src={imgbg} className={useStyle.cardMedia} />
                        </Grid>
                        <Grid item component='div' xl={5} lg={5} md={5}  sm={12} xs={12} className={useStyle.textSection}>
                            <Grid container direction='column' spacing={1}>
                                <Grid item>
                                    <Typography variant ='h2'>
                                        Long-Term Stocks List
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Grid container spacing={1} alignItems='center'>
                                        {tags.map(tag =>(
                                            <Grid item>
                                                <Chip label={tag.value} color='secondary' size='small'>
                                                </Chip>
                                            </Grid>
                                        ))}
                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <Typography variant='h6'>
                                    Invest in blue chips and low risk stocks picked by our analysts. Our recommendations will help direct you to grow and build a portfolio for the future.
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Grid container>
                                        <Button variant='contained' className='btnMore sizeAble'>More Info</Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            
        </Grid>
    )
}