import React from 'react'
import { useState } from 'react';
import {Grid, Card, Typography, CardHeader, Avatar, CardContent} from '@material-ui/core'
import '../../resources/css/stockItem.css'

const TestimonyCard2 = (props) => {
    return (
        <Card style={{height:'100%'}}>
            <CardHeader
            avatar={<Avatar aria-label="recipe">
                {props.data.init}
            </Avatar>}
            title={props.data.name}
            subheader={props.data.title}
            />
            <CardContent>
                <Typography>
                    {props.data.article}
                </Typography>
            </CardContent>

        </Card>
    );
  };
  
export default TestimonyCard2;