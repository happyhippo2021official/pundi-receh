import React from 'react'
import { useState } from 'react';
import {Grid, Card, Typography, CardHeader, Avatar} from '@material-ui/core'
import '../../resources/css/stockItem.css'
import SingleCard from './TestimonyCard'
import useStyles from '../../resources/css/Styles'
import { Carousel } from 'react-responsive-carousel'
import "react-responsive-carousel/lib/styles/carousel.min.css";

const TestiSection = (props) => {
    const datas=[
        {init:'AL', name:'Astrid Laurentia', title:'ZTH Member', article:'Akhirnya ketemu teman ynag mau ajar saham betul-betul untuk pemula. Diajarin step by step. Lalu bisa ditanya langsung dan betul-betul mau mengajar. Biasanya yang mengajar saham itu ngomongnya udah "tinggi banget" buat aku gagal paham hahaha..'},
        {init:'N', name:'Nicke', title:'ZTH Member', article:'Bagus banget!! Karena bisa educate orang agar mengerti tentang saham. Padahal awalnya dikira broker yang ujung-ujungnya minta orang buat join.'},
        {init:'D', name:'Dina', title:'ZTH Member', article:'Recommended!! Sekarang jadi tahu istilah MA MACD RSI Elliot Wave dan FIB. Clear banget penjelasannya, padahal saya tidak ada background TA.. Well spent time and money!'},
        {init:'P', name:'Petrus', title:'ZTH Member', article:'Terima Kasih sudah diinvite, benar-benar bukan materi receh!'},
        {init:'IH', name:'Iwan Haryadi', title:'ZTH Member', article:'Setelah mendapat pengetahuan dari kelas analisis Fundamental jadi lebih mantap dan ngerti untuk eksekusi saham. Apalagi kelas Analisis Teknikalnya, saya sudah beberapa coba menerapkan dan memang terbukti bukan kaleng-kaleng, hahahaha...'},
        {init:'MW', name:'Mariana Widjaja', title:'ZTH Member', article:'Uda sering diajakin ato ditawarin buat investasi sama temen ataupun financial planner. Tapi baru berani nyemplung ya karena ikut kelas ZTH-nya Pundi Receh. Donny dan Irene bener-bener genuine buat bantu partisipan supaya melek financial. Post class follow up-nya juga pakem dan menyeluruh jadi berasa selalu ada support dari komunitas. Im so grateful to join this class and looking forward for other sessions! Biar makin pinter invest pastinya. Thank you, Pundi Receh, for helping me breaking the barrier so I can start investing now'},
    ]
    const useStyle = useStyles()
    return (
        <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
            <Grid container className={useStyle.darkerBackground} style={{gap:'1vw'}} justifyContent='center' alignContent='center' alignItems='center'>
                <Grid item xl={4} lg={4} md={5} sm={11} xs={11}>
                    <Grid container justifyContent='center' style={{height:'20vh', minHeight:150, maxHeight:200}}>
                        <Typography className={`${useStyle.subTitle} ${useStyle.LatoHeadline}`} variant='h3' align='center'>
                            Our
                            <br />
                            Member's
                            <br/>
                            Testimonial
                        </Typography>
                    </Grid>
                </Grid>
                <Grid item xl={4} lg={4} md={4} sm={11} xs={11}>
                    {/* <Grid container style={{gap:20}} justifyContent='center' alignItems='strecth'> */}
                    <Carousel autoPlay centerMode infiniteLoop>
                        {datas.map(data=>(
                            <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                                <SingleCard data={data}/>
                            </Grid>
                        ))}
                    </Carousel>
                    {/* </Grid> */}
                </Grid>
            </Grid>
        </Grid>
    );
  };
  
export default TestiSection;