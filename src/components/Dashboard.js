import React from 'react'
import { Typography, Grid,CardMedia, Button, TextField,} from '@material-ui/core'
import useStyles from '../resources/css/Styles'
import Sections from './Sections'
import Testimony from './Testimony/TestiSection'
import Whyus from './Whyus/Whyus'
import Showcase from './Showcase/ShowSection'
import laptop from '../resources/images/laptop.png'
import phone from '../resources/images/phone.png'
import icon1 from '../resources/images/1.png'
import icon2 from '../resources/images/2.png'
import icon3 from '../resources/images/3.png'
import icon4 from '../resources/images/4.png'
import icon5 from '../resources/images/5.png'
import icon6 from '../resources/images/6.png'
import {useTranslation} from "react-i18next";

export default function Dashboard(){
    const { t } = useTranslation();
    const useStyle = useStyles();
    const potentials = [{icon:icon1,text: t('potential1')},
    {icon:icon2,text:t('potential2')},
    {icon:icon3,text:t('potential3')},
    {icon:icon4,text:t('potential4')},
    {icon:icon5,text:t('potential5')},
    {icon:icon6,text:t('potential6')}];
    return(
        <Grid container justifyContent='center' alignContent='center' className={`${useStyle.topLayerEffect} ${useStyle.Roboto}`}>
            {/* top pict */}
            <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                <Grid container spacing={1} className={useStyle.topSectionContainer} justifyContent='center' alignContent='center'>
                    <Grid item xl={10} lg={10} md={10}>
                        <Grid container justifyContent='flexStart' alignItems='center'>
                            <Typography variant='h2'> Your Financial Journey Starts Here</Typography>
                            <Typography variant='subtitle1'>Register to get your personal financial checklist</Typography>
                        </Grid>
                    </Grid>
                    <Grid item xl={10} lg={10} md={10}>
                        <Grid container justifyContent="space-between" alignItems="center">    
                            {/* Form */}
                            <Grid item md={5} xs={10}>
                                <Grid container spacing={2} justifyContent='center' alignItems='center'>
                                    <Grid item sm={12}>
                                        <TextField fullWidth InputProps={{ className: useStyle.topInput }} label="First Name" variant="outlined" margin='dense'/>
                                    </Grid>
                                    <Grid item sm={12}>
                                        <TextField fullWidth InputProps={{ className: useStyle.topInput }} label="Last Name" variant="outlined" margin='dense'/>
                                    </Grid>
                                    <Grid item sm={12}>
                                        <TextField fullWidth InputProps={{ className: useStyle.topInput }} label="Email" variant="outlined" margin='dense'/>
                                    </Grid>
                                    <Grid item sm={12}>
                                        <Button fullWidth className={`${useStyle.gradientButtonStyle2} ${useStyle.Roboto}`}>Subscribe</Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                            {/* Pictures */}
                            <Grid item md={6} xs={0}>
                                <Grid container justifyContent='center' align='center' className={useStyle.topSectionPictureContainer}>
                                    <img src={laptop} className={useStyle.laptopPict}/>
                                    <Grid item className={useStyle.phonePictContainer}>
                                        {/* <img src={phone} className={useStyle.phonePict}/> */}
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                {/* <Grid container spacing={10} justifyContent='space-around' className={useStyle.gradientBackground}>
                    <Grid item xl={4} lg={4} md={4} sm={12} xs={12}>
                        <Grid container spacing={2} justifyContent='flex-end' alignContent='center' style={{height:'100vh'}}>
                            <Grid item sm={12}>
                                <Typography align='right' variant='h3' className={`${useStyle.Roboto}`}>Learn while you earn</Typography>
                            </Grid>
                            <Grid item sm={8}>
                                <Grid container>
                                    <Grid item sm={12}>
                                        <TextField fullWidth InputProps={{ className: useStyle.topInput }} label="First Name" variant="outlined" margin='dense'/>
                                    </Grid>
                                    <Grid item sm={12}>
                                        <TextField fullWidth InputProps={{ className: useStyle.topInput }} label="Last Name" variant="outlined" margin='dense'/>
                                    </Grid>
                                    <Grid item sm={12}>
                                        <TextField fullWidth InputProps={{ className: useStyle.topInput }} label="Email" variant="outlined" margin='dense'/>
                                    </Grid>
                                    <Grid item sm={12}>
                                        <Button fullWidth className={`${useStyle.gradientButtonStyle2} ${useStyle.Roboto}`}>Subscribe</Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xl={7} lg={7} md={7} sm={0} xs={0}>
                        <Grid container justifyContent="flex-start" alignItems="center" className={useStyle.fullWide}>
                            <img src={phone} className={useStyle.cardMedia}/>
                        </Grid>
                    </Grid>
                </Grid> */}
            </Grid>
            <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                {/* <Sections /> */}
            </Grid> 
            <Showcase />
            <Whyus />
            <Testimony />
            <Grid item  xl={12} lg={12} md={12} sm={12} xs={12} className={useStyle.aboveFooter}>
                <Grid container className={useStyle.lighterBackground} spacing={10} className='gridWrapper'>
                    <Grid item component='div' xl={12} lg={12} md={12} sm={12} xs={12} className="gridTitle">
                        <Typography variant='h2' classname={useStyle.Roboto}>Unlock More Potentials</Typography>
                        <Typography variant='h4' classname={useStyle.Roboto}>with Pundi Receh Premium features</Typography>
                    </Grid>
                    <Grid item xl={8} lg={8} md={12} sm={12} xs={12}>
                        <Grid container spacing={10} className="gridContent">
                            {potentials.map(potent =>(
                                <Grid item xl={4} lg={4} md={4} sm={12} xs={12}>
                                    <Grid container justifyContent='center' alignContent='center' className={useStyle.potentialContainer}>
                                        <Grid item sm={12} className="gridItemImgContainer">
                                            <Grid container justifyContent='center'>
                                                <Grid container alignContent='center' justifyContent='center'>
                                                    <img src={potent.icon} className={useStyle.cardItemMedia}/>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item sm={12} className="gridItemTextContainer">
                                            <Typography variant='h6' align='center' className={useStyle.Roboto}>{potent.text}</Typography>
                                        </Grid>                        
                                    </Grid>
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}