import React from 'react'
import {Grid, TextField} from '@material-ui/core'
import useStyle from '../../resources/css/components/Dashboard/Why'


import name from '../../resources/images/header/name.png'

export default function Welcome() {
    const classes = useStyle()
    const Datas = [
        {no:1, text:'Affordable', subtext:'We help you get the most affordable financial classes  for individuals and professionals. Numerous members are from zero to hero. The next is you. '},
        {no:2, text:'Fundamental & Technical', subtext:'Learn from the basic, combining both fundamental  and technical to ensure you get the whole  understanding of the financial market.'},
        {no:3, text:'Professional Mentor', subtext:'Taught by Donny Justin, blogger and practitioner of financial market with experiences and 3 CFA  certification and level 1 CMT certification.'},
        {no:4, text:'2 Languages Available', subtext:'Learn the financial market in 2 languages, available both in Bahasa and English. '},
    ]
  return (
      <Grid container alignItems='center'>
          <Grid item xs={6}>
              <Grid container justifyContent='center' className={classes.textContainer}>
                Why &nbsp;<img src={name} className={classes.name}/>&nbsp; ?
              </Grid>
          </Grid>
          <Grid item xs={6}>
                {Datas.map((data)=>(
                    <Grid container className={classes.dataTextContainer}>
                        <Grid key={data.no} container className={classes.text}>
                            {data.text}
                        </Grid>
                        <Grid container className={classes.subText}>
                            {data.subtext}
                        </Grid>
                    </Grid>
                ))}
          </Grid>
      </Grid>
  )
}
