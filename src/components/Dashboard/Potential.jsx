import React from 'react'
import {Grid, TextField} from '@material-ui/core'
import useStyle from '../../resources/css/components/Dashboard/Potential'

import {FaChartBar, FaChartLine} from 'react-icons/fa'
import Up from '../../../src/resources/images/svg/Up'
import Stat from '../../../src/resources/images/svg/Statistics'

export default function Potential(){
    const classes = useStyle()
    const points = [
        {
            code:'exp',
            title:'Expert Market Analysis',
            subtitle:'Get the deep analysis from our expert for your reference.',
            icon:FaChartLine,
        },
        {
            code:'fnt',
            title:'Fundamental and Technical',
            subtitle:'We provide the comprehensive tools both fundamental and technical to help you make your decision',
            icon:FaChartBar,
        }
    ]
    return(
        <Grid container>
            <Grid container justifyContent='center' className={classes.title}>
                Unlock More Potentials
            </Grid>
            <Grid container justifyContent='center' className={classes.subtitle}>
                With Pundi Receh Premium Features
            </Grid>
            <Grid container justifyContent='space-around' className={classes.points}>
                {points.map((point)=>{
                    return(
                        <Grid item md={5} xs={11} className={classes.point}>
                            <Grid container justifyContent='center' className={classes.iconContainer}>
                                <Grid container justifyContent='center' alignItems='center' className={classes.iconCircle}>
                                    <point.icon className={classes.icon}/>
                                </Grid>
                            </Grid>
                            <Grid container justifyContent='center' className={classes.pointsTitle}>
                                {point.title}
                            </Grid>
                            <Grid container justifyContent='center' className={classes.pointsSubtitle}>
                                {point.subtitle}
                            </Grid>
                        </Grid>
                    )
                })}
            </Grid>
            <Grid container justifyContent='center'>
                <Grid container justifyContent='center' className={classes.button}>
                    Subscribe
                </Grid>
            </Grid>
        </Grid>
    )
}