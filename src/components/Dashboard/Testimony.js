import React from 'react'
import {Grid} from '@material-ui/core'
import useStyles from '../../resources/css/components/Dashboard/Testimony'

import DefP from '../../resources/images/default.png'

export default function Performance() {
    const classes = useStyles()
    const datas=[
        {no:1,init:'AL',name:'Astrid Laurentia',status:'ZTH Member',pict:DefP,text:'Akhirnya ketemu teman ynag mau ajar saham betul-betul untuk pemula. Diajarin step by step. Lalu bisa ditanya langsung dan betul-betul mau mengajar. Biasanya yang mengajar saham itu ngomongnya udah "tinggi banget" buat aku gagal paham hahaha..'},
        {no:2,init:'N',name:'Nicke',status:'ZTH Member',pict:DefP,text:'Bagus banget!!Karena bisa educate orang agar mengerti tentang saham. Padahal awalnya dikira broker yang ujung-ujungnya minta orang buat join.'},
        {no:3,init:'D',name:'Dina',status:'ZTH Member',pict:DefP,text:'Recommended!! Sekarang jadi tahu istilah MA MACD RSI Elliot Wave dan FIB. Clear banget penjelasannya, padahal saya tidak ada background TA.. Well spent time and money!'},
        {no:4,init:'P',name:'Petrus',status:'ZTH Member',pict:DefP,text:'Terima Kasih sudah diinvite, benar-benar bukan materi receh!'},
        {no:5,init:'IH',name:'Iwan Haryadi',status:'ZTH Member',pict:DefP,text:'Setelah mendapat pengetahuan dari kelas analisis Fundamental jadi lebih mantap dan ngerti untuk eksekusi saham. Apalagi kelas Analisis Teknikalnya, saya sudah beberapa coba menerapkan dan memang terbukti bukan kaleng-kaleng, hahahaha...'},
        {no:6,init:'MW', name:'Mariana Widjaja',status:'ZTH Member',pict:DefP,text:'Donny dan Irene bener-bener genuine buat bantu partisipan supaya melek financial. Post class follow up-nya juga pakem dan menyeluruh jadi berasa selalu ada support dari komunitas. Im so grateful to join this class and looking forward for other sessions!'},
    ]
  return (
    <Grid container justifyContent="center" className={classes.container}>
        <Grid item xs={12}>
            <Grid container justifyContent="center" className={classes.title}>
                What Our Member Says
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <Grid container justifyContent="center" className={classes.subtitle}>
                Hear what our Zero to Heroes members opinion
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <Grid container justifyContent="center" className={classes.items}>
                {datas.map((data)=>{
                    return(
                        <Grid item md={4} xs={6} key={data.no}>
                            <Grid container className={classes.boxOuter}>
                                <Grid container alignContent='flex-start' className={classes.box}>
                                    <Grid container justifyContent='flex-start' alignItems='center' className={classes.content}>
                                        <Grid item xs={3}>
                                            <Grid container className={classes.pictContainer}>
                                                <img src={data.pict} className={classes.pict}/>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs>
                                            <Grid container className={classes.name}>
                                                {data.name}
                                            </Grid>
                                            <Grid container className={classes.status}>
                                                {data.status}
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid container className={classes.text}>
                                        {data.text}    
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    )
                })}
            </Grid>
        </Grid>
    </Grid>
  )
}
