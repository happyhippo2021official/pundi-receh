import React from 'react'
import {Grid} from '@material-ui/core'
import useStyles from '../../resources/css/components/Dashboard/Performance'

export default function Performance() {
    const classes = useStyles()
    const datas = [
        {code:'SR',text:'Success Rate',value:'67%',sign:'', details:'18 out of 27 transactions'},
        {code:'AR',text:'Average Return',value:'7.1%',sign:'+', details:'Average return/transaction'},
        {code:'YR',text:'12 Months Return',value:'12.21%',sign:'+', details:'Return in the last 12 months'},
        {code:'YTR',text:'YTD Return',value:'12.21%',sign:'+', details:'Return since the beginning of current fiscal  year'},
    ]
  return (
    <Grid container justifyContent="center" className={classes.container}>
        <Grid item xs={12}>
            <Grid container justifyContent="center" className={classes.title}>
                Our Performance
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <Grid container justifyContent="flex-start" className={classes.subtitle}>
                Per Oct 2021,
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <Grid container justifyContent="center" className={classes.items}>
            {datas.map((data, index)=>{
                return(
                    <Grid item md={6} xs={12} key={index}>
                        <Grid container justifyContent='center' className={classes.spacing}>
                            <Grid container className={classes.box}>
                                <Grid container  justifyContent='flex-start' className={`${classes.texts} ${classes.itemtitle}`}>
                                    {data.text}
                                </Grid>
                                <Grid container  justifyContent='flex-start' className={`${classes.texts} ${classes.itemvalue}`}>
                                    {data.sign}{data.value}
                                </Grid>
                                <Grid container  justifyContent='flex-start' className={`${classes.texts} ${classes.itemdetails}`}>
                                    {data.details}
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                )
            })}
            </Grid>
        </Grid>
        <Grid item  md={10} xs={12}>
            <Grid container justifyContent="center" className={classes.footertext}>
            <span>Pundi receh is at Tipranks.com to compare with other world class analyst and </span>
            <span>S&P500 benchmark. We are able to beat Tipranks best analyst and S&P500 in 2021. </span>
            <span>Please click here for more detail.</span>
            </Grid>
        </Grid>
    </Grid>
  )
}
