import React, {useState} from 'react'
import {Grid, TextField} from '@material-ui/core'
import {useHistory} from 'react-router-dom'
import useStyle from '../../resources/css/components/Dashboard/Welcome'

import laptopPict from '../../resources/images/laptop.png'
import hpPict from '../../resources/images/phone.png'

export default function Welcome() {
    const inputProps={ 
        color:'#fff',
    }
    const classes = useStyle()
    const history = useHistory()
    const [data,setData] = useState({
        firstname:'',
        lastname:'',
        email:''
    })
    function changeData(prop, value){
        const newData = {...data,
            [prop]:value
        }
        console.log(newData)
        setData(newData)
    }
    function qsignup(){
        history.push({pathname:'/signup',state:{data:data}})
    }
  return (
      <Grid container>
          <Grid item xs={12}>
              <Grid container>
                    <Grid item xs={12}>
                        <Grid container className={classes.title}>
                            Your Financial Journey Starts Here
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container className={classes.subtitle}>
                            Register to get your personal financial checklist
                        </Grid>
                    </Grid>
              </Grid>
          </Grid>
          <Grid item xs={12}>
              <Grid container>
                  <Grid item md={6} xs={12}>
                      <Grid container justifyContent='center' className={classes.formContainer}>
                        <Grid item md={8} xs={11}>
                            <Grid container alignItems='center' justifyContent='space-between' className={classes.itemWrap}>
                                First Name
                                <input type='text' value={data.firstname} name='fname' className={`${classes.formItem}`} onChange={(e)=>{changeData('firstname',e.target.value)}}/>      
                            </Grid>
                            <Grid container alignItems='center' justifyContent='space-between' className={classes.itemWrap}>
                                Last Name
                                <input type='text' value={data.lastname} name='lname' className={`${classes.formItem}`} onChange={(e)=>{changeData('lastname',e.target.value)}}/>      
                            </Grid> 
                            <Grid container alignItems='center' justifyContent='space-between' className={classes.itemWrap}>
                                Email
                                <input type='text' value={data.email} name='email' className={`${classes.formItem}`} onChange={(e)=>{changeData('email',e.target.value)}}/>      
                            </Grid>
                            <Grid container justifyContent='center' >
                                <Grid item xs='auto' className={classes.button} onClick={()=>{qsignup()}}>
                                    Get your 7-days free trial now
                                </Grid>
                            </Grid>
                        </Grid>
                      </Grid>
                  </Grid>
                  <Grid item md={6} xs={12}>
                      <Grid container justifyContent='center'>
                        <Grid item xs={10}>
                            <Grid container className={classes.pictContainer}>
                                <img src={laptopPict} className={classes.laptopPict}/>
                                <img src={hpPict} className={classes.hpPict}/>
                            </Grid>
                        </Grid>
                      </Grid>
                  </Grid>
              </Grid>
          </Grid>
      </Grid>
  )
}
