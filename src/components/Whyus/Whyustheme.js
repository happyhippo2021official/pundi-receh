import { createTheme } from '@material-ui/core/Styles'
import { grey, teal, blue } from '@material-ui/core/colors'


const customTheme = createTheme({
    palette:{
        primary:{
            main: grey[900]
        },
    },
    typography: {
        h4:{
            fontStyle:'italic',
        }
    },
})

export default customTheme;