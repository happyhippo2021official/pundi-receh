import React from 'react'
import { useState } from 'react';
import {Grid, Card, Typography, CardHeader, Avatar, ThemeProvider} from '@material-ui/core'
import '../../resources/css/stockItem.css'
import { ThumbUpAlt, AttachMoney, Money, AccountBalance}from '@material-ui/icons'
import { grey, blueGrey, cyan } from '@material-ui/core/colors'
import useStyles from '../../resources/css/Styles'
import {useTranslation} from "react-i18next"

const ShowSection = (props) => {
    const { t } = useTranslation();
    const useStyle = useStyles()
    return (
        <ThemeProvider >
            <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                <Grid container className={useStyle.lighterBackground} style={{ gap:'2vw'}}>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                        <Grid container style={{height:'20vh', maxHeight:125, minHeight:100}} justifyContent='center' alignContent='center'>
                            <Grid item xl={8} lg={8} md={12} sm={12} xs={12}>
                                <Typography className={`${useStyle.gradientText} ${useStyle.gradientTextColor2} ${useStyle.subTitle} ${useStyle.LatoHeadline}`} variant='h2' align='center'>
                                    {t('reason')}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                        <Grid container spacing={4} style={{gap:'1vw', paddingBottom:32}} justifyContent='center'>
                            <Grid item xl={8} lg={8} md={11} sm={11} xs={11}>
                                <Typography variant='h4' align='left' useStyle={useStyle.LatoHeadline} >
                                    {t('reason1Title')}
                                </Typography>
                                <Typography variant='subtitle1' useStyle={useStyle.LatoHeadline}  align='left'>
                                    {t('reason1Detail')}
                                </Typography>
                            </Grid>
                            <Grid item xl={8} lg={8} md={11} sm={11} xs={11}>
                                <Typography variant='h4' useStyle={useStyle.LatoHeadline}  align='right'>
                                    {t('reason2Title')}
                                </Typography>
                                <Typography variant='subtitle1' useStyle={useStyle.LatoHeadline}  align='right'>
                                    {t('reason2Detail')}
                                </Typography>
                            </Grid>
                            <Grid item xl={8} lg={8} md={11} sm={11} xs={11}>
                                <Typography variant='h4' useStyle={useStyle.LatoHeadline}  align='left'>
                                    {t('reason3Title')}
                                </Typography>
                                <Typography variant='subtitle1' useStyle={useStyle.LatoHeadline}  align='left'>
                                    {t('reason3Detail')}
                                </Typography>
                            </Grid>
                            <Grid item xl={8} lg={8} md={11} sm={11} xs={11}>
                                <Typography variant='h4' useStyle={useStyle.LatoHeadline}  align='right'>
                                    {t('reason4Title')}
                                </Typography>
                                <Typography variant='subtitle1' useStyle={useStyle.LatoHeadline} align='right'>
                                    {t('reason4Detail')}
                                </Typography>
                            </Grid> 
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </ThemeProvider>
    );
  };
  
export default ShowSection;