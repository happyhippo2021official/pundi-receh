import react from 'react'
import {Grid, Typography} from '@material-ui/core'
import useStyles from '../../resources/css/Styles'
import {useTranslation} from 'react-i18next'
import DOMPurify from "dompurify";
import '../../resources/css/heatmap.css'
import useScript from '../../hooks/useScript'


export default function HeatmapList(){
    useScript('https://quantifycrypto.com/widgets/heatmaps/js/qc-heatmap-widget.js')

    const useStyle = useStyles()

    const {t} = useTranslation()
    return(
        <>
            <Grid container className={`${useStyle.heatmapOutsideContainer} ${useStyle.darkerBackground}`} justifyContent='center'>
                <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                <div id="demobox">
                    <qc-heatmap height="400px" num-of-coins="50" currency-code="USD"></qc-heatmap>
                </div>
                </Grid>
            </Grid>
        </>
    )
}