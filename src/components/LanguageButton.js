import React, { useState, useEffect } from 'react';
import {FaArrowCircleLeft} from 'react-icons/fa';
import {FaArrowCircleRight} from 'react-icons/fa';
import {useTranslation} from "react-i18next";
  
const LanguageButton = () =>{
    const [currentLanguage, setCurrentLanguage] = useState("en")
    const { i18n } = useTranslation();
    
    useEffect(() => {
        i18n.changeLanguage(currentLanguage)
    }, [currentLanguage])

  return (
    <div className="langWrapper">
        <div className={`langBtn ${currentLanguage === "en" ? "active" : ""}`} onClick={() => setCurrentLanguage('en')} >
            EN
        </div>
        <div className={`langBtn ${currentLanguage === "id" ? "active" : ""}`} onClick={() => setCurrentLanguage('id')} >
            ID
        </div>
    </div>
  );
}
  
export default LanguageButton;