import React from 'react'

export default function OutputComponent(props) {
    const data = props.data
  console.log(data)
  return (
    <>
        {data.itemType=='bold1'?(
            <p>
                {data.value.en}
            </p>
        ):data.itemType=='bold2'?(
            <p>
                {data.value.en}
            </p>
        ):data.itemType=='pict'?(
            <>
                <img src={data.value}/>
            </>
        ):data.itemType=='text'?(
            <p>
                {data.value.en}
            </p>
        ):null}
    </>
  )
}
