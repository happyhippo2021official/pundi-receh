import React from 'react'
import {Link} from "react-router-dom"
import { useState, useEffect, useLayoutEffect } from 'react';
import {Grid, Typography, Button} from '@material-ui/core'
import useStyles from '../../resources/css/Styles'
import {useTranslation} from 'react-i18next'
import '../../resources/css/playList.css'
import CompanySections from '../Playlist/CompanySections';
import StockholderChart from '../Playlist/StockholderChart';
import { Carousel } from 'react-responsive-carousel'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {GetMonthlyAnalysis} from '../../databases/Read'


const Playlist = () => {
    const [data, setData] = useState([])

    const arrCompanyBios = [
        { id: '1', name: 'Sector', description: 'Technology'  },
        { id: '2', name: 'Industry', description: 'Consumer Electronics'  },
        { id: '3', name: 'IPODate', description: '1980-12-12'  },
        { id: '4', name: 'CountryName', description: 'USA'  },
        { id: '5', name: 'Address', description: 'One Apple Park Way, Cupertino, CA, United States, 95014'  },
        { id: '6', name: 'Phone', description: '408 996 1010'  },
        { id: '7', name: 'Website', description: 'https://www.apple.com'  },
        { id: '8', name: 'Employees', description: '154000'  }
        
    ];
    const dataChart = [
        { name: 'Vanguard Group Inc', value: 7.71 },
        { name: 'BlackRock Inc', value: 6.2701 },
        { name: 'Berkshire Hathaway Inc', value: 5.4073 },
        { name: 'State Street Corporation', value: 3.7947 },
        { name: 'FMR Inc', value: 2.1361 }
    ];
    const datas = [
        { name: '1' },
        { name: '1' },
        { name: '1' },
        { name: '1' }
    ];
    const [currentContentMode, setContentMode] = useState("small")
    const useStyle = useStyles()

    const {t} = useTranslation()
    
    useEffect(() => {
        // console.log("loading playlist");
        setContentMode('big')
    }, []);

    useEffect(()=>{
       GetMonthlyAnalysis().then((resolve)=>{
        setData([...resolve])
        console.log(resolve)
      }) 
    },[])

    useLayoutEffect(() => {
        return () => {
            // console.log("loading useLayoutEffect");
            // setContentMode('small')
        }
    }, [])
    

    return (
        <Carousel autoPlay showStatus={false} showArrows={true} indicators={false} interval={4000} showIndicators={false}>
            {data.map(datum=>(
                <Grid container className={`${useStyle.outsideContainer} ${useStyle.darkerBackground} ${currentContentMode} ${useStyle.textColor1} playlistWrapper`} justifyContent='center'>
                <Grid item xl={7} lg={7} md={7} sm={11} xs={11} className="sideCompanyInfo">
                    <Grid container className={`sideCompanyInfo-content`} justifyContent='space-between'>
                        <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                            <Grid container className={`sideCompanyInfo-bio`} justifyContent='space-between'>
                                <Grid item xl={9} lg={9} md={9} sm={12} xs={12} className="leftAlign">
                                    <div><span className='tickerName'>{datum.basic.name}</span> <span className='tickerCode'>(NASDAQ:AAPL)</span></div>
                                    <div><span className='tickerCurrentPrice'>179.30</span> <span className='tickerCurrentChange'>+4.97</span> <span className='tickerCurrentChangePercentage'>(+2.85%)</span></div>
                                </Grid>
                                <Grid item xl={3} lg={3} md={3} sm={12} xs={12}>
                                    <div className='tickerLogo'>
                                        <img src ="https://eodhistoricaldata.com/img/logos/US/aapl.png" />
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                            <Grid container className={`sideCompanyInfo-desc`}>
                                <div className='desc-content'>
                                    Apple Inc. designs, manufactures, and markets smartphones, personal computers, tablets, wearables, and accessories worldwide. It also sells various related services. In addition, the company offers iPhone, a line of smartphones; Mac, a line of personal computers; iPad, a line of multi-purpose tablets; AirPods Max, an over-ear wireless headphone; and wearables, home, and accessories comprising AirPods, Apple TV, Apple Watch, Beats products, HomePod, and iPod touch. Further, it provides AppleCare support services; cloud services store services; and operates various platforms, including the App Store that allow customers to discover and download applications and digital content, such as books, music, video, games, and podcasts. Additionally, the company offers various services, such as Apple Arcade, a game subscription service; Apple Music, which offers users a curated listening experience with on-demand radio stations; Apple News+, a subscription news and magazine service; Apple TV+, which offers exclusive original content; Apple Card, a co-branded credit card; and Apple Pay, a cashless payment service, as well as licenses its intellectual property. The company serves consumers, and small and mid-sized businesses; and the education, enterprise, and government markets. It distributes third-party applications for its products through the App Store. The company also sells its products through its retail and online stores, and direct sales force; and third-party cellular network carriers, wholesalers, retailers, and resellers. Apple Inc. was incorporated in 1977 and is headquartered in Cupertino, California.
                                </div>
                            </Grid>
                        </Grid>
                        <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                            <Grid container className={`sideCompanyInfo-infotags`}>
                                {arrCompanyBios.map(item => (
                                    <Grid item xl={3} lg={3} md={3} sm={6} xs={6}>
                                        <CompanySections key={item.id} name={item.name} description={item.description} />
                                    </Grid>
                                ))}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xl={3} lg={3} md={3} sm={11} xs={11} className="sideChart">
                    <div className='titleChart'>Holders (Institutions)</div>
                    <StockholderChart props={dataChart}/>
                    <Link to={`/analysis/${datum.id}`}>
                        <Button className={`${useStyle.gradientButtonStyle2} ${useStyle.textColor1} buttonDetail`}>Technical analysis</Button>
                    </Link>
                </Grid>
                </Grid>
            ))}
            {/* {datas.map(data=>(
                <Grid container className={`${useStyle.outsideContainer} ${useStyle.darkerBackground} ${currentContentMode} ${useStyle.textColor1} playlistWrapper`} justifyContent='center'>
                <Grid item xl={7} lg={7} md={7} sm={11} xs={11} className="sideCompanyInfo">
                    <Grid container className={`sideCompanyInfo-content`} justifyContent='space-between'>
                        <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                            <Grid container className={`sideCompanyInfo-bio`} justifyContent='space-between'>
                                <Grid item xl={9} lg={9} md={9} sm={12} xs={12} className="leftAlign">
                                    <div><span className='tickerName'>Apple Inc</span> <span className='tickerCode'>(NASDAQ:AAPL)</span></div>
                                    <div><span className='tickerCurrentPrice'>179.30</span> <span className='tickerCurrentChange'>+4.97</span> <span className='tickerCurrentChangePercentage'>(+2.85%)</span></div>
                                </Grid>
                                <Grid item xl={3} lg={3} md={3} sm={12} xs={12}>
                                    <div className='tickerLogo'>
                                        <img src ="https://eodhistoricaldata.com/img/logos/US/aapl.png" />
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                            <Grid container className={`sideCompanyInfo-desc`}>
                                <div className='desc-content'>
                                    Apple Inc. designs, manufactures, and markets smartphones, personal computers, tablets, wearables, and accessories worldwide. It also sells various related services. In addition, the company offers iPhone, a line of smartphones; Mac, a line of personal computers; iPad, a line of multi-purpose tablets; AirPods Max, an over-ear wireless headphone; and wearables, home, and accessories comprising AirPods, Apple TV, Apple Watch, Beats products, HomePod, and iPod touch. Further, it provides AppleCare support services; cloud services store services; and operates various platforms, including the App Store that allow customers to discover and download applications and digital content, such as books, music, video, games, and podcasts. Additionally, the company offers various services, such as Apple Arcade, a game subscription service; Apple Music, which offers users a curated listening experience with on-demand radio stations; Apple News+, a subscription news and magazine service; Apple TV+, which offers exclusive original content; Apple Card, a co-branded credit card; and Apple Pay, a cashless payment service, as well as licenses its intellectual property. The company serves consumers, and small and mid-sized businesses; and the education, enterprise, and government markets. It distributes third-party applications for its products through the App Store. The company also sells its products through its retail and online stores, and direct sales force; and third-party cellular network carriers, wholesalers, retailers, and resellers. Apple Inc. was incorporated in 1977 and is headquartered in Cupertino, California.
                                </div>
                            </Grid>
                        </Grid>
                        <Grid item xl={11} lg={11} md={11} sm={11} xs={11}>
                            <Grid container className={`sideCompanyInfo-infotags`}>
                                {arrCompanyBios.map(item => (
                                    <Grid item xl={3} lg={3} md={3} sm={6} xs={6}>
                                        <CompanySections key={item.id} name={item.name} description={item.description} />
                                    </Grid>
                                ))}
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xl={3} lg={3} md={3} sm={11} xs={11} className="sideChart">
                    <div className='titleChart'>Holders (Institutions)</div>
                    <StockholderChart props={dataChart}/>
                    <Link to='/analysis/1231'>
                        <Button className={`${useStyle.gradientButtonStyle2} ${useStyle.textColor1} buttonDetail`}>Technical analysis</Button>
                    </Link>
                </Grid>
                </Grid>
            ))} */}
        </Carousel>

    );
  };
  
export default Playlist;