import * as React from 'react';
import PropTypes from 'prop-types';
import {Grid, Box, Tabs, Tab, Typography} from '@material-ui/core';
import Output from './OutputComponent'

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function assignProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

export default function DetailAnalysisTabs(props) {
  const [value, setValue] = React.useState(0);
  console.log(props)
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const background = props.data.background
  console.log(background)
  const tabList = [
        {
        name: 'tab1',
        label: 'Latar belakang',
        content: (
            <div className="tab-content">
                <p><strong>Oleh {props.data.by}</strong></p>



                <p><strong>Latar belakang</strong></p>



                <p>Nama: {props.data.basic.name}</p>



                <p>Kode ticker: {props.data.basic.code}</p>



                <p>Harga: $ {props.data.basic.price} (per {props.data.time})</p>



                <p>Kategori:  {props.data.basic.category}</p>
                {background.map((datum, index)=>(
                  <Output key={index} data={datum}/>
                ))}
            </div>
        )
        },
        {
        name: 'tab2',
        label: 'Tinjauan industry',
        content: (
            <div className="tab-content">
              
              {props.data.overview.map((datum, index)=>(
                  <Output key={index} data={datum}/>
                ))}
            </div>
        )
        },
        {
        name: 'tab3',
        label: 'Ringkasan investasi',
        content: (
            <div className="tab-content">
              {props.data.fSummary.map((datum, index)=>(
                <Output key={index} data={datum}/>
              ))}
            </div>
        )
        },
        {
        name: 'tab4',
        label: 'Ringkasan keuangan',
        content: (
            <div className="tab-content">
              {props.data.tSummary.map((datum, index)=>(
                <Output key={index} data={datum}/>
              ))}
            </div>
        )
        },
        {
        name: 'tab5',
        label: 'Evaluasi teknis',
        content: (
            <div className="tab-content">
              {props.data.eval.map((datum, index)=>(
                <Output key={index} data={datum}/>
              ))}
            </div>
        )
        }
    ];



  return (
    <Grid container className="detailAnalysisTabs">
        <Grid item xl={12} lg={12} md={12} sm={12} xs={12} className="detailAnalysisTabArea">
            <Tabs
                orientation="horizontal"
                variant="scrollable"
                value={value}
                onChange={handleChange}
                scrollButtons
                allowScrollButtonsMobile
                aria-label="horizontal tabs example"
                sx={{ borderColor: 'divider' }}
            >
                {
                    tabList.map((tab, i) => (
                        <Tab label={tab.label} {...assignProps(i)} />
                    ))
                }
            </Tabs>
        </Grid>
        <Grid item xl={12} lg={12} md={12} sm={12} xs={12} className="detailAnalysisContentArea">
            {
                tabList.map((tab, i) => (
                    <TabPanel className='tabPanel' value={value} index={i}>
                        {tab.content}
                    </TabPanel>
                ))
            }
        </Grid>
    </Grid>
  );
}