import React from 'react'
import { useState } from 'react';
import { BrowserRouter as Router , Link }from 'react-router-dom';
import '../../resources/css/stockItem.css'

const StockItemCard = (props) => {
    
    return (
    <div className="stock-item">
        <div className="stock-item-wrapper">
            <div className="stock-item-title">
                <div className="stock-item-name" key={props.key}>{props.name}</div>
                <div className="stock-item-nameCode" key={props.key}>{props.nameCode}</div>
            </div>
            <div className="stock-item-body">
                <div className="stock-item-price stock-item-priceValue bull glow" key={props.key}>{props.price}</div>
                <div className="stock-item-price stock-item-priceChange bull glow">{props.priceChange}</div>
                <div className="stock-item-price stock-item-priceChangePercentage bull" key={props.key}>{props.priceChangePercentage}</div>
            </div>
            <div className="stock-item-date">
                <div className="stock-item-posted">{props.posted}</div>
                <div className="stock-item-posted"><Link to={`/detail/${props.name}`}><button>Go to details</button></Link></div>
            </div>
        </div>
    </div>
        
    );
  };
  
export default StockItemCard;