import React from "react";
import { Link, useHistory, useParams, useRouteMatch } from "react-router-dom";
import { useState, useEffect, useLayoutEffect } from 'react';
import {Grid, Typography, Button} from '@material-ui/core'
import useStyles from '../../resources/css/Styles'
import DetailAnalysisTabs from '../Playlist/DetailAnalysisTabs';
import {GetOneMAnalysis} from '../../databases/Read'


const DetailAnalysis = () => {
  const { id } = useParams();
  const { url } = useRouteMatch();
  const [data, setData] = useState(null)
  const history = useHistory();
  const useStyle = useStyles()
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    GetOneMAnalysis(id).then((resolve)=>{
      setData([...resolve])
      console.log(resolve)
    })
    // console.log("loading playlist");
},[]);
  useEffect(()=>{
    if(data){
      setLoading(false)
    }
  },[data])

// useLayoutEffect(() => {
//     return () => {
//     }
// }, [])


  return loading?(
      <>
        Loading...
      </>
  ):(
      <Grid container className={`${useStyle.outsideContainer} ${useStyle.textColor1} detailAnalysisWrapper `} justifyContent='center'>
      {/* <p>You have selected Campaign - {id}</p>
      <button onClick={history.goBack}>Go Back</button> */}
      <Grid item xl={8} lg={8} md={8} sm={11} xs={11} className="detailAnalysis">
        <DetailAnalysisTabs data={data[0]}/>
      </Grid>
    </Grid>
  );
};

export default DetailAnalysis;