import React from 'react'
import { useState } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

const ResultItem = (props) => {
    
    return (
    <div className="result-item">
        <div className="itemImage"><img src={require('../../resources/images/img1.png').default}/></div>
        <div className="itemName" key={props.key}>{props.name}</div>
        <div className="itemDesc">{props.description}</div>
        <div className="itemInfo">
            <div className="itemCount">6 modules</div>
            <div className="itemPop">Preview</div>
        </div>
    </div>
        
    );
  };
  
export default ResultItem;