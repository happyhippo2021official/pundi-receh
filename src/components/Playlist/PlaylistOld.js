import React from 'react'
import { useState, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Announcer from '../announcer';
import ResultItem from '../Playlist/ResultItem';
import StockItemCard from '../Playlist/StockItemCard';
import moment from 'moment';
import SearchPlaylist from './SearchPlaylist'
import WeekPicker from './WeekPicker'
import Tabs from '../Tabs/Tabs'
// import Helmet from 'react-helmet';
import DayPicker from 'react-day-picker';

// css
import 'react-day-picker/lib/style.css';
import '../../resources/css/tabs.css'

const posts = [
    { id: '1', name: 'first', description: 'This first post of Pundi Receh', category: 'blog'  },
    { id: '2', name: 'second', description: 'This second post of Pundi Receh', category: 'podcasts' },
    { id: '3', name: 'third', description: 'This third post of Pundi Receh', category: 'blog' },
    { id: '4', name: 'fourth', description: 'This fourth post of Pundi Receh', category: 'vlogs' },
];

const stocks = [
    { id: '1', name: 'Tesla Inc', nameCode: 'NASDAQ: TSLA', price: '886', priceChange: '+15.2', priceChangePercentage: '+1.2%', posted: 'As of 10:01AM EDT.', status: 'Market open'  },
    { id: '2', name: 'Google Inc', nameCode: 'NASDAQ: GOOGL', price: '2833', priceChange: '+135.2', priceChangePercentage: '+1.12%', posted: 'As of 10:01AM EDT.', status: 'Market open'  },
    { id: '3', name: 'Apple Inc', nameCode: 'NASDAQ: AAPL', price: '164', priceChange: '+15.2', priceChangePercentage: '+1.2%', posted: 'As of 10:01AM EDT.', status: 'Market open'  },
    { id: '4', name: 'General Motors Company', nameCode: 'NASDAQ: GM', price: '886', priceChange: '+15.2', priceChangePercentage: '+1.2%', posted: 'As of 10:01AM EDT.', status: 'Market open'  },
    { id: '5', name: 'Hippo Inc', nameCode: 'NASDAQ: TSLA', price: '886', priceChange: '+15.2', priceChangePercentage: '+1.2%', posted: 'As of 10:01AM EDT.', status: 'Market open'  },
    { id: '6', name: 'Palantir Technologies Inc', nameCode: 'NASDAQ: PLTR', price: '24', priceChange: '+15.2', priceChangePercentage: '+1.2%', posted: 'As of 10:01AM EDT.', status: 'Market open'  },
];

const category = [
    { id: '1', name: 'blogs', description: 'This blogs of Pundi Receh'  },
    { id: '2', name: 'podcasts', description: 'This podcasts of Pundi Receh' },
    { id: '3', name: 'vlogs', description: 'This vlogs of Pundi Receh' },
];

const weeksList = () => {
    var weeks = [];
    var weekCounter=1;
    var startDate = moment(new Date(2021,0,1)).isoWeekday(8);
    if(startDate.date() == 8) {
        startDate = startDate.isoWeekday(-6)
    }
    var today = moment().isoWeekday('Sunday');
    while(startDate.isBefore(today)) {
    let startDateWeek = startDate.isoWeekday('Monday').format('DD-MM-YYYY');
    let endDateWeek = startDate.isoWeekday('Sunday').format('DD-MM-YYYY');
    startDate.add(7,'days');
    weeks.push({ weekNum: weekCounter, startDateWeek: startDateWeek, endDateWeek: endDateWeek  });
    weekCounter++;
    }
    console.log(weeks);
    return weeks;
}

const searchByCat = () => {
    return console.log("clicked");
};

const filterStocks = (stocks, query) => {
    if (!query) {
        return stocks;
    }

    return stocks.filter((stock) => {
        const stocksName = stock.name.toLowerCase();
        const stocksCode = stock.nameCode.toLowerCase();
        return stocksName.includes(query) || stocksCode.includes(query);
    }); 
};


const Playlist = () => {
    const { search } = window.location;
    const query = new URLSearchParams(search).get('s');
    const [searchQuery, setSearchQuery] = useState(query || '');
    const filteredStocks = filterStocks(stocks, searchQuery);
    useEffect(() => {
        let x = weeksList();
    });
    return (
        <Router>
            <div className="playlistWrapper">
                <Announcer
                    message={`${filteredStocks.length} results`}
                />
                <div className="rowSearch">
                <SearchPlaylist 
                    searchQuery={searchQuery}
                    setSearchQuery={setSearchQuery}
                />
                    <div className="dateSelector">
                        <WeekPicker></WeekPicker>
                    </div>
                </div>
                <div className="result-list">
                    {filteredStocks.map(stock => (
                        <StockItemCard key={stock.key} name={stock.name} nameCode={stock.nameCode} price={stock.price} priceChange={stock.priceChange} priceChangePercentage={stock.priceChangePercentage} posted={stock.posted} status={stock.status}/>
                    ))}
                </div>
            </div>
        </Router>
        
    );
  };
  
export default Playlist;