import React from 'react'
import { useState } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

const CompanySections = (props) => {
    
    return (
        <div className="companySections" key={props.key}>
            <div className="companySections-name">{props.name}</div>
            <div className="companySections-desc">{props.description}</div>
        </div>
    );
  };
  
export default CompanySections;