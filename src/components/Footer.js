import React from "react";
import { Chip, Typography, Grid, Button, CardMedia} from '@material-ui/core'
import useStyles from '../resources/css/Styles'
import '../resources/css/footer.css'
import socmed1 from '../resources/images/fb.png'
import socmed2 from '../resources/images/twt.png'
import socmed3 from '../resources/images/ig.png'
import socmed4 from '../resources/images/yt.png'
import LanguageButton from './LanguageButton'


const Footer = () => {
  
  const useStyle = useStyles();
  return (
    <Grid container item xl={12} lg={12} md={12} sm={12} xs={12} className={useStyle.footer} justifyContent='center'>
      <Grid container item xl={11} lg={11} md={11} sm={11} xs={11} justifyContent='center' className='collapseFooter'>
        <Grid item xl={3} lg={3} md={12} sm={12} xs={12} justifyContent='center'>
          <Grid container item xl={12} lg={12} md={12} sm={12} xs={12} direction='column' justifyContent='center'>
            <h3 className="title">Get Connected</h3>
            <p className="topClear">
              For further informations and contacts, reach us in these social medias.
            </p>
          </Grid>
          <Grid item xl={12} lg={12} md={12} sm={12} xs={12} justifyContent='center'>
            <a className="socmedIcon" href="https://wa.me/6598003940?text=I%27m%20interested%20knowing%20Pundi%20Receh">
              <img className="socialzoom" src={require('../resources/images/wa.png').default} width="64" height="64" />
            </a>
            <a className="socmedIcon" href="https://twitter.com/pundirecehcom">
              <img className="socialzoom" src={require('../resources/images/twt.png').default} width="64" height="64" />
            </a>
            <a className="socmedIcon" href="https://www.instagram.com/pundireceh">
              <img className="socialzoom" src={require('../resources/images/ig.png').default} width="64" height="64" />
            </a>
            <a className="socmedIcon" href="https://youtube.com/channel/UC4E1taXeZIJ8ASkfyIDnZJA">
              <img className="socialzoom" src={require('../resources/images/yt.png').default} width="64" height="64" />
            </a>
          </Grid>
            
        </Grid>
        <Grid item xl={3} lg={3} md={12} sm={12} xs={12} justifyContent='center' alignItems='center'>
            <h3 className="title">Office</h3>
            <p>
              <ul>
                <li className="list-unstyled">
                  Pundi Receh Pte Ltd
                </li>
                <li className="list-unstyled">
                  20 Cecil Street #05-03
                </li>
                <li className="list-unstyled">
                  Singapore 049705
                </li>
                <li className="list-unstyled">
                  sobatmu@pundireceh.com
                </li>
              </ul>
            </p>
        </Grid>
        <Grid item xl={3} lg={3} md={12} sm={12} xs={12} justifyContent='center' alignItems='center'>
            <h3 className="title">Links</h3>
            <p>
            <ul>
              <li className="list-unstyled">
                <a href="#!">About Us</a>
              </li>
              <li className="list-unstyled">
                <a href="#!">Zero to Hero Member</a>
              </li>
              <li className="list-unstyled">
                <a href="#!">Contact Us</a>
              </li>
              <li className="list-unstyled">
                <a href="#!">FAQs</a>
              </li>
            </ul>
            </p>
        </Grid>
      </Grid>
      <LanguageButton />
      <Grid className="footer-copyright" item xl={12} lg={12} md={12} sm={12} xs={12} className={useStyle.copyright}>
          &copy; {new Date().getFullYear()} Copyright: <a href="https://www.pundireceh.com"> pundireceh.com </a>
      </Grid>
    </Grid>
  );
}

export default Footer;