import react from 'react'
import {Grid, Typography} from '@material-ui/core'
import useStyles from '../../resources/css/Styles'
import {useTranslation} from 'react-i18next'
import '../../resources/css/heatmap.css'
import useScript from '../../hooks/useScript'
import { ForexHeatMap } from "react-ts-tradingview-widgets";


export default function HeatmapForexList(){

    const useStyle = useStyles()

    const {t} = useTranslation()
    return(
        <Grid container className={`${useStyle.heatmapOutsideContainer} ${useStyle.darkerBackground}`} justifyContent='center'>
            <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                <ForexHeatMap  autosize currencies={["EUR", "USD", "JPY", "GBP", "SGD", "AUD", "IDR", "TWD", "CNY", "MYR"]}></ForexHeatMap>
            </Grid>
        </Grid>
    )
}