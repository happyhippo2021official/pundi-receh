import React, { useState, useEffect } from "react";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default function TableDatePicker(props) {
    const [startDate, endDate] = props.dateRange;
    // useEffect(
    //     () => {
    //       if (startDate && endDate && startDate != null && endDate != null) {
    //         console.log(dateRange);
    //       }
    //     },
    //     [dateRange],
    // );
    return (
        <DatePicker
        selectsRange={true}
        startDate={startDate}
        endDate={endDate}
        onChange={(update) => {
            props.setDateRange(update);
        }}
        withPortal
        placeholderText="Please select date period"
        isClearable
        />
    );
}
