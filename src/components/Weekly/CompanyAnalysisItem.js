import React from 'react'
import { Grid, TableRow, TableCell} from '@material-ui/core'
import '../../resources/css/companyAnalysisItem.css'
import * as firebase from 'firebase/app'
import AnalysisItem from './AnalysisItem'

const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

const anyFunction = (a) => {
    return a <=0.1 ? a = "No debt" : a;
}

const convertDate = (date) => {
    let formattedDate =  (new Date(date*1000)).toLocaleString(undefined, options);
    return formattedDate;
}


const CompanyAnalysisItem = (props) => {

    let anyVar;

    if (props) {
        console.log("this is props", props);
        console.log("this is props stock", props.stock);
        console.log(typeof(props.stock));
    }

    return (
        <div className="companyAnalysisItemWrapper">
            <div className="companyAnalysisItemTitle">
                {props.title}
            </div>
            <div className="companyAnalysisItemPosted">
                {convertDate(props.posted)}
            </div>
            <div className="companyAnalysisItemText">
                {props.text}
            </div>

            <Grid container justifyContent='space-between' alignItems='center' className="companyAnalysisItemContentWrapper">
                {Object.keys(props.stock).map((keyName, keyIndex) => {
                        return(
                        <AnalysisItem dataProps={props.stock[keyIndex]}/>
                    )
                })}
            </Grid>
        </div>
    );
  };
  
export default CompanyAnalysisItem;