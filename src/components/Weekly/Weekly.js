import React from 'react'
import { useState, useEffect } from 'react';
import {Grid, Card, Typography, CardHeader, Avatar, ThemeProvider} from '@material-ui/core'
import { ThumbUpAlt, AttachMoney, Money, AccountBalance}from '@material-ui/icons'
import { grey, blueGrey, cyan } from '@material-ui/core/colors'
import useStyles from '../../resources/css/Styles'
import {useTranslation} from "react-i18next"
import Datepicker from "./Datepicker"
import {GetWeeklyAnalysis} from '../../databases/Read'
import '../../resources/css/components/Weekly/weekly.css'
import CompanyAnalysisItem from './CompanyAnalysisItem'

const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
const convertDate = (date) => {
    let formattedDate =  (new Date(date*1000));
    return formattedDate;
}
const filterObject = (obj, filterValue) => (
    Object.keys(obj).map((keyName, keyIndex) => {
        if ((convertDate(obj[keyIndex].time.seconds) > filterValue[0]) && (convertDate(obj[keyIndex].time.seconds) < filterValue[1])) {
            console.log("included");
        } else {
            console.log("not included");
        }
    })
);




const Weekly = (props) => {
    // test
    const [dateRange, setDateRange] = useState([null, null]);
    const { t } = useTranslation();
    const useStyle = useStyles();
    const [weeklyAnalysis, setWeeklyAnalysis] = useState([]);
    const fetchWeeklyAnalysis=async()=>{
        GetWeeklyAnalysis().then((result)=>{
            if(result){
                let filtered = result.filter(obj =>
                    convertDate(obj.time.seconds) > dateRange[0] && convertDate(obj.time.seconds) < dateRange[1]
                );
                let datas = {...filtered}
                setWeeklyAnalysis(datas)
            }else{
                console.log('nothing found')
            }
        })
    }
    useEffect(() => {
        console.log('date changed, now fetch data')
        if (dateRange[0] == null || dateRange[1] == null) {

        } else {
            fetchWeeklyAnalysis();
            console.log("this is date range", dateRange)
        }
    }, [dateRange])

    useEffect(() => {
        console.log(weeklyAnalysis);
        console.log(typeof weeklyAnalysis);
    }, [weeklyAnalysis])

    console.log('render')
    return (
        <ThemeProvider >
            <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                <Grid container className={useStyle.lighterBackground} style={{ gap:'2vw'}}>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                        <Grid container style={{height:'20vh', maxHeight:125, minHeight:100}} justifyContent='center' alignContent='center'>
                            <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                                <Grid container justifyContent='center'>
                                    <Grid item sm={11}>
                                        <Typography variant='h2' className={`${useStyle.subTitle} ${useStyle.gradientText} ${useStyle.gradientTextColor3}`} align='center'>Weekly Report</Typography>
                                        <Typography variant='h6' align='center'>
                                            {t('aboutSubTitle')}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                            
                        </Grid>
                        <Grid container justifyContent='center' alignItems='center' className="datepickerWrapper">
                            <Datepicker dateRange={dateRange} setDateRange={setDateRange}/>
                        </Grid>
                        <Grid container justifyContent='center' className="listWeeklyAnalysis">
                            {Object.keys(weeklyAnalysis).map((keyName, keyIndex) => {
                                return(
                                    <Grid item xl={7} lg={7} md={11} sm={11} xs={11} >
                                        <Grid container justifyContent='center'>
                                            <CompanyAnalysisItem title={weeklyAnalysis[keyIndex].title} text={weeklyAnalysis[keyIndex].text.en} textAnalysis={"bla"} posted={ weeklyAnalysis[keyIndex].time.seconds} stock={ weeklyAnalysis[keyIndex].stock}/>
                                        </Grid>
                                    </Grid>
                                )
                            })}
                        </Grid>
                    </Grid>

                </Grid>
            </Grid>
        </ThemeProvider>
    );
  };
  
export default Weekly;
