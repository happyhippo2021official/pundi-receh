import React from 'react'
import { Grid, TableRow, TableCell} from '@material-ui/core'
import '../../resources/css/companyAnalysisItem.css'
import * as firebase from 'firebase/app'

const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

const AnalysisItem = (props) => {

    var objData = props.dataProps;

    return (
        <Grid item md={5} sm={5} xs={12} className="analysisItemWrapper">
            <div className="analysisItemTitle">
                <span>{objData.code}</span> ({objData.name})
            </div>
            <div className={`analysisItemStatus ${objData.status.toLowerCase()}`}>
                {objData.status}
            </div>
            <div className="analysisItemPrice">
                {objData.price}
            </div>
            <div className="analysisContent">
                {objData.technical.en}
            </div>
        </Grid>
    );
  };
  
export default AnalysisItem;