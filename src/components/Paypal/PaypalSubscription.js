import React, { Suspense } from 'react'
import { useState } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Grid} from '@material-ui/core'
import useStyles from '../../resources/css/Styles'
import avail from '../../resources/images/check.png';
import notAvail from '../../resources/images/close.png';
import { PayPalScriptProvider, PayPalButtons } from "@paypal/react-paypal-js";
import { showSuccessMessage, showErrorMessage } from "../../utils/snackBarUtil";
import halfYear from '../../resources/images/halfYearBanner.png';
import fullYear from '../../resources/images/yearlyBanner.png';



const PaypalSubscription = (props) => {
    const [show, setShow] = useState(false);
    const [success, setSuccess] = useState(false);
    const [ErrorMessage, setErrorMessage] = useState("");
    const [orderID, setOrderID] = useState(false);
    const useStyle = useStyles()
    const [paypalMode, setPaypalMode] = useState(false);
    const SubscriptionButtonWrapper = (id) => {
        // P-61V04956SP597283AMKIDTII
        return (<PayPalButtons
            createSubscription={(data, actions) => {
                return actions.subscription
                    .create({
                        plan_id: 'P-61V04956SP597283AMKIDTII',
                    });
            }}
            style={{
                label: "subscribe",
            }}
            onApprove={onApprove}
            catchError={paypalOnError}
            onError={paypalOnError}
            onCancel={paypalOnError}
        />);
    }
    const OrderButtonWrapper = () => {
        return (<PayPalButtons
            createOrder={(data, actions) => {
                return actions.order.create({
                        purchase_units: [{
                            amount: {
                                value: '88.44'
                            }
                        }]
                    });
            }}
            onApprove={onApprove}
        />);
     }
     const onApprove = (data, actions) => {
        if (data && data.subscriptionID) {
            setSuccess(true);
            showSuccessMessage("Success", "Payment for " + data.subscriptionID + " is successful");
        }
     };
    
     const paypalOnError = (data, actions) => {
        setSuccess(false);
        showErrorMessage("Error", "Payment is error");
     };

    const changePaypalBtn = (item) => {
        setPaypalMode(true);
    };


    return (
        <Grid item xl={3} lg={3} md={11} sm={12} xs={12}>
            <div className={`${useStyle.wrapperSubscriptions} paypalItemSubscription`}>
                <div className={`banner`}>
                    {props.banner ?
                    <>
                    {props.banner =="halfYearBanner"
                        ? <img src={halfYear} className={`${useStyle.fullWide} `}/>
                        : <img src={fullYear} className={`${useStyle.fullWide} `}/>
                    }
                    </>
                    :
                    <></>
                    }
                    
                </div>
                <div className={`${useStyle.paypalSubscriptionItem_name}  ${props.nameColor}`}>{props.name}</div>
                <div className={`${useStyle.paypalSubscriptionItem_price} `}>{props.price}</div>
                {console.log(props.subscriptionID)}
                {
                    props.access.map((item, i) => (
                        <div className={`${useStyle.paypalSubscriptionItemAccess}`}>
                            <div className={`${useStyle.paypalSubscriptionItem_access_availability} `}>
                            {item.boolAccess == 1?(
                                    <img src={avail} className={`${useStyle.fullWide} `}/>
                                ):<img src={notAvail} className={`${useStyle.fullWide} `}/>}
                            </div>
                            <div className={`${useStyle.paypalSubscriptionItem_access_text} `}>{item.text}</div>
                        </div>
                    ))
                }

                <div className={`${useStyle.paypalSubscriptionItemLink}`}>
                {props.subscriptionID !=='none'?
                (
                    (paypalMode) ? <div>                    <PayPalScriptProvider
                    options={{
                        "client-id": 'AWlPWjyPQNDx-5fTHQ86Th_SYtb49hPDEO_HJYYJWQgJtfcVD8LqsmRWPPr_N_-vtTkbH_urS7cJhknD',
                        components: "buttons",
                        "data-namespace": "paypalOrder",
                        "vault": true,
                        "intent": "subscription"
                    }
                }
                >
                    <SubscriptionButtonWrapper id={props.subscriptionID} />
                </PayPalScriptProvider></div> : <div className={`${useStyle.subscriptionBtn}`} onClick={()=>changePaypalBtn(props)}>Subscribe</div>
                ):
                <></>
                }
                
                </div>
            </div>
        </Grid>
    );
  };
  
export default PaypalSubscription;