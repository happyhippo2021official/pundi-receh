import React, { useState, useEffect } from "react";
import { Typography, Grid,CardMedia, Button, TextField,} from '@material-ui/core'
import useStyles from '../../resources/css/Styles'
import {useTranslation} from 'react-i18next'
import { PayPalScriptProvider, PayPalButtons } from "@paypal/react-paypal-js";
import { showSuccessMessage, showErrorMessage } from "../../utils/snackBarUtil";
import PaypalSubscription from './PaypalSubscription'
import '../../resources/css/paypal.css';
 
export default function Paypal() {
const [show, setShow] = useState(false);
const [success, setSuccess] = useState(false);
const [ErrorMessage, setErrorMessage] = useState("");
const [orderID, setOrderID] = useState(false);
const useStyle = useStyles();
const packageObj = {
    "pundiBasic": {
        "id": "0",
        "nameColor": "subscription0",
        "name": "FREE",
        "price": "FREE",
        "access": [{ "boolAccess": 1,  "text": "Financial Blog"}, { "boolAccess": 1,  "text": "Financial Podcast"}, { "boolAccess": 1,  "text": "Financial Comics"}, { "boolAccess": 0,  "text": "Monthly Stock Analysis"}, { "boolAccess": 0,  "text": "Stock Checklist"}, { "boolAccess": 0,  "text": "Forex Heatmap"}, { "boolAccess": 0,  "text": "Crypto Heatmap"}],
        "link" : "www.google.com",
        "subscriptionID": "none"
    },
    "pundi1": {
        "id": "1",
        "nameColor": "subscription1",
        "name": "1 MONTH PLAN US$ 19.90",
        "price": "$19.90",
        "access": [{ "boolAccess": 1,  "text": "Financial Blog"}, { "boolAccess": 1,  "text": "Financial Podcast"}, { "boolAccess": 1,  "text": "Financial Comics"}, { "boolAccess": 1,  "text": "Monthly Stock Analysis"}, { "boolAccess": 1,  "text": "Stock Checklist"}, { "boolAccess": 1,  "text": "Forex Heatmap"}, { "boolAccess": 1,  "text": "Crypto Heatmap"}],
        "link" : "www.google.com",
        "subscriptionID": "P-8RV04295UW714563VMLFODOY"
    },
    "pundi6": {
        "id": "2",
        "nameColor": "subscription6",
        "name": "6 MONTHS PLAN US$ 99.50",
        "price": "$99.50",
        "access": [{ "boolAccess": 1,  "text": "Financial Blog"}, { "boolAccess": 1,  "text": "Financial Podcast"}, { "boolAccess": 1,  "text": "Financial Comics"}, { "boolAccess": 1,  "text": "Monthly Stock Analysis"}, { "boolAccess": 1,  "text": "Stock Checklist"}, { "boolAccess": 1,  "text": "Forex Heatmap"}, { "boolAccess": 1,  "text": "Crypto Heatmap"}],
        "link" : "www.google.com",
        "subscriptionID": "P-48P02591EW605844JMLFOG7A",
        "banner": "halfYearBanner"
    },
    "pundi12": {
        "id": "3",
        "nameColor": "subscription12",
        "name": "1 YEAR PLAN US$ 199",
        "price": "$199",
        "access": [{ "boolAccess": 1,  "text": "Financial Blog"}, { "boolAccess": 1,  "text": "Financial Podcast"}, { "boolAccess": 1,  "text": "Financial Comics"}, { "boolAccess": 1,  "text": "Monthly Stock Analysis"}, { "boolAccess": 1,  "text": "Stock Checklist"}, { "boolAccess": 1,  "text": "Forex Heatmap"}, { "boolAccess": 1,  "text": "Crypto Heatmap"}],
        "link" : "www.google.com",
        "subscriptionID": "P-41R832517W0541152MLFOFSQ",
        "banner": "yearlyBanner"
    }
  };
const SubscriptionButtonWrapper = () => {
	return (<PayPalButtons
		createSubscription={(data, actions) => {
			return actions.subscription
				.create({
					plan_id: "P-61V04956SP597283AMKIDTII",
				});
		}}
		style={{
			label: "subscribe",
		}}
        onApprove={onApprove}
        catchError={paypalOnError}
        onError={paypalOnError}
        onCancel={paypalOnError}
	/>);
}

 const OrderButtonWrapper = () => {
	return (<PayPalButtons
		createOrder={(data, actions) => {
			return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: '88.44'
                        }
                    }]
                });
		}}
        onApprove={onApprove}
	/>);
 }
 const onApprove = (data, actions) => {
    if (data && data.subscriptionID) {
        setSuccess(true);
        showSuccessMessage("Success", "Payment for " + data.subscriptionID + " is successful");
    }
 };

 const paypalOnError = (data, actions) => {
    setSuccess(false);
    showErrorMessage("Error", "Payment is error");
 };

 return (
    <Grid container className={`${useStyle.aboutOutsideContainer} ${useStyle.topLayerEffect} ${useStyle.darkerBackground}`} justifyContent='center'>
        <Grid item xl={10} lg={10} md={11} sm={11} xs={11} className={`${useStyle.paypalItemSubscription}`}>
            <Grid container className={`${useStyle.packageWrapper}`}>
                {Object.keys(packageObj).map((keyName, keyIndex) => {
                    return (<PaypalSubscription key={keyName} nameColor={packageObj[keyName].nameColor} keyName={keyName} name={packageObj[keyName].name} price={packageObj[keyName].price} access={packageObj[keyName].access} subscriptionID={packageObj[keyName].subscriptionID} banner={packageObj[keyName].banner}/>)
                })}
            </Grid>
        </Grid>
    </Grid>
 );
}
