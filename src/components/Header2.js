import React, {useState,useEffect} from 'react'
import { Grid, AppBar, Toolbar, Typography, Button, ThemeProvider, ButtonGroup, MenuItem, IconButton, Drawer} from '@material-ui/core'
import {Link} from "react-router-dom"
import customTheme from '../customTheme'
import useStyles from '../resources/css/Header2Style'
import MenuIcon from '@material-ui/icons/Menu';





function DisplayDesktop(allMenuItem, setAllMenuItem, Logout){
  function changeOpenStatus(headMenuName){
    const newDatas=allMenuItem.map((menuItem)=>{
      if(menuItem.name === headMenuName){
        menuItem.openStatus=!menuItem.openStatus
      }
      return menuItem
    })
    setAllMenuItem(newDatas)
  }
  const classes = useStyles()
  return(
    <Grid container justifyContent='center' alignItems='center'>
      {/* Left side: logo & name */}
      <Grid item md={5}>
        <Link to='/'>
          <Grid container justifyContent='flex-start' alignItems='center'>
            <Grid item xs={3}>
              <Grid container justifyContent='center' alignItems='center'>
                <img src={require('../logo.png').default} className={classes.imageLogo}/>
              </Grid>
            </Grid>
            <Grid item xs={9}>
              <Grid container justifyContent='flex-start' alignItems='center'>
                <Grid item xs={12}>
                  <Typography variant='h4' align='left'>PUNDI RECEH</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant='subtitle1' align='left'>Semua Hal Tentang Investasi dan Keuangan</Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Link>
      </Grid>
      {/* Right side: menus */}
      <Grid item md={7} xs={0}>
        <Grid container justifyContent='flex-end' alignItems='center'>
          {/* Menu non sign-in */}
          <Grid item md={8}>
            <Grid container justifyContent='space-between' alignItems='stretch'>
              {allMenuItem.map((menuHead)=>{                
                if(menuHead.head===true){
                  return(
                    // Head with child
                    <Grid item md={4}>
                      <Grid container className={`${classes.headMenus}`}  justifyContent='center' alignItems='center'>
                        <Grid item md={10}>
                          <Grid container className={`${classes.menuButton}`}
                          justifyContent='center' alignItems='center'
                          onClick={()=>changeOpenStatus(menuHead.name)}>
                            <Typography variant='h6'>
                              {menuHead.name}
                            </Typography>
                          </Grid>
                          <Grid container justifyContent='center' alignItems='center'
                          className={`${menuHead.openStatus?classes.itemMenusOpened:classes.itemMenusClosed} ${classes.effect}`}>
                            {menuHead.items.map((itemMenu)=>(
                              <Grid item xs={12}>
                                <Link to={itemMenu.linkTo} onClick={()=>changeOpenStatus(menuHead.name)}>
                                  <Grid container className={classes.itemMenuButton} justifyContent='center' alignItems='center'>
                                    {itemMenu.name}
                                  </Grid>
                                </Link>
                              </Grid>
                            ))}
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>)
                  }else{
                  return(
                    // Head no child
                    <Grid item md={4}>
                      <Grid container justifyContent='center' alignItems='center'>
                        <Grid item md={10}>
                          <Grid container className={`${classes.menuButton} ${classes.effect}`} justifyContent='center' alignItems='center'>
                            <Link to={menuHead.linkTo}>
                              <Typography variant='h6'>
                                {menuHead.name}    
                              </Typography>
                            </Link>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  )
                }
              })}
            </Grid>
          </Grid>
          {/* Sign-in Menus */}
          <Grid item md={4}>
            <Grid container justifyContent='flex-end' alignItems='stretch'>
              <Grid item md={5}>
                <Grid container justifyContent='center' alignItems='center'>
                  <Grid item md={10}>
                    <Link to='/signin'>
                      <Grid container className={`${classes.signButton}`} justifyContent='center' alignItems='center'>
                        <Typography variant='h6'>Sign In</Typography>
                      </Grid>
                    </Link>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item md={5}>
                <Grid container justifyContent='center' alignItems='center'>
                  <Grid item md={10}>
                    <Link to='/signup'>
                      <Grid container className={`${classes.signButton}`} justifyContent='center' alignItems='center'>
                        <Typography variant='h6'>Sign Up</Typography>
                      </Grid>
                    </Link>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>

    // <Toolbar>
    //   <img src={require('../logo.png').default} className={props.useStyle.imageLogo}/>
    //   <Typography variant='h6'className={props.useStyle.toolBarTitle} align='center'>Pundi Receh</Typography>
    //   {props.token ? <Typography variant='h5'>Welcome, {props.token}</Typography>:''}
    //   {menu}
    // </Toolbar>
  )
}


export default function Header2(props) {
  const classes = useStyles()
  const [allMenuItem, setAllMenuItem] = useState([
    {name:'Financial',linkTo:'',head:true, openStatus:false,items:[
      {name:'Stock Analysis',linkTo:'/playlist'},
      {name:'Checklist',linkTo:'/checklist'},
      {name:'Heat Map',linkTo:'/heatmap'},
    ]},
    {name:'Resources',linkTo:'',head:true,openStatus:false,items:[
      {name:'Stock Analysis',linkTo:'/playlist'},
      {name:'Checklist',linkTo:'/checklist'},
      {name:'Heat Map',linkTo:'/heatmap'},
    ]},
    {name:'About',linkTo:'/aboutus',head:false,openStatus:false,items:''},
  ])
  function handleToken(value) {
    props.setToken(value);
  }
  function Logout(){
    handleToken('');
  }



  

  const menuItem=[
    {name:'Dashboard', linkTo:'/'},
    {name:'Stock Analysis', linkTo:'/playlist'},
    {name:'Checklist', linkTo:'/checklist'},
    {name:'Heat Map', linkTo:'/heatmap'},
    {name:'About Us', linkTo:'/aboutus'}
  ]
  const menu = (
    <ButtonGroup>
      {menuItem.map(item=>(
        <Link to={item.linkTo}><Button variant='contained' color='primary'>{item.name}</Button></Link>
      ))}
      {
        props.token ? 
        <Button variant='contained' color='primary' onClick={()=>handleToken('')}>Logout</Button> : 
        <><Link to='signin'><Button variant='contained' color='primary'>Signin</Button></Link>
        <Link to='/signup'><Button variant='contained' color='primary'>Signup</Button></Link>
        </>
      }
    </ButtonGroup>
  )
  function DisplayMobile (menuItem, props, Logout){
    // const [drawer, setDrawer] = useState(false)
    const handleDrawerOpen = ()=>{
      props.setDrawerOpen(true);
    }
    const handleDrawerClose = ()=>{
      props.setDrawerOpen(false);
    }
    return(
      <Toolbar>
        <Link to='/'><img src={require('../logo.png').default} className={props.useStyle.imageLogo}/></Link>
        <Typography variant='h6'className={props.useStyle.toolBarTitle} align='center'>Pundi Receh</Typography>
        <IconButton
          {...{
            edge: "end",
            color: "inherit",
            "aria-label": "menu",
            "aria-haspopup": "true",
            onClick: handleDrawerOpen
          }}
        >
          <MenuIcon />
        </IconButton>
        {/* <Drawer
          {...{
            anchor: "right",
            open: props.drawerOpen,
            onClose: handleDrawerClose
          }}
        >
          {menuItem.map(item=>(
            <Link to={item.linkTo} color='inherit'><MenuItem className={props.useStyle.mobileMenuItem}>{item.name}</MenuItem></Link>
          ))}
          {props.token ? 
          <Link color='inherit' onClick={()=>Logout}><MenuItem>Signout</MenuItem></Link> : 
          <><Link to='/signin' color='inherit'><MenuItem className={props.useStyle.mobileMenuItem}>Signin</MenuItem></Link>
          <Link to='/signup' color='inherit'><MenuItem className={props.useStyle.mobileMenuItem}>Signup</MenuItem></Link></>
          }
        </Drawer> */}
      </Toolbar>
    )
  }

  return (
    <ThemeProvider theme={customTheme}>
      <AppBar position='sticky' className={classes.headerStyle}>
        {/* {DisplayDesktop(allMenuItem, setAllMenuItem, Logout())} */}
        {/* {props.mobileView ? DisplayMobile(menuItem,props, Logout()) : DisplayDesktop(allMenuItem, setAllMenuItem, Logout())} */}
        {/* {console.log(props.mobileView)} */}
        {props.mobileView ? console.log('test') : DisplayDesktop(allMenuItem, setAllMenuItem, ()=>handleToken(''))}
      </AppBar>
    </ThemeProvider> 
  )
}