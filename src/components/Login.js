import React, {useState} from 'react'
import Add from '../databases/Add'

export default function Signin({state, setToken}){
    const [uname, setUname] = useState('');
    const LogingIn = (e) => {
        e.preventDefault();
        setToken(uname);
    }
    
    function addMember(){
        Add()
    }

    return (
    <div className="login-wrapper">
        <h1>Please Log In</h1>
        <form onSubmit={LogingIn}>
            <label>
                <p>Username</p>
                <input type="text" value={uname} required onChange={e=> setUname(e.target.value)}/>
            </label>
            <label>
                <p>Password</p>
                <input type="password" required />
            </label>
            <div>
                <button type="submit" onClick={()=>addMember()}>Submit</button>
            </div>
        </form>
    </div>
    )
}