import React, {useState, useEffect} from 'react'
import { Grid, AppBar, Toolbar, Typography, Button, ThemeProvider, ButtonGroup, MenuItem, IconButton, Drawer} from '@material-ui/core'
import {Link} from "react-router-dom"
import Cookies from 'universal-cookie'
import useStyle from '../resources/css/HeaderStyle'
import customTheme from '../customTheme'
import MenuIcon from '@material-ui/icons/Menu';

export default function Header(props) {
  const classes = useStyle()
  const cookies = new Cookies()
  const menuItem=[
    {name:'Dashboard', linkTo:'/'},
    {name:'Stock Analysis', linkTo:'/playlist'},
    {name:'Checklist', linkTo:'/checklist'},
    {name:'Heat Map', linkTo:'/heatmap'},
    {name:'About Us', linkTo:'/aboutus'}
  ]
  const menu=(
    <Grid container justifyContent='flex-end'>
      {menuItem.map(item=>(
        <Grid item className={!props.token?classes.menuWidthin:classes.menuWidthout} key={item}>
          <Grid container justifyContent='center' alignItems='center' className={classes.headerHeight}>
            <Link to={item.linkTo}><Button variant='contained' color='primary'>{item.name}</Button></Link>
          </Grid>
        </Grid>
        ))}
      {
        props.token ? (
          <Grid item className={!props.token?classes.menuWidthin:classes.menuWidthout}>
            <Grid container justifyContent='center' alignItems='center' className={classes.headerHeight}>
              <Button variant='contained' color='primary' onClick={()=>{Logout()}}>Signout</Button>
            </Grid>
          </Grid>
        ):(
          <>
            <Grid item className={!props.token?classes.menuWidthin:classes.menuWidthout}>
              <Grid container justifyContent='center' alignItems='center' className={classes.headerHeight}>
                <Link to='signin'><Button variant='contained' color='primary'>Signin</Button></Link>
              </Grid>
            </Grid>
            <Grid item className={!props.token?classes.menuWidthin:classes.menuWidthout}>
              <Grid container justifyContent='center' alignItems='center' className={classes.headerHeight}>
                <Link to='/signup'><Button variant='contained' color='primary'>Signup</Button></Link>
              </Grid>
            </Grid>
          </>
        )
      }
    </Grid>
  )

  function handleToken(value) {
    props.setToken(value);
  }
  function Logout(){
    console.log('logout')
    cookies.remove('Loggedin')
    props.setToken(false)
  }

  function setLoginData(data){
    console.log(`LoginData -> ${data}`)
    props.setToken(data)
  }


  useEffect(()=>{
    if(cookies.get('Loggedin') && props.token===false){
      console.log(`Logged in already -> ${cookies.get('Loggedin', {doNotParse: true})}`)
      // logged in state
      setLoginData(cookies.get('Loggedin'))
    }
  },[])

  const displayMobile = (menuItem)=>{
    const handleDrawerOpen = ()=>{
      props.setDrawerOpen(true);
    }
    const handleDrawerClose = ()=>{
      props.setDrawerOpen(false);
    }
    return(
      <Toolbar>
        <Link to='/'><img src={require('../logo.png').default} className={props.useStyle.imageLogo}/></Link>
        <Typography variant='h6'className={props.useStyle.toolBarTitle} align='center'>Pundi Receh</Typography>
        <IconButton
          {...{
            edge: "end",
            color: "inherit",
            "aria-label": "menu",
            "aria-haspopup": "true",
            onClick: handleDrawerOpen
          }}
        >
          <MenuIcon />
        </IconButton>
        <Drawer
          {...{
            anchor: "right",
            open: props.drawerOpen,
            onClose: handleDrawerClose,
          }}
        >
          {menuItem.map(item=>(
            <Link to={item.linkTo} color='inherit'><MenuItem className={props.useStyle.mobileMenuItem}>{item.name}</MenuItem></Link>
          ))}
          {props.token ? 
          <Link color='inherit' onClick={()=>{Logout()}}><MenuItem>Signout</MenuItem></Link> : 
          <><Link to='/signin' color='inherit'><MenuItem className={props.useStyle.mobileMenuItem}>Signin</MenuItem></Link>
          <Link to='/signup' color='inherit'><MenuItem className={props.useStyle.mobileMenuItem}>Signup</MenuItem></Link></>
          }
        </Drawer>
      </Toolbar>
    )
  }

  const displayDesktop = (menu)=>{
    return(
      <Grid container>
        <Grid item md={1}>
          <Grid container justifyContent='center' alignItems='center' className={classes.headerHeight}>
            <img src={require('../logo.png').default} className={classes.imageLogo}/>
          </Grid>
        </Grid>
        <Grid item md>
          <Grid container justifyContent='center' alignItems='center' className={classes.headerHeight}>
            <Typography variant='h6' align='center'>Pundi Receh</Typography>
          </Grid>
        </Grid>
        {props.token?(
          <Grid item md={2}>
            <Grid container justifyContent='flex-end' alignItems='center' className={classes.headerHeight}>
              {console.log(`props.token.call -> ${props.token.call}`)}
              {props.token ? <Typography variant='h5'>{`Welcome, ${props.token.call}`}</Typography>:''}
            </Grid>
          </Grid>
        ):null}
        <Grid item md={6}>
          <Grid container justifyContent='center' alignItems='center' className={classes.headerHeight}>
            {menu}
          </Grid>
        </Grid>
        
      </Grid>
    )
  }

  return(
    <ThemeProvider theme={customTheme}>
      <AppBar position='sticky' className={props.useStyle.headerStyle}>
        {props.mobileView ? displayMobile(menuItem) : displayDesktop(menu)}
      </AppBar>
    </ThemeProvider> 
  );
  
}