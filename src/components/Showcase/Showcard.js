import React from 'react'
import { useState } from 'react';
import {Grid, Card, Typography, CardHeader, Avatar, CardContent, Paper} from '@material-ui/core'
import '../../resources/css/stockItem.css'

const Showcard = (props) => {
    return (
        <Paper style={{height:'100%'}}>
            <CardContent>
                <Grid container style={{gap:'1vw'}} justifyContent='space-between'>
                    <Grid item sm={6}>
                        <Typography variant='h6' style={{fontStyle:'bold'}}>
                            {props.data.title}
                        </Typography>
                        <Typography variant='h4'>
                            {props.data.number}
                        </Typography>
                    </Grid>
                    <Grid item sm={4}>
                        <Avatar style={{width:'4.5vw', minWidth:50, minHeight:50 ,height:'4.5vw', backgroundColor:props.data.bgcolor}} >
                            {props.data.icon}
                        </Avatar>
                    </Grid>
                    <Grid item sm={12}>
                        <Typography variant='subtitle1'>
                            {props.data.detail}
                        </Typography>
                    </Grid>
                </Grid>
            </CardContent>
        </Paper>
    );
  };
  
export default Showcard;