import React from 'react'
import { useState } from 'react';
import {Grid, Card, Typography, CardHeader, Link, ThemeProvider} from '@material-ui/core'
import '../../resources/css/stockItem.css'
import { ThumbUpAlt, AttachMoney, Money, AccountBalance}from '@material-ui/icons'
import { grey, blue, cyan } from '@material-ui/core/colors'
import SingleCard from './Showcard'
import useStyles from '../../resources/css/Styles'
import {useTranslation} from "react-i18next"

const ShowSection = (props) => {
    const { t } = useTranslation()
    const datas=[
        {title:'Success Rate', number:'67%', icon:<ThumbUpAlt fontSize='large' color='primary'/>, bgcolor: cyan['A200'], detail:t('performance1')},
        {title:'Average Return', number:'+7.1%', icon:<AttachMoney fontSize='large' color='primary'/>, bgcolor: cyan['A200'], detail:t('performance2')},
        {title:'12 Month Return', number:'+12.21%', icon:<Money fontSize='large' color='primary'/>, bgcolor: cyan['A200'], detail:t('performance3')},
        {title:'YTD Returns', number:'+12.21%', icon:<AccountBalance fontSize='large' color='primary'/>, bgcolor: cyan['A200'], detail:t('performance4')},
    ]
    const useStyle = useStyles()
    return (
        <>
            <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                <Grid container className={useStyle.darkerBackground} style={{gap:'2vw'}} justifyContent='center'>
                    <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
                        <Grid container justifyContent='center' alignContent='center' style={{ height:'20vh', minHeight:100,maxHeight:125}}>
                            <Typography className={`${useStyle.gradientText} ${useStyle.gradientTextColor1} ${useStyle.subTitle} ${useStyle.LatoHeadline}`} variant='h2' align="center">
                                {t('performance')}
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid item xl={10} lg={10} md={11} sm={11} xs={11}>
                        <Grid container style={{gap:'2vw'}} justifyContent='center'>
                            {datas.map(data=>(
                                <Grid item xl={2} lg={2} md={5} sm={5} xs={11}>
                                    <SingleCard data={data}/>
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>
                    <Grid item xl={6} lg={6} md={8} sm={8} xs={8}>
                        <Grid container justifyContent='center' alignContent='center' alignItems='center'>
                            <Typography variant='h6' align="center" className={useStyle.LatoHeadline}>
                                {t('performanceDetail')}
                            </Typography>
                            <Link variant='h6' style={{color:'blue'}} underline='always' href='https://www.tipranks.com/investors/2153349/pundi-receh'>
                                Link
                            </Link>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
  };
  
export default ShowSection;