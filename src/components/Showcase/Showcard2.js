import React from 'react'
import { useState } from 'react';
import {Grid, Card, Typography, CardHeader, Avatar, CardContent, Paper} from '@material-ui/core'
import '../../resources/css/stockItem.css'

const Showcard2 = (props) => {
    return (
        <Paper style={{height:'100%'}}>
            <CardContent>
                <Grid container style={{gap:20}} justifyContent='center'>
                    <Grid item sm={12}>
                        <Grid container justifyContent='center'>
                            <Typography variant='h6' align='center'>
                                {props.data.title}
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid item sm={12}>
                        <Grid container justifyContent='center'>
                            <Avatar style={{width:90,height:90, backgroundColor:props.data.bgcolor, color:'black', fontSize:'30px'}} >
                                {props.data.number}                                
                            </Avatar>
                        </Grid>
                    </Grid>
                    <Grid item sm={12}>
                        <Grid container justifyContent='center'>
                            <Typography variant='subtitle' align='center'>
                                {props.data.detail}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </CardContent>
        </Paper>
    );
  };
  
export default Showcard2;