import React from "react";
import { useEffect, useState } from "react";
import { TickerTape } from "react-ts-tradingview-widgets";
import '../../resources/css/tickerTape.css';

function TickerTapes() {
  // const fixedText = "I am fixed :)";
  // const whenNotFixed = "I am not a fixed header :(";
  // const [headerText, setHeaderText] = useState(whenNotFixed);
  useEffect(() => {
    const header = document.getElementById("tickerTape");
    const sticky = header.offsetTop;
    const scrollCallBack = window.addEventListener("scroll", () => {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
        // if (headerText !== fixedText) {
        //   setHeaderText(fixedText);
        // }
      } else {
        header.classList.remove("sticky");
        // if (headerText !== whenNotFixed) {
        //   setHeaderText(whenNotFixed);
        // }
      }
    });
    return () => {
      window.removeEventListener("scroll", scrollCallBack);
    };
  }, []);
  return (
    <div>
      <div className="stickerHeader" id="tickerTape">
        <TickerTape colorTheme="dark" autosize></TickerTape>
      </div>
    </div>
  );
}

export default TickerTapes;
