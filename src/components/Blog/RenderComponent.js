import React from 'react'
import {TextField, Grid} from '@mui/material'
import { FaTrashAlt as Trash} from 'react-icons/fa'
import useStyle from '../../resources/css/components/Renderer'

export default function RenderComponent(props) {
    const classes = useStyle()
    return (
        props.type=='text'?(

            <Grid container justifyContent='center' >
                <Grid item xs={12} className={classes.paragraph}>
                {props.value[props.lang]}
                </Grid>
            </Grid>

        ):props.type=='pict'?(
            <Grid container justifyContent='center' >
                <Grid item xs={12}>
                    <img src={props.value} className={classes.image}/>
                </Grid>
            </Grid>
        ):props.type=='bold1'?(

            <Grid container justifyContent='center' >
                <Grid item xs={12} className={classes.bold1}>
                {props.value[props.lang]}
                </Grid>
            </Grid>

        ):(
            <Grid container justifyContent='center' >
                <Grid item xs={12} className={classes.bold2}>
                {props.value[props.lang]}
                </Grid>
            </Grid>
        )
    )
}
