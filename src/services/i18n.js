import i18next from "i18next";
import { initReactI18next } from "react-i18next";

// "Inline" English and Arabic translations. 
// We can localize to any language and any number of languages.
const resources = {
  en: {
    translation: {
        potential1: "Expert Market Analysis",
        potential2: "Proprietary Quant Ratings",
        potential3: "Unlimited Analysis Access",
        potential4: "Earnings Call",
        potential5: "Powerfull Stock Screeners",
        potential6: "Expert Market Portofolio",
        performance: "OUR PERFORMANCE",
        performanceDetail:'Pundi receh is at Tipranks.com to compare with other world class analyst and S&P500 benchmark. We are able to beat Tipranks best analyst and S&P500 in 2021. Please click below for more detail:',
        performance1: '18 out of 27 profitable transaction',
        performance2: 'Average return / transaction',
        performance3: 'Return in the last 12 months',
        performance4: 'Return since the beginning of current fiscal year',
        reason: "WHY US?",
        reason1Title: "Affordable",
        reason1Detail: "Pundi Receh helps individuals and professionals in Indonesia and Singapore to get the most affordable financial knowledge",
        reason2Title: "Both Fundamental & Technical",
        reason2Detail: "Combining fundamental and technical analysis to get the whole understanding of the financial world",
        reason3Title: "2 Available Languages",
        reason3Detail: "Materials available in Bahasa and English.",
        reason4Title: "By Professionals",
        reason4Detail: "Taught by Donny Justin, a financial educator and blogger who completed 3 levels of CFA.",
        aboutSubTitle:'All about investment and finance',
        aboutVision:'Everyone has the right to get affordable financial education',
        aboutMission:'To create opportunity for everyone to get financial education for their better future in a fun, easy to understand and safer way',
        aboutParagraph:'We believe that financial literacy is vital. We started off as financial bloggers, podcasters and youtubers. Today, we still are, but we improve our services to online financial and investment courses, plus a lifetime member forum discussion group. We aim to educate people in the most fun and affordable way possible-to make you financially stable',
        formulaValue: "Value Company",
        formulaFinancial: "Financial Company",
        formulaGrowth: "Growth Company",
        myprofileTitle: "My Profile",
        yourPlanTitle: "Your plan",
        includedTitle: "Included in your plan",
        billingTitle: "Billing and payment"
    },
  },
  id: {
    translation: {
        potential1: "Expert Market Analysis",
        potential2: "Proprietary Quant Ratings",
        potential3: "Unlimited Analysis Access",
        potential4: "Earnings Call",
        potential5: "Powerfull Stock Screeners",
        potential6: "Expert Market Portofolio",
        performance: "Performa Kita",
        performanceDetail:'Pundi receh ada di Tipranks.com untuk membandingkan diri kami dengan analis kelas dunia dan benchmark S&P500. Hasilnya Pundi receh mampu mengalahkan analis terbaik Tipranks dan S&P500 untuk tahun 2021. Silakan klik di bawah ini untuk lebih detail:',
        performance1: '18 dari 27 transaksi yang menguntungkan',
        performance2: 'Rata-rata pengembalian / transaksi',
        performance3: 'Kembali dalam 12 bulan terakhir',
        performance4: 'Kembali sejak awal tahun fiskal berjalan',
        reason: "MENGAPA KITA?",
        reason1Title: "Terjangkau",
        reason1Detail: "Pundi Receh membantu individu dan profesional di Indonesia dan Singapura untuk mendapatkan pengetahuan keuangan yang paling terjangkau",
        reason2Title: "Fundamental & Teknis",
        reason2Detail: "Menggabungkan analisis fundamental dan teknikal untuk mendapatkan pemahaman menyeluruh tentang dunia keuangan",
        reason3Title: "Tersedia dalam 2 bahasa",
        reason3Detail: "Materi tersedia dalam Bahasa Indonesia dan Bahasa Inggris.",
        reason4Title: "Oleh Profesional",
        reason4Detail: "Diajarkan oleh Donny Justin, seorang financial Educator dan blogger yang menyelesaikan 3 level CFA.",
        aboutSubTitle:'Semua Hal Tentang Investasi dan Keuangan',
        aboutVision:'Semua berhak untuk edukasi keuangan yang terjangkau ',
        aboutMission:'Menciptakan kesempatan pendidikan keuangan untuk masa depan yang lebih baik dengan cara yang menyenangkan, aman dan mudah dipahami',
        aboutParagraph:'Kami percaya bahwa financial literacy sangat penting buat semua orang, sehingga kami mulai menulis dan melengkapi blog ini agar dapat menjadi wahana belajar bagi siapa saja yang tertarik mempelajari soal finansial dan juga investasi. Pundi Receh hadir mengikuti perkembangan zaman dengan media sosial, podcast, komik, video, kelas online, komunitas, dan analisa saham',
        formulaValue: "Value Company",
        formulaFinancial: "Financial Company",
        formulaGrowth: "Growth Company",
        myprofileTitle: "My Profile",
        yourPlanTitle: "Your plan",
        includedTitle: "Included in your plan",
        billingTitle: "Billing and payment"
    },
  },
};

i18next
  .use(initReactI18next)
  .init({
    resources,
    lng: "en",
    interpolation: {
      escapeValue: false,
    },
  });

export default i18next;