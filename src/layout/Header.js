import React, {useState, useEffect} from 'react'
import { Grid,  Toolbar, Typography, Button, ThemeProvider, MenuItem, IconButton, Drawer, Collapse} from '@material-ui/core'
import {Link, useHistory} from "react-router-dom"
import Cookies from 'universal-cookie'
import useStyle from '../resources/css/layout/Header'
import customTheme from '../customTheme'
import MenuIcon from '@material-ui/icons/Menu';

import Logo from '../resources/images/header/logo2.png'
import Name from '../resources/images/header/name.png'

export default function Header(props) {
  const history = useHistory()
  const classes = useStyle()
  const [open, setOpen] = useState(false)
  const cookies = new Cookies()
  function openMenu(code){
      if(open===code){
            setOpen(false)
      }else{
            setOpen(code)
      }
  }
  const menuItem=[
        {code:'fin', text:'Financial', to:'', submenu:[
            {code:'san', text:'Stock Analysis', to:'/playlist'},
            {code:'chc', text:'Checklist', to:'/checklist'},
            {code:'mht', text:'Forex Heatmap', to:'/heatmapForex'},
            {code:'cht', text:'Crypto Heatmap', to:'/heatmap'}
        ]},
        {code:'res', text:'Resources', to:'', submenu:[
            {code:'fbl', text:'Finance Blog', to:'/blog'},
            {code:'fpo', text:'Finance Podcast', to:'/podcast'},
            {code:'prc', text:'Pundi & Receh Comic', to:'/comic'}
        ]},
        {code:'abs', text:'About Us', to:'/aboutus', submenu:''},
  ]
  const menu=(
    <Grid container justifyContent='flex-end' alignItems='center'>
      {menuItem.map(item=>(
        <Grid item className={classes.menuButton}>
            {item.submenu===''?(
              <Link to={item.to} className={classes.menuItem}>
                <Grid container className={`${classes.menuButtonText}`} justifyContent='center' onClick={()=>openMenu(item.code)}>
                    {item.text}
                </Grid>
              </Link>
            ):(
                <>
                <Grid container className={classes.menuButtonText} justifyContent='center' onClick={()=>openMenu(item.code)}>
                    {item.text}
                </Grid>
                <Grid container className={`${classes.subMenuContainer}`}>
                    <Collapse in={open===item.code}>
                        <Grid container className={classes.subMenuPanel}>
                            {item.submenu.map((subitem,index)=>(
                              <Link to={subitem.to} className={classes.menuItem} onClick={()=>openMenu(false)}>
                                <Grid item xs={12}>
                                    <Grid container className={`${index===item.submenu.length-1?classes.botMenu:(index===0?classes.topMenu:null)} ${classes.subMenuItem}`}>
                                        {subitem.text}
                                    </Grid>
                                </Grid>
                              </Link>
                            ))}
                        </Grid>
                    </Collapse>
                </Grid>
                </>
            )}
            
        </Grid>
        ))}
      {
        props.token ? (
          <>
            <Grid item className={classes.menuButton}>
              <Link to={'/myprofile'} className={classes.menuItem}>
                <Grid container className={`${classes.menuButtonText}`} justifyContent='center' onClick={()=>openMenu()}>
                    {'My Profile'}
                </Grid>
              </Link>
            </Grid>
            <Grid item className={classes.menuButton}>
              <Grid container justifyContent='center' alignItems='center' className={classes.menuButtonText}>
                  <Grid container className={classes.signLink} onClick={()=>{Logout()}}>
                    Signout
                  </Grid>
              </Grid>
            </Grid>
          </>

        ):(
          <>
            <Grid item className={classes.menuButton}>
              <Grid container justifyContent='center' alignItems='center' className={classes.menuButtonText}>
                <Link to='/signin' className={classes.signLink}>Signin</Link>
              </Grid>
            </Grid>
            <Grid item className={classes.menuButton}>
              <Grid container justifyContent='center' alignItems='center' className={classes.menuButtonText}>
                <Link to='/signup' className={classes.signLink}>Signup</Link>
              </Grid>
            </Grid>
          </>
        )
      }
    </Grid>
  )
  //useEffect -> listener, onscroll, close menu

  function handleToken(value) {
    props.setToken(value);
  }
  function Logout(){
    console.log('logout')
    cookies.remove('Loggedin')
    props.setToken(false)
    history.push('/')
  }

  function setLoginData(data){
    console.log(`LoginData -> ${data}`)
    props.setToken(data)
  }


  useEffect(()=>{
    if(cookies.get('Loggedin') && props.token===false){
      console.log(`Logged in already -> ${cookies.get('Loggedin', {doNotParse: true})}`)
      // logged in state
      setLoginData(cookies.get('Loggedin'))
    }
  },[])

  const displayMobile = (menuItem)=>{
    const handleDrawerOpen = ()=>{
      props.setDrawerOpen(true);
    }
    const handleDrawerClose = ()=>{
      props.setDrawerOpen(false);
    }
    return(
      <Toolbar>
        <Link to='/'><img src={require('../logo.png').default} className={props.useStyle.imageLogo}/></Link>
        <Typography variant='h6'className={props.useStyle.toolBarTitle} align='center'>Pundi Receh</Typography>
        <IconButton
          {...{
            edge: "end",
            color: "inherit",
            "aria-label": "menu",
            "aria-haspopup": "true",
            onClick: handleDrawerOpen
          }}
        >
          <MenuIcon />
        </IconButton>
        <Drawer
          {...{
            anchor: "right",
            open: props.drawerOpen,
            onClose: handleDrawerClose,
          }}
        >
          {menuItem.map(item=>(
            <Link to={item.linkTo} color='inherit'><MenuItem className={props.useStyle.mobileMenuItem}>{item.name}</MenuItem></Link>
          ))}
          {props.token ? 
          <Link color='inherit' onClick={()=>{Logout()}}><MenuItem>Signout</MenuItem></Link> : 
          <><Link to='/signin' color='inherit'><MenuItem className={props.useStyle.mobileMenuItem}>Signin</MenuItem></Link>
          <Link to='/signup' color='inherit'><MenuItem className={props.useStyle.mobileMenuItem}>Signup</MenuItem></Link></>
          }
        </Drawer>
      </Toolbar>
    )
  }

  const displayDesktop = (menu)=>{
    return(
      <Grid container alignItems='center' justifyContent='space-between' className={classes.headerContainer}>
        <Grid item>
            <Grid container justifyContent='flex-start' alignItems='center' className={classes.logoNameContainer}>
                <Grid item>
                    <Link to='/dashboard'>
                        <Grid container className={classes.logoContainer}>
                            <img src={Logo} className={classes.logo}/>
                        </Grid>
                    </Link>
                </Grid>
                <Grid item>
                    <Grid container>    
                        <Grid item xs={12}>
                            <img src={Name} className={classes.name}/>
                        </Grid>
                        <Grid item xs={12}>
                            Semua Hal Tentang Investasi dan Keuangan
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
        <Grid item>
            <Grid container justifyContent='flex-end' alignItems='center' className={classes.menuButtonsContainer}>
                {menu}
            </Grid>
        </Grid>
      </Grid>
    )
  }

  return(
    <ThemeProvider theme={customTheme}>
        {/* {props.mobileView ? displayMobile(menuItem) : displayDesktop(menu)} */}
        {displayDesktop(menu)}
    </ThemeProvider> 
  );
  
}
