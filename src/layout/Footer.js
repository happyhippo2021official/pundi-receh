import React from 'react'
import { Grid } from '@material-ui/core'
import {Link} from "react-router-dom"
import useStyle from '../resources/css/layout/Footer'
import customTheme from '../customTheme'

import wapict from '../resources/images/wa.png'
import ytpict from '../resources/images/yt.png'
import twpict from '../resources/images/twt.png'
import igpict from '../resources/images/ig.png'
import LanguageButton from '../components/LanguageButton'


import '../resources/css/footer.css'

export default function Footer() {
    const classes = useStyle()
    const links = [
        {code:'abs', text:'About Us', to:'/aboutus', submenu:''},
        {code:'cts', text:'Contact Us', to:'/', submenu:''},
        {code:'faq', text:'FAQ', to:'/', submenu:''}
    ]
  return (
      <Grid container className={classes.outsideContainer}>
        <Grid item md={4} xs={12}>
            <Grid container className={classes.sectionContainer}>
                <Grid container justifyContent='flex-end' className={`${classes.partSections} ${classes.headPart} ${classes.right}`}>
                    Get Connected
                </Grid>
                <Grid container justifyContent='flex-end' className={classes.partSections}>
                    <img src={wapict} className={classes.pict}/>
                    <img src={twpict} className={classes.pict}/>
                    <img src={igpict} className={classes.pict}/>
                    <img src={ytpict} className={classes.pict}/>
                </Grid>
                <Grid container justifyContent='flex-end' className={`${classes.partSections} ${classes.textPart}  ${classes.right}`}>
                    For further informations and contacts, reach us in these social medias.
                </Grid>
            </Grid>
        </Grid>
        <Grid item md={4} xs={12}>
            <Grid container className={classes.sectionContainer}>
                <Grid container justifyContent='center' className={`${classes.partSections} ${classes.headPart}  ${classes.center}`}>
                    Office
                </Grid>
                <Grid container justifyContent='center' className={`${classes.partSections} ${classes.textPart}  ${classes.center}`}>
                Pundi Receh Pte Ltd<br/><br/>
                20 Cecil Street #05-03<br/>
                Singapore, 049705<br/>
                <br/>
                sobatmu@pundireceh.com<br/>
                </Grid>
            </Grid>
        </Grid>
        <Grid item md={4} xs={12}>
            <Grid container justifyContent='space-between' className={classes.sectionContainer}>
                <Grid item>
                    <Grid container className={`${classes.partSections} ${classes.headPart}`}>
                        Links
                    </Grid>
                    <Grid container className={`${classes.partSections} ${classes.textPart}`}>
                        {links.map((link)=>{
                            return(
                                <Grid container >
                                    <Link to={link.to} className={classes.footlink}>
                                        {link.text}
                                    </Link>
                                </Grid>
                            )
                        })}
                    </Grid>
                </Grid>
                <Grid item>
                    <LanguageButton />
                </Grid>
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <Grid container justifyContent='center' className={classes.copyright}>
            &copy; {new Date().getFullYear()} Copyright: &nbsp; <a href="https://www.pundireceh.com" className={classes.link}> pundireceh.com </a>
            </Grid>
        </Grid>
      </Grid>
  )
}
