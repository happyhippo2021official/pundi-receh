import React, {useEffect, useState} from 'react'
import {  Route, Switch, useHistory} from 'react-router-dom'
import Cookies from 'universal-cookie'

import Login from './Pages/Login'
import Home from './Pages/Home'
import Container from './Layout/Container'
import AdminMaster from './Pages/AdminMaster'
import UserMaster from './Pages/UserMaster'
import ContentMaster from './Pages/ContentMaster'
import ResourceMaster from './Pages/ResourceMaster'
import ContentList from './Pages/ContentList'
import MyPage from './Pages/MyPage'

export default function Routing() {
  const history = useHistory()
  const [adminData, setAdminData] = useState(false)
  const cookies = new Cookies()
  useEffect(()=>{
    if(cookies.get('LoggedinAdmin') && !adminData){
      setAdminData(cookies.get('LoggedinAdmin'))
    }else{
      history.push('/admin')
      setAdminData(false)
    }
  },[])
  return (
     <Switch>
        <Route path='/admin/Home'>
          <Container load={Home} title='Home' setAdminData={setAdminData} adminData={adminData}/>
        </Route>
        <Route path='/admin/AdminMaster'>
          <Container load={AdminMaster} title='Admin Master' setAdminData={setAdminData} adminData={adminData}/>
        </Route>
        <Route path='/admin/UserMaster'>
          <Container load={UserMaster} title='User Master' setAdminData={setAdminData} adminData={adminData}/>
        </Route>
        <Route path='/admin/Accounting'>
          <Container load={AdminMaster} title='Paypal Report' setAdminData={setAdminData} adminData={adminData}/>
        </Route>
        <Route path='/admin/ContentMaster/:id'>
          <Container load={ContentMaster} title='Content Master' setAdminData={setAdminData} adminData={adminData}/>
        </Route>
        <Route path='/admin/ContentMaster'>
          <Container load={ContentMaster} title='Content Master' setAdminData={setAdminData} adminData={adminData}/>
        </Route>
        <Route path='/admin/ContentList'>
          <Container load={ContentList} title='Content List' setAdminData={setAdminData} adminData={adminData}/>
        </Route>
        <Route path='/admin/ResourceMaster'>
          <Container load={ResourceMaster} title='Resource Master' setAdminData={setAdminData} adminData={adminData}/>
        </Route>
        <Route path='/admin/MyPage'>
          <Container load={MyPage} title='Admin Personal Page' setAdminData={setAdminData} adminData={adminData}/>
        </Route>
        <Route path='/admin' >
          <Login setAdminData={setAdminData}/>
        </Route>
     </Switch>
  );
}
