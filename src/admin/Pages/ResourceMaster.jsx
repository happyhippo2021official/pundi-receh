import React, {useState} from 'react'
import {Grid, Tab} from '@material-ui/core'
import {useParams} from 'react-router-dom'
import useStyle from '../Resources/css/Content'
import Comic from '../Components/ResourceMaster/Comic'
import Podcast from '../Components/ResourceMaster/Podcast'
import Blog from '../Components/ResourceMaster/Blog'
import BlogMaster from '../Components/ResourceMaster/BlogMaster'

export default function ResourceMaster(props) {
  const {id} = useParams()
  const classes = useStyle()
  const tabs = [
    {code:'cmc', text:'Comic'},
    {code:'pod', text:'Podcast'},
    {code:'blglst', text:'Blog List'},
    {code:'blg', text:'Blog'},
  ]
  const [selectData,setSelectData] = useState(null)
  const [activeTab,setActiveTab] = useState('cmc')

  function renderSwitch(){
    switch(activeTab){
        case (tabs[0].code):{
          return(
            <Comic adminData={props.adminData}/>
          )
        }
        case (tabs[1].code):{
          return(
            <Podcast adminData={props.adminData}/>
          )
        }
        case (tabs[2].code):{
          return(
            <BlogMaster adminData={props.adminData} setActiveTab={setActiveTab} setSelectData={setSelectData}/>
          )
        }
        case (tabs[3].code):{
          return(
            <Blog adminData={props.adminData} selectData={selectData} setSelectData={setSelectData}/>
          )
        }
    }
  }

  return (
    <Grid container>
        <Grid item xs={12}>
          <Grid container justifyContent='flex-start' alignItems='center' className={classes.tabContainer}>
              {tabs.map((tab)=>{
                return(
                  <Tab key={tab.code} label={tab.text} onClick={()=>{setActiveTab(tab.code)}} className={`${classes.tabItem} ${activeTab===tab.code?classes.active:null}`}/>
                )
              })}
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Grid container justifyContent='center'>
            {renderSwitch()}
          </Grid>
        </Grid>
    </Grid>
  )
}
