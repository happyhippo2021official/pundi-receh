import React, {useState} from 'react';
import {Grid, TextField, Button, Collapse} from '@material-ui/core'
import useStyle from '../Resources/css/Mypage'

export default function Home(props) {
  const [cName, setCName] = useState(false)
  const [cPass, setCPass] = useState(false)
  const classes = useStyle()
  // function 
  return (
      <Grid container justifyContent='center' className={classes.contentHeight}>
        <Grid item md={8} xs={11}>
          <Grid container justifyContent='flex-start' alignItems='center'>
            <Grid item xs={4}>
              <Grid container justifyContent='flex-start' className={classes.title}>
                Name: 
              </Grid>
            </Grid>
            <Grid item xs={6}>
              <Grid container justifyContent='flex-start' className={[classes.title]}>
                {props.adminData.name}
              </Grid>
            </Grid>
            <Grid item xs>
              <Grid container justifyContent='center' className={classes.saveBox}>
                <Button className={classes.saveButton} onClick={()=>{setCName(!cName)}}>{cName?'Save':'Update'}</Button>
              </Grid>
            </Grid>

            <Collapse in={cName} className={classes.panel}>
              <Grid container>
                <Grid item xs={4}>
                  <Grid container justifyContent='flex-start' className={[classes.textboxTitle]}>
                    First Name
                  </Grid>
                </Grid>

                <Grid item xs={6}>
                  <TextField fullWidth />
                </Grid>

                <Grid item xs={4}>
                  <Grid container justifyContent='flex-start' className={[classes.textboxTitle]}>
                    Last Name
                  </Grid>
                </Grid>

                <Grid item xs={6}>
                  <TextField fullWidth />
                </Grid>
              </Grid>
            </Collapse>
            

            <Grid item xs={10}>
              <Grid container justifyContent='flex-start' className={classes.title}>
                Change Password
              </Grid>
            </Grid>
            <Grid item xs>
              <Grid container justifyContent='center' className={classes.saveBox}>
                <Button className={classes.saveButton} onClick={()=>{setCPass(!cPass)}}>{cPass?'Save':'Update'}</Button>
              </Grid>
            </Grid>
            
            <Collapse in={cPass} className={classes.panel}>
              <Grid container>
                <Grid item xs={4}>
                  <Grid container justifyContent='flex-start' className={classes.textboxTitle}>
                    New Password
                  </Grid>
                </Grid>
                <Grid item xs={6}>
                  <TextField fullWidth />
                </Grid>
                <Grid item xs={4}>
                  <Grid container justifyContent='flex-start' className={classes.textboxTitle}>
                    Confirm New Pass
                  </Grid>
                </Grid>
                <Grid item xs={6}>
                  <TextField fullWidth />
                </Grid>
              </Grid>
            </Collapse>
          </Grid>
        </Grid>

        <Grid item md={3} xs={11}>
          image
        </Grid>
        
      </Grid>
  );
}
