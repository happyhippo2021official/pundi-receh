import React, {useState, useEffect} from 'react'
import {Grid, Divider, TextField, Typography, Button} from '@material-ui/core'
import {useHistory} from 'react-router-dom'
import Cookies from 'universal-cookie'
import useStyle from '../Resources/css/LoginPage'
import {CheckLoginAdmin} from '../../databases/Read'

export default function Login(props) {
    const classes = useStyle()
    const cookies = new Cookies()
    const history = useHistory()
    const [loginData,setLoginData] = useState({
        username:'',
        password:''
    })

    useEffect(()=>{
        if(cookies.get('LoggedinAdmin')){
            history.push('/admin/Home')
        }
    })
    function CheckLogin(){
        CheckLoginAdmin(loginData).then((result)=>{
            if(result){
                const cookieData = {
                    username: result.username,
                    name: `${result.personname.givenname} ${result.personname.surname}`,
                    first: result.personname.givenname,
                    last: result.personname.surname,
                    position: result.position,
                    pict: result.pict
                }
                const cookies = new Cookies()
                cookies.set('LoggedinAdmin', cookieData, { path: '/' })
                props.setAdminData(cookieData)
                history.push('/admin/Home')
            }else{
                alert('wrong pass or usrname')
            }
        })
    }
    function ValueChange(e){
        const value = e.target.value
        setLoginData({
            ...loginData,
            [e.target.name]:value
        })
    }

    return (
        <Grid container justifyContent='center' alignItems='center' className={classes.outerContainer}>
            <Grid item md={4} xs={5}>
                <Grid container justifyContent='center' alignItems='center' className={classes.middleContainer}>
                    <Grid item xs={12}>
                        <Grid container justifyContent='center' alignItems='center' className={classes.section}>
                            <Typography align='center'>
                                Admin Login page
                            </Typography>
                        </Grid>
                    </Grid>
                    <Divider className={classes.divider}/>
                    <Grid item xs={12}>
                        <Grid container justifyContent='center' alignItems='center' className={classes.section}>
                            <Grid item xs={10}>
                                <Grid container justifyContent='center' alignItems='center'>
                                    <Typography align='center' className={classes.warningMessage}>
                                        Welcome to Admin Login page, if you're not Pundi Receh's administrator, PLEASE LEAVE AT ONCE!
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item xs={10}>
                                <Grid container justifyContent='center' alignItems='center'>
                                    <TextField 
                                        fullWidth
                                        type='input'
                                        value={loginData.username}
                                        name='username' 
                                        label='Username' 
                                        onChange={ValueChange}
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={10}>
                                <Grid container justifyContent='center' alignItems='center'>
                                    <TextField 
                                        fullWidth 
                                        type='password'
                                        value={loginData.password}
                                        name='password'
                                        label='Password' 
                                        onChange={ValueChange}
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={10}>
                                <Grid container justifyContent='center' alignItems='center'>
                                    <Button variant='contained' onClick={()=>{CheckLogin()}}>
                                        Login
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
