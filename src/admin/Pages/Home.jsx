import React from 'react';
import {Grid} from '@material-ui/core'
import useStyle from '../Resources/css/Home'

export default function Home(props) {
  const classes = useStyle()
  return (
      <Grid container justifyContent='center' alignItems='center' className={classes.contentHeight}>
        <Grid item>
          Home
        </Grid>
      </Grid>
  );
}
