import React, {useEffect, useState} from 'react';
import {Grid, TextField} from '@material-ui/core'
import {useHistory} from 'react-router-dom'
import {Table, TableBody, TableCell, TableRow, TableHead} from '@mui/material'
import {addImage, addComic} from '../../databases/Add'
import {GetAllUsers} from '../../databases/Read'
import useStyle from '../Resources/css/User'
import {FaTrashAlt} from 'react-icons/fa'

export default function AdminMaster() {
  const classes = useStyle()
  const history = useHistory()
  const [title, setTitle] = useState('')
  const [users, setUsers] = useState([])

  async function refresh(){
    const result = await GetAllUsers()
    console.log(result)
    setUsers([...result])
  }
  useEffect(async ()=>{
      refresh()
  },[])
  return (
    <Grid container>
      <Grid xs={11}>
        <Grid container className={classes.section}>
          User List
          <Grid container className={classes.list}>
            <Table aria-label='Content List'>
              <TableHead>
                <TableRow>
                  <TableCell className={classes.head}>Email</TableCell>
                  <TableCell className={classes.head} align='center'>Level</TableCell>
                  <TableCell className={classes.head} align='center'>Name</TableCell>
                  <TableCell className={classes.head} align='center'>Status</TableCell>
                  <TableCell className={classes.head} align='center'>Last Payment</TableCell>
                  <TableCell className={classes.head} align='center'>Package</TableCell>
                  <TableCell className={classes.head} align='center'>OK Leave</TableCell>
                  {/* <TableCell align='center'>Hide</TableCell> */}
                </TableRow>
              </TableHead>
              <TableBody>
                {users.map((datum, index)=>{
                  const userName = `${datum.username.firstname} ${datum.username.middlename} ${datum.username.lastname}`
                  return(
                    <TableRow key={index}>
                      <TableCell className={classes.comicTitle}>{datum.email}</TableCell>
                      <TableCell className={classes.comicPict} align='center'>{datum.level}</TableCell>
                      <TableCell className={classes.w5} align='center'>{userName}</TableCell>
                      <TableCell className={`${classes.w5} ${(datum.status=='active')?classes.greenFont:classes.redFont}`} align='center'>{datum.status}</TableCell>
                      <TableCell className={classes.w5} align='center'>Payment Date</TableCell>
                      <TableCell className={classes.w5} align='center'>Their package</TableCell>
                      <TableCell className={classes.w5} align='center'>
                        {(datum.status=='request')?(
                          <FaTrashAlt className={classes.delIcon} onClick={()=>{console.log('deleted')}}/>
                        ):null}
                      </TableCell>
                      {/* <TableCell align='center'><FaEyeSlash/></TableCell> */}
                    </TableRow>
                  )
                })}
              </TableBody>
            </Table>
          </Grid>
        </Grid>
      </Grid>
        {/* <input type='file' onChange={(event)=>{setImage(event.target.files[0])}}></input>
        <button onClick={uploadImage}> Upload Image</button> */}
    </Grid>
  )
}
