import React, {useEffect, useState} from 'react'
import {Grid, Tab} from '@material-ui/core'
import {useHistory, Link} from  'react-router-dom'
import {Table, TableBody, TableCell, TableRow, TableHead} from '@mui/material'
import useStyle from '../Resources/css/Contentfile'
import moment from 'moment'
import {GetWeeklyAnalysis} from '../../databases/Read'
import {DeleteWeeklyAnalysis} from '../../databases/Delete'
import WeeklyContent from '../Components/WeeklyContent/Weekly'
import MonthlyContent from '../Components/MonthlyContent/Monthly'
import {FaPen, FaTrashAlt, FaEyeSlash} from 'react-icons/fa'


export default function ContentMaster() {
  const history = useHistory()
  const classes = useStyle()
  const [data,setData] = useState([])
  const [loading, setLoad] = useState(true)
  const [activeTab,setActiveTab] = useState('wcon')

  const column = [
      {field:'title',headerName:'Content Title',width:'30%'},
      {field:'date',headerName:'Release Date',width:'30%'},
      {field:'stock',headerName:'Updated Stock',width:'10%'},
      {field:'update',headerName:'Update',width:'10%'},
      {field:'delete',headerName:'Delete',width:'10%'},
      {field:'hide',headerName:'Hide',width:'10%'},
  ]

  function deleteDoc(id){
    DeleteWeeklyAnalysis(id).then(()=>{
        history.go(0)
    })
  }

  useEffect(()=>{
      setLoad(true)
      GetWeeklyAnalysis().then((resolve)=>{
        setData([...resolve])
        setLoad(false)
        console.log('weekly analysis -------------------')
        console.log(resolve)
      })
  },[])

  return (
    <Grid container>
        {data.length<1?(
            <>
                Loading~
            </>
        ):(
            <Grid container>
                <Table aria-label='Content List'>
                    <TableHead>
                        <TableRow>
                            <TableCell className={classes.head}>Title</TableCell>
                            <TableCell className={classes.head} align='center'>Date</TableCell>
                            <TableCell className={classes.head} align='center'>Stock Updated</TableCell>
                            <TableCell className={classes.head} align='center'>Update</TableCell>
                            <TableCell className={classes.head} align='center'>Delete</TableCell>
                            {/* <TableCell align='center'>Hide</TableCell> */}
                        </TableRow>
                    </TableHead>
                        <TableBody>
                            {data.map((datum, index)=>{
                                console.log(datum.time)
                                return(
                                    <TableRow key={index}>
                                        <TableCell className={classes.title}>{datum.title}</TableCell>
                                        <TableCell className={classes.w10} align='right'>{typeof(datum.time)==='string' ? datum.time:moment(Date(datum.time.seconds*1000)).format('D MMMM YYYY')}</TableCell>
                                        <TableCell className={classes.w10} align='center'>{datum.stock.length}</TableCell>
                                        <TableCell className={classes.w5} align='center'><Link to={{pathname:`/admin/ContentMaster/${datum.id}`}}><FaPen className={classes.icon}/></Link></TableCell>
                                        <TableCell className={classes.w5} align='center'><FaTrashAlt onClick={()=>{deleteDoc(datum.id)}} className={classes.icon}/></TableCell>
                                        {/* <TableCell align='center'><FaEyeSlash/></TableCell> */}
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                </Table>
            </Grid>
        )}
    </Grid>
  )
}
