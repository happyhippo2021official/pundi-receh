import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom'
import {Table, TableBody, TableCell, TableRow, TableHead, RadioGroup,FormControlLabel, Radio,FormControl, Grid, TextField} from '@mui/material'
import {addImage, addAdmin} from '../../databases/Add'
import {GetAdminLevel, GetAllAdmin} from '../../databases/Read'
import {deleteAdmin} from '../../databases/Delete'
import {ActivateAdmin} from '../../databases/Update'
import useStyle from '../Resources/css/Admin'
import {FaWindowClose, FaEyeSlash, FaEye,FaTrashAlt, FaPlay, FaPause} from 'react-icons/fa'

export default function AdminMaster() {
  const classes = useStyle()
  const history = useHistory()
  const [image, setImage] = useState(null)
  const [pImage, setPImage] = useState(null)
  const [title, setTitle] = useState('Analyst')
  const [username, setUsername] = useState('')
  const [personname, setPersonname] = useState({givenname:'',surname:''})
  const [level, setLevel] = useState(10)
  const [adminLevel, setAdminLevel] = useState([])
  const [pass, setPass] = useState('')
  const [see, setSee] = useState(false)
  const [admins, setAdmins] = useState([])

  const buttonClicked = async ()=>{
    let url = ''
    if(image !== null) {
      url = await addImage(image,'adminPict')
    }
    // console.log(url)
    const newObj = {
      username:username,
      pict:image?url:null,
      personname:personname,
      level:level,
      password:pass,
      position:title,
      active:true,
    }

    console.log(newObj)
    const result = await addAdmin(newObj)
    refresh()
  }

  async function refresh(){
    const refresh = await GetAllAdmin()
    setAdmins([...refresh])
  }

  function selectImage(event){
    if(event.target.files.length !== 0){
      setImage(event.target.files[0])
      // URL.revokeObjectURL()
      setPImage(URL.createObjectURL(event.target.files[0]))
    }
  }

  function selectRadio(e){
    const newLevel = e.target.value
    setLevel(newLevel)
    const newTitle = adminLevel.find(ele=>ele.value===newLevel)
    setTitle(e.target.name)
  }

  function resetImage(){
    setImage(null)
    setPImage(null)
  }

  async function changeActive(obj){
    const newObj = {...obj,active:!obj.active}
    const id = newObj.id
    delete newObj.id
    const data = await ActivateAdmin(id, newObj)
    refresh()
  }

  async function deleteData(id){
    const data = await deleteAdmin(id)
    refresh()
  }

  const handleClick = e => {
      const { name, value } = e.target;
      setPersonname(prevState => ({
          ...prevState,
          [name]: value
      }));
  }


  useEffect(async ()=>{
   const newList = await GetAdminLevel()
   setAdminLevel([...newList])
   refresh()
  },[])

  return (
    <Grid container>
      <Grid xs={11}>
        <Grid container alignItems='stretch' className={`${classes.section} ${classes.borderBot}`}>
          <Grid item md={8} xs={12}>
            <Grid container>
              Admin Master
            </Grid>
            <Grid container className={classes.add}>
              <Grid item xs={3}>
                <Grid container className={classes.height100} alignItems='center'>
                  Username
                </Grid>
              </Grid>
              <Grid item xs={9}>
                <Grid container className={classes.textbox}>
                  <TextField fullWidth value={username} onChange={(e)=>{setUsername(e.target.value)}}/>
                </Grid>
              </Grid>
            </Grid>
            <Grid container className={classes.add}>
              <Grid item xs={3}>
                <Grid container className={classes.height100} alignItems='center'>
                  Given Name
                </Grid>
              </Grid>
              <Grid item xs={3}>
                <Grid container className={classes.textbox}>
                  <TextField fullWidth value={personname.givenname} name="givenname" onChange={handleClick}/>
                </Grid>
              </Grid>
              <Grid item xs={3}>
                <Grid container className={classes.height100} alignItems='center'>
                  Family Name
                </Grid>
              </Grid>
              <Grid item xs={3}>
                <Grid container className={classes.textbox}>
                  <TextField fullWidth value={personname.surname} name="surname" onChange={handleClick}/>
                </Grid>
              </Grid>
            </Grid>
            <Grid container className={classes.add}>
              <Grid item xs={3}>
                <Grid container className={classes.height100} alignItems='center'>
                  Password
                </Grid>
              </Grid>
              <Grid item xs={8}>
                <Grid container className={classes.textbox}>
                  <TextField fullWidth type={see?'text':'password'} value={pass} onChange={(e)=>{setPass(e.target.value)}}/>
                </Grid>
              </Grid>
              <Grid item xs={1}>
                <Grid container justifyContent='center' className={classes.height100} alignItems='center'>
                  {see?(
                    <FaEye onClick={()=>{setSee(false)}} className={classes.icon}/>
                  ):(
                    <FaEyeSlash onClick={()=>{setSee(true)}} className={classes.icon}/>
                  )}
                </Grid>
              </Grid>
            </Grid>
            <Grid container className={classes.add}>
              <Grid item xs={3}>
                <Grid container>
                  Pict
                </Grid>
              </Grid>
              <Grid item xs={8}>
                <Grid container className={classes.textbox}>
                  <input type='file' onChange={(event)=>{selectImage(event)}}></input>
                </Grid>
              </Grid>
              <Grid item xs={1}>
                <Grid container justifyContent='center' className={classes.height100} alignItems='center'>
                  <FaWindowClose className={classes.delIcon} onClick={()=>{resetImage()}}/>
                </Grid>
              </Grid>
            </Grid>
            <Grid container className={classes.add}>
              <Grid item xs={3}>
                <Grid container className={classes.height100} alignItems='center'>
                  Admin Level
                </Grid>
              </Grid>
              <Grid item xs={9}>
                <Grid container className={classes.textbox}>
                    <FormControl>
                      <RadioGroup
                        row
                        aria-labelledby="demo-row-radio-buttons-group-label"
                        name="row-radio-buttons-group"
                        defaultValue={level}
                        onChange={(event)=>{selectRadio(event)}}
                      >
                        {adminLevel.map((alevel)=>{
                          return(
                            <FormControlLabel value={alevel.value} name={alevel.title} control={<Radio />} label={alevel.title} />
                          )
                        })}
                      </RadioGroup>
                    </FormControl>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item md={4} xs={12}>
            <Grid container justifyContent='center'>
              {image?(
                <img src={pImage} className={classes.preview}/>
              ):null}
            </Grid>
            <Grid container justifyContent='center' className={classes.add}>
              <Grid item xs='auto'>
                <Grid container justifyContent='center' className={classes.addButton} onClick={()=>{buttonClicked()}}>
                  Add Admin
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          
          
        </Grid>
        <Grid container className={classes.section}>
          List
          <Grid container className={classes.list}>
            <Table aria-label='Content List'>
              <TableHead>
                <TableRow>
                  <TableCell className={classes.head}>Username</TableCell>
                  <TableCell className={classes.head} align='center'>Title</TableCell>
                  <TableCell className={classes.head} align='center'>Active</TableCell>
                  <TableCell className={classes.head} align='center'>Delete</TableCell>
                  {/* <TableCell align='center'>Hide</TableCell> */}
                </TableRow>
              </TableHead>
              <TableBody>
                {admins.map((datum, index)=>{
                  return(
                    <TableRow key={index}>
                      <TableCell className={classes.comicTitle}>{datum.username}</TableCell>
                      <TableCell className={classes.comicPict} align='center'>{datum.position}</TableCell>
                      <TableCell className={classes.w5} align='center'>
                        {datum.level>0?(datum.active?(
                          <FaPlay onClick={()=>{changeActive(datum)}} className={classes.activeIcon}/>
                        ):(
                          <FaPause onClick={()=>{changeActive(datum)}} className={classes.activeIcon}/>
                        )):null}
                      </TableCell>
                      <TableCell className={classes.w5} align='center'>
                        <FaTrashAlt onClick={()=>{deleteData(datum.id)}} className={classes.activeIcon}/>
                      </TableCell>
                    </TableRow>
                  )
                })}
              </TableBody>
            </Table>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}
