import React, {useState} from 'react'
import {Grid, Tab} from '@material-ui/core'
import {useParams} from 'react-router-dom'
import useStyle from '../Resources/css/Content'
import WeeklyContent from '../Components/WeeklyContent/Weekly'
import MonthlyContent from '../Components/MonthlyContent/Monthly'

export default function ContentMaster(props) {
  const {id} = useParams()
  const classes = useStyle()
  const tabs = [
    {code:'wcon', text:'Weekly Content'},
    {code:'mcon', text:'Monthly Content'}
  ]
  const [activeTab,setActiveTab] = useState('wcon')
  return (
    <Grid container>
        <Grid item xs={12}>
          <Grid container justifyContent='flex-start' alignItems='center' className={classes.tabContainer}>
              {tabs.map((tab)=>{
                return(
                  <Tab key={tab.code} label={tab.text} onClick={()=>{setActiveTab(tab.code)}} className={`${classes.tabItem} ${activeTab===tab.code?classes.active:null}`}/>
                )
              })}
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Grid container justifyContent='center'>
            {
              (activeTab==='wcon')
              ?<WeeklyContent adminData={props.adminData} id={id?id:null}/>
              :<MonthlyContent adminData={props.adminData}/>
            }
          </Grid>
        </Grid>
    </Grid>
  )
}
