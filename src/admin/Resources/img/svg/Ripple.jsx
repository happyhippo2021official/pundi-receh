import * as React from "react"

const SvgComponent = (props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    style={{
      margin: "auto",
      background: "0 0",
      display: "block",
      shapeRendering: "auto",
    }}
    width={30}
    height={30}
    viewBox="0 0 100 100"
    preserveAspectRatio="xMidYMid"
    {...props}
  >
    <circle cx={50} cy={50} r={0} fill="none" stroke="#e90c59" strokeWidth={10}>
      <animate
        attributeName="r"
        repeatCount="indefinite"
        dur="0.6666666666666666s"
        values="0;40"
        keyTimes="0;1"
        keySplines="0 0.2 0.8 1"
        calcMode="spline"
        begin="0s"
      />
      <animate
        attributeName="opacity"
        repeatCount="indefinite"
        dur="0.6666666666666666s"
        values="1;0"
        keyTimes="0;1"
        keySplines="0.2 0 0.8 1"
        calcMode="spline"
        begin="0s"
      />
    </circle>
    <circle cx={50} cy={50} r={0} fill="none" stroke="#46dff0" strokeWidth={10}>
      <animate
        attributeName="r"
        repeatCount="indefinite"
        dur="0.6666666666666666s"
        values="0;40"
        keyTimes="0;1"
        keySplines="0 0.2 0.8 1"
        calcMode="spline"
        begin="-0.3333333333333333s"
      />
      <animate
        attributeName="opacity"
        repeatCount="indefinite"
        dur="0.6666666666666666s"
        values="1;0"
        keyTimes="0;1"
        keySplines="0.2 0 0.8 1"
        calcMode="spline"
        begin="-0.3333333333333333s"
      />
    </circle>
  </svg>
)

export default SvgComponent
