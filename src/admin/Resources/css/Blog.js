import { makeStyles } from '@material-ui/core/styles'
import color from '../../../resources/css/Color'


const useStyles = makeStyles((theme)=>{
    return{
        contentHeight:{
            height:'90vh'
        },
        contentSection:{
            paddingTop:'2vmin',
            paddingLeft:'1vmin',
            paddingRight:'1vmin',
        },
        tabContainer:{
            borderBottom:'1px solid',
            borderColor:color.black,
        },
        tabItem:{
            borderStyle:'solid',
            borderWidth:'1px',
            borderColor:color.black,
            borderRadius:'5px 5px 0 0',
            backgroundColor:color.black,
            color:color.white,
            margin:'0 0 0 5px',
            transition:'all .2s ease-in-out',
            '&:hover':{
                backgroundColor:color.darkGrey,
            }
        },
        active:{
            backgroundColor:color.darkGrey,
            color:color.goldyellow
        },
        optionButton:{
            width:'100%',
            marginTop:'1vmin',
            height:'5vmin',
            minHeight:'25px',
            color:color.white,
            backgroundColor:color.darkGrey,
            fontSize:'2vmin',
            borderRadius:'5px',
            textAlign:'center',
            cursor:'pointer',
            transition: 'all 0.2s ease-in-out',
            '&:active':{
                background: color.lightGrey,
                transform: 'translateY(2px)',
                color:color.darkGrey
            }
        },
        saveButton:{
            width:'100%',
            marginTop:'1vmin',
            height:'5vmin',
            minHeight:'25px',
            color:color.white,
            backgroundColor:color.brightblue,
            fontSize:'2vmin',
            borderRadius:'5px',
            textAlign:'center',
            cursor:'pointer',
            transition: 'all 0.2s ease-in-out',
            '&:active':{
                transform: 'translateY(2px)',
                color:color.darkGrey
            }
        },
    }
})

export default useStyles;