import { makeStyles } from '@material-ui/core/styles'
import color from '../../../resources/css/Color'


const useStyles = makeStyles((theme)=>{
    return{
        title:{
            textOverflow:'ellips',
            width:'70%'
        },
        w10:{
            width:'10%'
        },
        w15:{
            width:'5%'
        },
        head:{
            fontWeight:'700 !important',
        },
        icon:{
            transition:'all .25s ease-in-out',
            color:color.black,
            cursor:'pointer',
            '&:hover':{
                transition:'all .25s ease-in-out',
                color:color.hotRed
            }
        }
    }
})

export default useStyles;