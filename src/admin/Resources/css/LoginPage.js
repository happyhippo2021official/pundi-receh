import { makeStyles } from '@material-ui/core/styles'
import color from '../../../resources/css/Color'
import BG from '../img/LoginBg.jpg'


const useStyles = makeStyles((theme)=>{
    return {
        outerContainer:{
            height:'100vh',
            width:'100vw',
            backgroundImage:`url(${BG})`
        },
        middleContainer:{
            backgroundColor:'rgba(255,255,255,0.5)',
            borderRadius: '5px',
        },
        divider:{
            width:'100%'
        },
        warningMessage: {
            color:color.hotRed
        },
        section:{
            padding:'2.5vh'
        }
    }
})

export default useStyles;