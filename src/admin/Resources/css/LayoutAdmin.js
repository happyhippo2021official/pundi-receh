import { makeStyles } from '@material-ui/core/styles'
import color from '../../../resources/css/Color'


const useStyles = makeStyles((theme)=>{
    return {
        //Container
        contentHeight:{
            height:'90vh'
        },
        //Header
        headerContainer:{
            minHeight:'10vh',
            backgroundColor:color.darkGrey,
            color:color.white,
            borderColor:color.goldyellow,
            borderWidth:'0 0 2px 0',
            borderStyle:'solid',
            cursor:'default'
        },
        headerSection:{
            padding:'0 3vw 0 3vw'
        },
        headerUserMenuSection:{
            margin:'0 5px 0 5px'
        },
        userPictContainer:{
            pointerEvents:'none'
        },
        userPict:{
            borderRadius:'50%',
            height:'5vh',
            width:'5vh'
        },
        exitLogo:{
            color:color.white
        },
        userName:{
            textTransform:'capitalize',
            fontSize:'1.125rem'
        },
        userPosition:{
            fontSize:'.75rem'
        },

        //Menu
        menuContainer:{
            backgroundColor:color.darkGrey,
            color:color.white,
            borderColor:color.goldyellow,
            borderWidth:'0 2px 0 0',
            borderStyle:'solid',
            height:'100%',
            minHeight:'100vh',
            '& a,a:visited':{
                color:color.white
            }
        },
        menuInsideContainer:{
            height:'85vh'
        },
        menuItem:{
            padding:'10px 0 10px 0',
            cursor:'pointer',
            transition:'all .5s ease-out',
            '&:hover':{
                backgroundColor:'rgba(100,100,100,.8)',
            }
        }
    }
})

export default useStyles;