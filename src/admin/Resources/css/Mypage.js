import { makeStyles } from '@material-ui/core/styles'
import color from '../../../resources/css/Color'


const useStyles = makeStyles((theme)=>{
    return{
        contentHeight:{
            margin:'5vmin 0'
        },
        title:{
            padding:'2vmin 0',
            transition: 'all 0.5s ease-in-out',
        },
        textboxTitle:{
            padding:'2vmin',
            transition: 'all 0.5s ease-in-out',
        },
        panel:{
            width:'100%'
        },
        saveBox:{
            padding:'1vmin'
        },
        saveButton:{
            //margin:'0 1vmin',
            width:'100%',
            //marginTop:'1vmin',
            height:'5vmin',
            minHeight:'20px',
            color:color.white,
            backgroundColor:color.brightblue,
            fontSize:'1.5vmin',
            borderRadius:'5px',
            textAlign:'center',
            cursor:'pointer',
            transition: 'all 0.2s ease-in-out',
            '&:active':{
                transform: 'translateY(2px)',
                color:color.darkGrey
            },
            '&:hover':{
                backgroundColor:color.brightblue,
            }
        },
    }
})

export default useStyles;