import { makeStyles } from '@material-ui/core/styles'
import color from '../../../resources/css/Color'


const useStyles = makeStyles((theme)=>{
    return{
        contentHeight:{
            height:'90vh'
        },
        tabContainer:{
            borderBottom:'1px solid',
            borderColor:color.black,
        },
        tabItem:{
            borderStyle:'solid',
            borderWidth:'1px',
            borderColor:color.black,
            borderRadius:'5px 5px 0 0',
            backgroundColor:color.black,
            color:color.white,
            margin:'0 0 0 5px',
            transition:'all .2s ease-in-out',
            '&:hover':{
                backgroundColor:color.darkGrey,
            }
        },
        active:{
            backgroundColor:color.darkGrey,
            color:color.goldyellow
        }
    }
})

export default useStyles;