import { makeStyles } from '@material-ui/core/styles'
import color from '../../../resources/css/Color'


const useStyles = makeStyles((theme)=>{
    return{
        oneData:{
            height:'100%',
            padding: '1vh 1vw',
            margin: '1vh 0',
            borderRadius: '5px',
            backgroundColor:color.lightGreyRGBA06,
            borderColor:color.darkGrey,
            borderWidth:'1px',
            borderStyle:'solid',
            transition: 'all 0.2s ease-in-out',
            '&:hover':{
                borderColor:color.goldyellow,
                transition: 'all 0.2s ease-in-out',
            }
        },
        mainDatus:{
            margin:'10px 0',
        },
        singleKey:{
            height:'100%'
        },
        singleKeyValue:{
            margin:'10px 0',
            height:'100%',
        },
        deleteIcon:{
            margin: '1vh',
            transition: 'all 0.2s ease-in-out',
            '&:hover':{
                color: color.hotRed,
                transition: 'all 0.2s ease-in-out',
            }
        },
        selectboxContainer:{
           width:'100%',
           height:'70%',
           backgroundColor:color.brightGreen,
           position:'relative'
        }
    }
})

export default useStyles;