import { makeStyles } from '@material-ui/core/styles'
import color from '../../../resources/css/Color'


const useStyles = makeStyles((theme)=>{
    return{
        section:{
            margin:'5vmin',
            fontSize:'5vmin',

        },
        add:{
            fontSize:'1.5rem',
            padding:'2vmin'
        },
        list:{
            fontSize:'1.5rem',
            margin:'2vmin'
        },
        borderBot:{
            borderBottom:'1px solid',
            borderColor:color.black,
        },
        textbox:{
            height:'100%',
        },
        addButton:{
            margin: '20px',
            border: '1px solid black',
            fontSize: '1rem',
            borderRadius: '5px',
            background: color.grey,
            cursor: 'pointer',
            padding: '10px',
            transition: 'all 0.2s ease-in-out',
            boxShadow: '9px 9px #999',
            '&:hover':{
                background: color.lightGrey,
                borderColor: color.goldyellow,
            },
            '&:active':{
                background: color.darkGrey,
                transform: 'translateY(4px)',
                boxShadow: '3px 3px #666',
            }
        },
        preview:{
            height:'100%',
            maxHeight:'35vmin',
            aspectRatio: '1:1'
        },
        delButton:{
            margin: '1vh',
            transition: 'all 0.2s ease-in-out',
            '&:hover':{
                color: color.hotRed,
                transition: 'all 0.2s ease-in-out',
            }
        }
    }
})

export default useStyles;