import { makeStyles } from '@material-ui/core/styles'
import color from '../../../resources/css/Color'


const useStyles = makeStyles((theme)=>{
    return{
        button:{
            margin: '20px',
            border: '1px solid black',
            borderRadius: '5px',
            background: color.grey,
            cursor: 'pointer',
            padding: '15px',
            transition: 'all 0.2s ease-in-out',
            boxShadow: '0 9px #999',
            '&:hover':{
                background: color.lightGrey,
                borderColor: color.goldyellow,
            },
            '&:active':{
                background: color.darkGrey,
                transform: 'translateY(4px)',
                boxShadow: '0 5px #666',
            }

        },
        stockDivider:{
            borderTop: '1px solid',
            padding: '20px 0',
        },
        stockTitle:{
            textTransform: 'uppercase',
            fontSize: '2rem'
        },
        pimage:{
            height:'100%',
            maxHeight:'15vmin',
            aspectRatio: '1:1'
        },
        height100:{
            height:'100%'
        }
    }
})

export default useStyles;