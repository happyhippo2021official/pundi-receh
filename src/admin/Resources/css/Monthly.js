import { makeStyles } from '@material-ui/core/styles'
import color from '../../../resources/css/Color'


const useStyles = makeStyles((theme)=>{
    return{
        contentSection:{
            paddingTop:'2vmin',
            paddingLeft:'1vmin',
            paddingRight:'1vmin',
        },
        middleSection:{
            maxHeight: '70vh',
            overflowY:'auto'
        },
        headIndi:{
            fontSize:'4vmin',
            // margin:'1vmin',
            color:color.goldyellow,
        },
        optionButton:{
            width:'100%',
            marginTop:'1vmin',
            height:'5vmin',
            minHeight:'25px',
            color:color.white,
            backgroundColor:color.darkGrey,
            fontSize:'1.5vmin',
            borderRadius:'5px',
            textAlign:'center',
            cursor:'pointer',
            transition: 'all 0.2s ease-in-out',
            '&:active':{
                background: color.lightGrey,
                transform: 'translateY(2px)',
                color:color.darkGrey
            }
        },
        saveButton:{
            width:'100%',
            marginTop:'1vmin',
            height:'5vmin',
            minHeight:'25px',
            color:color.white,
            backgroundColor:color.brightblue,
            fontSize:'1.5vmin',
            borderRadius:'5px',
            textAlign:'center',
            cursor:'pointer',
            transition: 'all 0.2s ease-in-out',
            '&:active':{
                transform: 'translateY(2px)',
                color:color.darkGrey
            }
        },
        itemBorder:{
            borderTop:'solid 2px',
            borderColor:color.hotRed,
            paddingTop:'5px',
            marginBottom: '1vmin'
        },
        itemTitle:{
            fontSize:'3vmin',
            color:color.goldyellow
        },
        fullWidth:{
            width:'100%'
        },
        trashcan:{
            cursor: 'pointer',
            transition: 'all 0.2s ease-in-out',
            '&:hover':{
                color:color.hotRed,
                transition: 'all 0.2s ease-in-out',
            }
        },
        image:{
            maxWidth: '450px',
        }
    }
})

export default useStyles;