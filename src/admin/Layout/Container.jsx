import React, {useState} from 'react'
import useStyle from '../Resources/css/LayoutAdmin'
import {Grid} from '@material-ui/core'

import Header from './Header'
import Menu from './Menu'

export default function Container(props) {
    const [open, setOpen] = useState(true)
    const classes = useStyle()
    return (
        <Grid container>
            <Grid item xs={12}>
                <Header title={props.title} setAdminData={props.setAdminData} adminData={props.adminData}/>
            </Grid>
            <Grid item xs={2} >
                <Menu adminData={props.adminData}/>
            </Grid>
            <Grid item xs={10}>        
                <props.load adminData={props.adminData}/>
            </Grid>
        </Grid>
    );
}
