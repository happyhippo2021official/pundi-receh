import React from 'react';
import useStyle from '../Resources/css/LayoutAdmin'
import {useHistory, Link} from 'react-router-dom'
import {Grid, Typography, CardMedia, Tooltip} from '@material-ui/core'
import { IconButton } from '@mui/material';
import Cookies from 'universal-cookie'

import Menu from './Menu'
import {ExitToApp} from '@material-ui/icons'
import defpict from '../Resources/img/admin_default.png'

export default function Header(props) {
    const history = useHistory()
    const classes = useStyle()
    const cookies = new Cookies()
    function Logout(){
        cookies.remove('LoggedinAdmin', { path: '/' })
        props.setAdminData(false)
        history.push('/admin')
    }
    function imageClick(){
        history.push('/admin/MyPage')
    }
    return (
        <Grid container justifyContent='space-between' alignItems='center' className={classes.headerContainer}>
            <Grid item xs={5}>
                <Grid container justifyContent='flex-start' alignItems='center' className={classes.headerSection}>
                    <Typography variant='h4'>{props.title}</Typography>
                </Grid>
            </Grid>
            {/* <Grid item xs={3}>
                <Typography variant='h4'>{props.title}</Typography>
            </Grid> */}
            <Grid item xs={4}>
                <Grid container justifyContent='flex-end' alignItems='center' className={classes.headerSection}>
                    <Grid item>
                        <Grid container justifyContent='space-between' alignItems='center' className={classes.headerUserMenuSection}>
                            <Grid item xs={10}>
                                <Grid container justifyContent='flex-end' alignItems='center' >
                                    <Typography align='right' variant='subtitle1' className={classes.userName}>
                                        {props.adminData.username}
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item xs={10}>
                                <Grid container justifyContent='flex-end' alignItems='center' >
                                    <Typography align='right' variant='subtitle1' className={classes.userPosition}>
                                        {props.adminData.position}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Grid container justifyContent='center' alignItems='center'>
                            <Tooltip title={props.adminData.username}>
                                <IconButton onClick={()=>{imageClick()}}>
                                        {props.adminData.pict?(
                                            <img src={props.adminData.pict} className={classes.userPict}/>
                                        ):(
                                            <img src={defpict} className={classes.userPict}/>
                                        )}
                                </IconButton>
                            </Tooltip>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Grid container justifyContent='center' alignItems='center'>
                            <Tooltip title='Logout'>
                                <IconButton onClick={()=>{Logout()}}> 
                                    <ExitToApp className={classes.exitLogo}/>
                                </IconButton>
                            </Tooltip>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}
