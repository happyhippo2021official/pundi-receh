import { Grid, Typography } from '@material-ui/core';
import {Link} from 'react-router-dom'
import React from 'react';
import {GetMenu} from '../Components/Role'
import useStyle from '../Resources/css/LayoutAdmin'

export default function Menu(props) {
    const classes = useStyle()
    const menuList = [
        {name:'Home',link:'/admin/Home'},
        {name:'Admin Master',link:'/admin/AdminMaster'},
        {name:'User Master',link:'/admin/UserMaster'},
        {name:'Paypal',link:'/admin/Accounting'},
        {name:'Add Content',link:'/admin/ContentMaster'},
        {name:'Content List',link:'/admin/ContentList'},
        {name:'Resource Master',link:'/admin/ResourceMaster'},
        {name:'My Page',link:'/admin/MyPage'},
    ]
    const menuList2 = GetMenu(props.adminData.position)
    return (
        <Grid container justifyContent='flex-start' alignContent='flex-start' alignItems='flex-start' className={`${classes.contentHeight} ${classes.menuContainer}`}>
            {menuList2.map((item)=>{
                return(
                    <Grid item xs={12}>
                        <Link to={item.link}>
                            <Grid container justifyContent='center' alignItems='flex-start' className={classes.menuItem}>
                                <Grid item xs={10}>
                                    <Grid container justifyContent='flex-start' alignItems='flex-start'>
                                        <Typography variant='subtitle2'>{item.name}</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Link>
                    </Grid>
                )
            })}
        </Grid>
    );
}
