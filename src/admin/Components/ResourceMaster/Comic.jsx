import React, {useEffect, useState} from 'react'
import {Grid, TextField} from '@material-ui/core'
import {useHistory} from 'react-router-dom'
import {Table, TableBody, TableCell, TableRow, TableHead} from '@mui/material'
import {addImage, addComic} from '../../../databases/Add'
import {GetAllComics} from '../../../databases/Read'
import {deleteComic} from '../../../databases/Delete'
import useStyle from '../../Resources/css/Comic'
import {FaTrashAlt} from 'react-icons/fa'
import NoImg from '../../../resources/images/noImage.png'

export default function Comic() {
  const classes = useStyle()
  const history = useHistory()
  const [image, setImage] = useState(null)
  const [pImage, setPImage] = useState(null)
  const [title, setTitle] = useState('')
  const [comics, setComics] = useState([])

  const buttonClicked = async ()=>{
    if(image === null) alert('No image yet')
    const url = await addImage(image,'comic')
    // console.log(url)
    const newObj = {
      link:url,
      title:title
    }
    const result = await addComic(newObj)
    const refresh = await GetAllComics()
    setComics([...refresh])
  }
  
  const delComic = async (id)=>{
    const result = await deleteComic(id)
    refresh()
  }

  function SelectImage(event){
    setImage(event.target.files[0])
    // URL.revokeObjectURL()
    setPImage(URL.createObjectURL(event.target.files[0]))
  }
  async function refresh(){
    const result = await GetAllComics()
    setComics([...result])
  }

  useEffect(async ()=>{
    refresh()
  },[])
  useEffect(()=>{
    console.log(image)
  },[image])
  return (
    <Grid container>
      <Grid xs={11}>
        <Grid container alignItems='stretch' className={`${classes.section} ${classes.borderBot}`}>
          <Grid item md={8} xs={12}>
            <Grid container>
              Add
            </Grid>
            <Grid container className={classes.add}>
              <Grid item xs={3}>
                <Grid container>
                  Title
                </Grid>
              </Grid>
              <Grid item xs={9}>
                <Grid container className={classes.textbox}>
                  <TextField fullWidth value={title} onChange={(e)=>{setTitle(e.target.value)}}/>
                </Grid>
              </Grid>
            </Grid>
            <Grid container className={classes.add}>
              <Grid item xs={3}>
                <Grid container>
                  Pict
                </Grid>
              </Grid>
              <Grid item xs={9}>
                <Grid container className={classes.textbox}>
                <input type='file' onChange={(event)=>{SelectImage(event)}}></input>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item md={4} xs={12}>
            <Grid container justifyContent='center'>
              {image?(
                <img src={pImage} className={classes.preview}/>
              ):(
                <img src={NoImg} className={classes.preview}/>
              )}
            </Grid>
            <Grid container justifyContent='center' className={classes.add}>
              <Grid item xs='auto'>
                <Grid container justifyContent='center' className={classes.addButton} onClick={()=>{buttonClicked()}}>
                  Add Comic
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          
          
        </Grid>
        <Grid container className={classes.section}>
          List
          <Grid container className={classes.list}>
            <Table aria-label='Content List'>
              <TableHead>
                <TableRow>
                  <TableCell className={classes.head}>Title</TableCell>
                  <TableCell className={classes.head} align='center'>Pict</TableCell>
                  <TableCell className={classes.head} align='center'>Delete</TableCell>
                  {/* <TableCell align='center'>Hide</TableCell> */}
                </TableRow>
              </TableHead>
              <TableBody>
                {comics.map((datum, index)=>{
                  return(
                    <TableRow key={index}>
                      <TableCell className={classes.comicTitle}>{datum.title}</TableCell>
                      <TableCell className={classes.comicPict} align='center'><img src={datum.link} className={classes.preview}/></TableCell>
                      <TableCell className={classes.w5} align='center'><FaTrashAlt onClick={()=>{delComic(datum.id)}} className={classes.delButton}/></TableCell>
                      {/* <TableCell align='center'><FaEyeSlash/></TableCell> */}
                    </TableRow>
                  )
                })}
              </TableBody>
            </Table>
          </Grid>
        </Grid>
      </Grid>
        {/* <input type='file' onChange={(event)=>{setImage(event.target.files[0])}}></input>
        <button onClick={uploadImage}> Upload Image</button> */}
    </Grid>
  )
}
