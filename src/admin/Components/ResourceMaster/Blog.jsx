import React, {useState} from 'react'
import {Grid, TextField} from '@material-ui/core'
import {useHistory} from 'react-router-dom'
import useStyle from '../../Resources/css/Monthly'
import moment from 'moment'
import Input from './../MonthlyContent/InputComponent'
import {addBlog, addImage} from '../../../databases/Add'
import {UpdateBlog} from '../../../databases/Update'
import cardStyle from '../../Resources/css/StockCards'
import LoadingIcon from '../../Resources/img/svg/Ripple'

export default function Blog(props) {
  console.log(props)
  const classes = useStyle()
  const history = useHistory()
  const cardclass = cardStyle()
  const [SaveLoad, setSaveLoad] = useState(false)
  const itemType = [
    {type:'bold1', label:'Title 1'},
    {type:'bold2', label:'Title 2'},
    {type:'text',label:'Paragraph'},
    {type:'pict', label:'Insert Picture'},
  ]
  const [join, setJoin] = useState({
    title:props.selectData?props.selectData.title:'',
    by: props.selectData?props.selectData.by:props.adminData.name,
    time: props.selectData?props.selectData.time:moment().format('D MMMM YYYY'),
    timestamp: props.selectData?props.selectData.timestamp:null
  })
  const [listItem, setListItem] = useState(props.selectData?props.selectData.items:[])

  function addItem(type){
    const newArr = [...listItem,{itemType:type,value:(type=='pict')?null:{en:null,id:null}}]
    setListItem(newArr)
  }

  async function UploadPict(Arr1){
    const newObject = await Promise.all(Arr1.map(async obj =>{
      console.log(obj)
      if (obj.itemType ==='pict'){
        if (obj.value.name){
          console.log(obj.value)
          const newurl = await addImage(obj.value,'resource/blogs')
          obj.value = newurl
        }
      }
        return obj
    }))
  }

  function changeValue(index,newValue,lg=null){
    // console.log(`change value ${index} - ${lg} : ${newValue}`)
    let newArr = [...listItem]
    if (lg){
      newArr[index].value[lg] = newValue
    }else{
      newArr[index].value = newValue
    }
    setListItem(newArr)
  }

  function deleteItem(index){
    let newArr = [...listItem]
    newArr.splice(index,1)
    setListItem(newArr)
  }

  function updateJoin(props,e){
    const newValue = {...join}
    newValue[props] = e.target.value 
    setJoin(newValue)
  }

  async function Save(){
    setSaveLoad(true)
    await UploadPict(listItem)
    let newData = {
      ...join,
      items:[...listItem]
    }
    setJoin(newData)
    console.log(newData)

    if (props.selectData){
      props.setSelectData(null)
      UpdateBlog(props.selectData.id,newData).then((response)=>{
        history.go(0)
        console.log(response)
      })

    }else{
      addBlog(newData).then((response)=>{
        history.go(0)
        console.log(response)
      })
    }
    
  }

  return (
    <Grid container>
      <Grid item xs={12}>
        <Grid container justifyContent='space-between'>
            <Grid item md={1} xs={3}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                    Title:
                </Grid>
            </Grid>
            <Grid item md={7} xs={9}>
                <Grid container >
                    <TextField multiline maxRow={3} fullWidth value={join.title} onChange={(event)=>updateJoin('title',event)}/>
                </Grid>
            </Grid>
            <Grid item md={2} xs={6}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                  {join.time}
                </Grid>
            </Grid>
            <Grid item md={2} xs={6}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                    By {join.by}
                </Grid>
            </Grid>
        </Grid>
        <Grid container direction='row'>
          <Grid item xs>
            <Grid container justifyContent='center'className={classes.contentSection}>

            </Grid>
            {listItem.map((item,index)=>{
              return(
                <Input 
                  key={index}
                  type={item.itemType} 
                  value={item.value} 
                  index={index}
                  update={changeValue}
                  delete={deleteItem}
                />
              )
            })}
          </Grid>
          <Grid item md={1} xs={3} className={classes.contentSection}>
            {itemType.map((item)=>{
              return(
                <Grid container justifyContent='center' alignContent='center' onClick={()=>{addItem(item.type)}} className={classes.optionButton}> 
                  {item.label} 
                </Grid>
              )
            })}
            <Grid container justifyContent='center' alignContent='center' onClick={()=>{Save()}} className={classes.saveButton}>
              {SaveLoad?<LoadingIcon />:'Save'}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}
