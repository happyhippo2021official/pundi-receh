import React, {useState, useEffect} from 'react'
import {Grid, TextField} from '@material-ui/core'
import {useHistory} from 'react-router-dom'
import {Table, TableBody, TableCell, TableRow, TableHead} from '@mui/material'
import {GetAllBlogs} from '../../../databases/Read'
import {deleteBlog} from '../../../databases/Delete'
import useStyle from '../../Resources/css/Podcast'
import moment from 'moment'
import {FaTrashAlt,FaEdit} from 'react-icons/fa'

export default function BlogMaster(props) {
  const classes = useStyle()
  const [blogs, setBlogs] = useState([])
  const [title, setTitle] = useState('')
  const [link, setLink] = useState('')
  const history = useHistory()

  const refresh = async()=>{
    const result = await GetAllBlogs()
    setBlogs([...result])
  }

  const editButtonClicked = (datum)=>{
    console.log(datum)
    props.setActiveTab('blg')
    props.setSelectData(datum)
  }

  const delButtonClicked = async (id)=>{
    console.log(id)
    const result = await deleteBlog(id)
    refresh()
  }

  useEffect(async ()=>{
    const result = await GetAllBlogs()
    setBlogs([...result])
  },[])

  return (
    <Grid container>
      <Grid xs={11}>

        <Grid container className={classes.section}>
          List
          <Grid container className={classes.list}>
            <Table aria-label='Content List'>
              <TableHead>
                <TableRow>
                  <TableCell className={classes.head}>Title</TableCell>
                  <TableCell className={classes.head} align='center'>Date</TableCell>
                  <TableCell className={classes.head} align='center'>By</TableCell>
                  <TableCell className={classes.head} align='center'>Edit</TableCell>
                  <TableCell className={classes.head} align='center'>Delete</TableCell>
                  {/* <TableCell align='center'>Hide</TableCell> */}
                </TableRow>
              </TableHead>
              <TableBody>
                {blogs.map((datum, index)=>{
                  return(
                    <TableRow key={index}>
                      <TableCell className={classes.comicTitle}>{datum.title}</TableCell>
                      <TableCell className={classes.comicTitle}>{datum.time}</TableCell>
                      <TableCell className={classes.comicPict} align='center'>{datum.by}</TableCell>
                      <TableCell className={classes.w5} align='center'><FaEdit onClick={()=>{editButtonClicked(datum)}} className={classes.icon}/></TableCell>
                      <TableCell className={classes.w5} align='center'><FaTrashAlt onClick={()=>{delButtonClicked(datum.id)}} className={classes.icon}/></TableCell>
                      {/* <TableCell align='center'><FaEyeSlash/></TableCell> */}
                    </TableRow>
                  )
                })}
              </TableBody>
            </Table>
          </Grid>
        </Grid>
      </Grid>
      <Grid container >

      </Grid>
        {/* <input type='file' onChange={(event)=>{setImage(event.target.files[0])}}></input>
        <button onClick={uploadImage}> Upload Image</button> */}
    </Grid>
  )
}
