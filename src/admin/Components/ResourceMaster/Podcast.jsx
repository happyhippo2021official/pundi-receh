import React, {useState, useEffect} from 'react'
import {Grid, TextField} from '@material-ui/core'
import {useHistory} from 'react-router-dom'
import {Table, TableBody, TableCell, TableRow, TableHead} from '@mui/material'
import {addPods} from '../../../databases/Add'
import {GetAllPods} from '../../../databases/Read'
import {deletePods} from '../../../databases/Delete'
import useStyle from '../../Resources/css/Podcast'
import moment from 'moment'
import {FaTrashAlt} from 'react-icons/fa'

export default function Podcast() {
  const classes = useStyle()
  const [pods, setPods] = useState([])
  const [title, setTitle] = useState('')
  const [link, setLink] = useState('')
  const history = useHistory()

  const refresh = async()=>{
    const result = await GetAllPods()
    setPods([...result])
  }

  const addButtonClicked = async ()=>{
    const newObj = {
      title:title,
      link: link,
      date:  moment().format('D MMM YYYY'),
    }
    const result = await addPods(newObj)
    refresh()
    // history.go(0)
  }

  const delButtonClicked = async (id)=>{
    console.log(id)
    const result = await deletePods(id)
    refresh()
  }

  useEffect(async ()=>{
    const result = await GetAllPods()
    setPods([...result])
  },[])

  return (
    <Grid container>
      <Grid xs={11}>
        <Grid container alignItems='stretch' className={`${classes.section} ${classes.borderBot}`}>
          <Grid item md={8} xs={12}>
            <Grid container>
              Add
            </Grid>
            <Grid container className={classes.add}>
              <Grid item xs={3}>
                <Grid container>
                  Title
                </Grid>
              </Grid>
              <Grid item xs={9}>
                <Grid container className={classes.textbox}>
                  <TextField fullWidth value={title} onChange={(e)=>{setTitle(e.target.value)}}/>
                </Grid>
              </Grid>
            </Grid>
            <Grid container className={classes.add}>
              <Grid item xs={3}>
                <Grid container>
                  Link
                </Grid>
              </Grid>
              <Grid item xs={9}>
                <Grid container className={classes.textbox}>
                  <TextField fullWidth value={link} onChange={(e)=>{setLink(e.target.value)}}/>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item md={4} xs={12}>
            <Grid container justifyContent='center' className={classes.add}>
              <Grid item xs='auto'>
                <Grid container justifyContent='center' className={classes.addButton} onClick={()=>{addButtonClicked()}}>
                  Add Podcast
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          
          
        </Grid>
        <Grid container className={classes.section}>
          List
          <Grid container className={classes.list}>
            <Table aria-label='Content List'>
              <TableHead>
                <TableRow>
                  <TableCell className={classes.head}>Title</TableCell>
                  <TableCell className={classes.head} align='center'>Date</TableCell>
                  <TableCell className={classes.head} align='center'>Link</TableCell>
                  <TableCell className={classes.head} align='center'>Delete</TableCell>
                  {/* <TableCell align='center'>Hide</TableCell> */}
                </TableRow>
              </TableHead>
              <TableBody>
                {pods.map((datum, index)=>{
                  return(
                    <TableRow key={index}>
                      <TableCell className={classes.comicTitle}>{datum.title}</TableCell>
                      <TableCell className={classes.comicTitle}>{datum.date}</TableCell>
                      <TableCell className={classes.comicPict} align='center'><a href={datum.link} target='_blank' className={classes.link}>{datum.link}</a></TableCell>
                      <TableCell className={classes.w5} align='center'><FaTrashAlt onClick={()=>{delButtonClicked(datum.id)}} className={classes.icon}/></TableCell>
                      {/* <TableCell align='center'><FaEyeSlash/></TableCell> */}
                    </TableRow>
                  )
                })}
              </TableBody>
            </Table>
          </Grid>
        </Grid>
      </Grid>
      <Grid container >

      </Grid>
        {/* <input type='file' onChange={(event)=>{setImage(event.target.files[0])}}></input>
        <button onClick={uploadImage}> Upload Image</button> */}
    </Grid>
  )
}
