import React from 'react';

const GetMenu = (role) => {
    console.log(role)
    const allMenu = [{code:'hm',name:'Home',link:'/admin/Home'},
        {code:'am',name:'Admin Master',link:'/admin/AdminMaster'},
        {code:'um',name:'User Master',link:'/admin/UserMaster'},
        {code:'ac',name:'Add Content',link:'/admin/ContentMaster'},
        {code:'cl',name:'Content List',link:'/admin/ContentList'},
        {code:'rm',name:'Resource Master',link:'/admin/ResourceMaster'},
        {code:'mp',name:'My Page',link:'/admin/MyPage'},]

    const roles = [
        {name:'analyst',menus:['cl','ac','rm','mp','hm']},
        {name:'master',menus:['cl','ac','rm','mp','am','um','hm']},
        {name:'web maintainer',menus:['cl','ac','rm','mp','am','um','hm']},
        {name:'project manager',menus:['cl','ac','rm','mp','am','um','hm']}
    ]

    let result = []
    roles.map((datum)=>{
        if (String(role).toLowerCase() == datum.name){
            allMenu.map((menu)=>{
                if(datum.menus.includes(menu.code)){
                    result.push(menu)
                }
            })
        }
    })
    console.log(result)
    return result;
}

export {GetMenu};
