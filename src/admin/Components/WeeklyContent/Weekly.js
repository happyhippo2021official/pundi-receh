import React, {useEffect, useState} from 'react'
import {Grid} from '@material-ui/core'
import { TextField, Select, MenuItem, FormControl, FormControlLabel, RadioGroup, Radio } from '@mui/material'
import cardStyle from '../../Resources/css/StockCards'
import useStyle from '../../Resources/css/Weekly'
import moment from 'moment'
import {useHistory, useLocation, useParams} from 'react-router-dom'

import {addImage} from '../../../databases/Add'
import {addWeeklyAnalysis} from '../../../databases/Add'
import {GetOneAnalysis} from '../../../databases/Read'
import {UpdateAnalysis} from '../../../databases/Update'
import { Redirect } from 'react-router-dom'
import { FaTrashAlt as Trash} from 'react-icons/fa'


export default function Weekly(props) {
    // const passData = useLocation().data
    let datum = {}
    // if(passData){
    //     datum = {...passData.datum}
    //     if(typeof(datum.time)!=='string'){
    //         datum.time = moment(Date(datum.time.seconds*1000)).format('D MMMM YYYY')
    //         console.log(datum.time)
    //     }
    // }
    const [loading, setLoad] = useState(true)
    const history = useHistory()
    const cardclass = cardStyle()
    const weeklyclass = useStyle()
    const [joinData, setJoinData] = useState(
        {
            title:'',
            time: moment().format('D MMMM YYYY'),
            text:{
                en:'',
                id:''
            }
        }
    )
    const defaultstock = {
        code:'',
        name:'',
        price:'',
        status:'bearish',
        fundamental:{
            en:'',
            id:'',
        },
        technical:{
            en:'',
            id:''
        },
        imageUrl:''
    }
    const [stocks,setStocks] = useState([defaultstock])
    const [pImage,setPImage] = useState([null])

    useEffect(async ()=>{
        if(!props.id){
            setLoad(false)
        }else{
            GetOneAnalysis(props.id).then((response)=>{
                datum = response[0];
                console.log(datum)
                if(typeof(datum.time)!=='string'){
                    datum.time = moment(Date(datum.time.seconds*1000)).format('D MMMM YYYY')
                }
                setJoinData({
                    title: datum.title,
                    time: datum.time,
                    text:{
                        en: datum.text.en,
                        id: datum.text.id
                }})
                setStocks(datum.stock)
                let newPimage = []
                datum.stock.map((onestock)=>{
                    if (onestock.imageUrl===''){
                        newPimage.push(null)
                    }else{
                        newPimage.push(onestock.imageUrl)
                    }
                })
                setPImage(newPimage)
                setLoad(false)
            })
        }
    },[])

    useEffect(()=>{
        setLoad(false)
    },[pImage])

    function addstocks(){
        const stack = [...stocks,defaultstock]
        const pre = [pImage,null]
        setStocks(stack)
        setPImage(pre)
        // console.log(stack)
    }
    async function save(){
        const newStocks = await Promise.all(stocks.map(async stock =>{
            if (stock.imageUrl !==''){
                const newurl = await addImage(stock.imageUrl,'post/weekly')
                stock.imageUrl = newurl
            }
            return stock
        }))
        console.log(newStocks)

        // above is editing image url
        const wholeData = {...joinData,stock:newStocks}
        if(!props.id){
            addWeeklyAnalysis(wholeData).then((response)=>{
                history.go(0)
                console.log(response)
            })
        }else{
            UpdateAnalysis(props.id, wholeData).then((response)=>{
                console.log('finished update')
                history.go('/admin/ContentList')
            })
        }
        
    }


    function updatejoin(e){
        const newValue = {...joinData}
        newValue.title = e.target.value 
        setJoinData(newValue)
    }
    function updateJoinText(lang, e){
        const newValue = {...joinData}
        if(lang==='id'){
            newValue.text.id = e.target.value 
        }else{
            newValue.text.en = e.target.value 
        }
        setJoinData(newValue)
    }
    function updatestocks(index,variable,e){
        // console.log(`${index} - ${variable} - ${e.target.value}`)
        const newValue = [...stocks]
        newValue[index][variable] = e.target.value 

        setStocks(newValue)
        // const stack = [...stocks]
    }
    function updateImage(index,variable,e){
        const newValue = [...stocks]
        newValue[index][variable] = e.target.files[0]
        const newPImage = [...pImage]
        newPImage[index] = URL.createObjectURL(e.target.files[0])
        console.log(newPImage)
        setPImage(newPImage)
        setStocks(newValue)
    }
    function updateStockAnalysis(index, variable, lang, e){
        const newValue = [...stocks]
        newValue[index][variable][lang] = e.target.value 

        setStocks(newValue)

    }
    function DeleteStock(index){
        // setStocks(stocks.filter(stock => stock.code !== code))
        if(stocks.length>1 && index !==stocks.length){
            const newValue = [
                ...stocks.slice(0,index),
                ...stocks.slice(index+1,stocks.length)
            ]
            setStocks(newValue)
        }if(stocks.length===index){
            const newValue = [...stocks]
            newValue.pop()
            setStocks(newValue)
        }
    }

    useEffect(()=>{
        console.log(pImage)
        console.log(stocks)
    },[pImage])


    return (loading?(
        <>
        Loading...
        </>
    ):(
        <Grid item xs={11}>
            <Grid container justifyContent='space-between'>
                <Grid item md={1} xs={3}>
                    <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                        Title:
                    </Grid>
                </Grid>
                <Grid item md={8} xs={9}>
                    <Grid container className={cardclass.singleKeyValue}>
                        <TextField multiline maxRow={3} fullWidth value={joinData.title} onChange={(event)=>updatejoin(event)}/>
                    </Grid>
                </Grid>
                <Grid item md={3} xs={12}>
                    <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                        {joinData.time}
                    </Grid>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item xs={12}>
                    <Grid container justifyContent='center' alignItems='center'>
                        Text Data:
                    </Grid>
                </Grid>
                <Grid item md={1} xs={3}>
                    <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                        Id:
                    </Grid>
                </Grid>
                <Grid item md={5} xs={9}>
                    <Grid container className={cardclass.singleKeyValue}>
                        <TextField multiline maxRows={4} fullWidth value={joinData.text.id} onChange={(event)=>updateJoinText('id',event)}/>
                    </Grid>
                </Grid>
                <Grid item md={1} xs={3}>
                    <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                        En:
                    </Grid>
                </Grid>
                <Grid item md={5} xs={9}>
                    <Grid container className={cardclass.singleKeyValue}>
                        <TextField multiline maxRows={4} fullWidth value={joinData.text.en} onChange={(event)=>updateJoinText('en',event)}/>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container justifyContent='space-around' className={weeklyclass.stockDivider}>
                <Grid item xs={12}>
                    <Grid container justifyContent='center' className={weeklyclass.stockTitle}>
                        stocks
                    </Grid>
                </Grid>
                {stocks.map((stock,index)=>{
                    return(
                        <Grid item key={index} md={5} xs={11} className={cardclass.oneData}>
                            <Grid container justifyContent='flex-end'>
                                <Trash className={cardclass.deleteIcon} onClick={()=>{DeleteStock(index)}}/>
                            </Grid>
                            <Grid container className={cardclass.mainDatus}>
                                <Grid item md={2} xs={3}>
                                    <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                                        Code:
                                    </Grid>
                                </Grid>
                                <Grid item md={4} xs={9}>
                                    <Grid container className={cardclass.singleKeyValue}>
                                        <TextField value={stock.code} onChange={(event)=>updatestocks(index,'code',event)}/>
                                    </Grid>
                                </Grid>
                                <Grid item md={2} xs={3}>
                                    <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                                        Name:
                                    </Grid>
                                </Grid>
                                <Grid item md={4} xs={9}>
                                    <Grid container className={cardclass.singleKeyValue}>
                                        <TextField value={stock.name} onChange={(event)=>updatestocks(index,'name',event)}/>
                                    </Grid>
                                </Grid>
                                <Grid item md={2} xs={3}>
                                    <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                                        Price:
                                    </Grid>
                                </Grid>
                                <Grid item md={4} xs={9}>
                                    <Grid container className={cardclass.singleKeyValue}>
                                        <TextField value={stock.price} onChange={(event)=>updatestocks(index,'price',event)}/>
                                    </Grid>
                                </Grid>
                                <Grid item md={2} xs={3}>
                                    <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                                        Status:
                                    </Grid>
                                </Grid>
                                <Grid item md={4} xs={9}>
                                    <Grid container className={cardclass.singleKeyValue}>
                                        {/* <TextField value={stock.status} onChange={(event)=>updatestocks(index,'status',event)}/> */}
                                        <FormControl>
                                            <RadioGroup
                                                row
                                                aria-labelledby="demo-row-radio-buttons-group-label"
                                                name="row-radio-buttons-group"
                                                defaultValue={stock.status}
                                                onChange={(event)=>updatestocks(index,'status',event)}
                                            >
                                                <FormControlLabel value="bearish" control={<Radio />} label="Bearish" />
                                                <FormControlLabel value="bullish" control={<Radio />} label="Bullish" />
                                            </RadioGroup>
                                        </FormControl>
                                        {/* <Grid onChange={(event)=>updatestocks(index,'status',event)} className={cardclass.selectboxContainer}>
                                            
                                            <option value='Bearish'>Bearish</option>
                                            <option value='Bullish'>Bullish</option>
                                            <option value='Neutral'>Neutral</option>
                                        </Grid> */}
                                    </Grid>
                                </Grid>
                            </Grid>
                            {/* fundamental */}
                            <Grid container>
                                <Grid item xs={12}>
                                    <Grid container justifyContent='center' alignItems='center'>
                                        Fundamental Analysis
                                    </Grid>
                                </Grid>
                                <Grid item md={1} xs={3}>
                                    <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                                        Id:
                                    </Grid>
                                </Grid>
                                <Grid item md={5} xs={9}>
                                    <Grid container className={cardclass.singleKeyValue}>
                                        <TextField  multiline maxRows={4} fullWidth value={stock.fundamental.id} onChange={(event)=>updateStockAnalysis(index,'fundamental','id',event)}/>
                                    </Grid>
                                </Grid>
                                <Grid item md={1} xs={3}>
                                    <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                                        En:
                                    </Grid>
                                </Grid>
                                <Grid item md={5} xs={9}>
                                    <Grid container className={cardclass.singleKeyValue}>
                                        <TextField multiline maxRows={4}fullWidth value={stock.fundamental.en} onChange={(event)=>updateStockAnalysis(index,'fundamental','en',event)}/>
                                    </Grid>
                                </Grid>
                            </Grid>
                            {/* technical */}
                            <Grid container>
                                <Grid item xs={12}>
                                    <Grid container justifyContent='center' alignItems='center'>
                                        Technical Analysis
                                    </Grid>
                                </Grid>
                                <Grid item md={1} xs={3}>
                                    <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                                        Id:
                                    </Grid>
                                </Grid>
                                <Grid item md={5} xs={9}>
                                    <Grid container className={cardclass.singleKeyValue}>
                                        <TextField multiline maxRows={4} fullWidth value={stock.technical.id} onChange={(event)=>updateStockAnalysis(index,'technical','id',event)}/>
                                    </Grid>
                                </Grid>
                                <Grid item md={1} xs={3}>
                                    <Grid container justifyContent='center' alignItems='center'className={cardclass.singleKey} >
                                        En:
                                    </Grid>
                                </Grid>
                                <Grid item md={5} xs={9}>
                                    <Grid container className={cardclass.singleKeyValue}>
                                        <TextField multiline maxRows={4} fullWidth value={stock.technical.en} onChange={(event)=>updateStockAnalysis(index,'technical','en',event)}/>
                                    </Grid>
                                </Grid>
                                <Grid item xs={12}>
                                    <Grid container justifyContent='center' alignItems='center'>
                                        Image
                                    </Grid>
                                </Grid>
                                <Grid item xs={7}>
                                    <Grid container justifyContent='center' alignItems='center' className={weeklyclass.height100}>
                                        <input type='file' onChange={(event)=>{updateImage(index,'imageUrl',event)}}/>
                                    </Grid>
                                </Grid>
                                <Grid item xs={5}>
                                    <Grid container justifyContent='center' alignItems='center'>
                                    {pImage[index]!=null?(
                                        <img src={pImage[index]} className={weeklyclass.pimage}/>
                                    ):null}
                                    </Grid>
                                </Grid>

                            </Grid>
                        </Grid>
                    )
                })}
                <Grid item xs={12}>
                    <Grid container justifyContent='space-around'>
                        <Grid item xs='auto'>
                            <Grid container className={weeklyclass.button} justifyContent="center" onClick={()=>addstocks()}>
                                Add
                            </Grid>
                        </Grid>
                        <Grid item xs='auto'>
                            <Grid container className={weeklyclass.button} justifyContent="center" onClick={()=>save()}>
                                Save
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    ))
}
