import React, {useEffect, useState} from 'react'
import {Grid} from '@material-ui/core'
import { TextField, Select, MenuItem, FormControl, FormControlLabel, RadioGroup, Radio } from '@mui/material'
import cardStyle from '../../Resources/css/StockCards'
import useStyle from '../../Resources/css/Blog'
import Input from './InputComponent'
import LoadingIcon from '../../Resources/img/svg/Ripple'
import moment from 'moment'
import {useHistory, useLocation, useParams, Redirect} from 'react-router-dom'

import {addImage} from '../../../databases/Add'
import {addMonthlyAnalysis} from '../../../databases/Add'

export default function Monthly(props) {
  const menuItems = [
    {text:"Background",value:'bg'}, 
    {text:'Overview',value:'ov'},
    {text:'F Summary',value:'fs'},
    {text:'T Summary',value:'ts'},
    {text:'Evaluation',value:'ev'}]
  
  const [BackBase, setBgBase] = useState([])
  const [OverBase, setOvBase] = useState([])
  const [FSumBase, setFsBase] = useState([])
  const [TSumBase, setTsBase] = useState([])
  const [EvalBase, setEvBase] = useState([])
  const [SaveLoad, setSaveLoad] = useState(false)
  const [active,changeActive] = useState('bg')
  const history = useHistory()
  const cardclass = cardStyle()
  const classes = useStyle()
  const [join, setJoin] = useState({
    title:'',
    by: props.adminData.username,
    time: moment().format('D MMMM YYYY'),
    timestamp: null,
    background:{},
    overview:{},
    fSummary:{},
    tSummary:{},
    eval:{}
  })
  // tiap join data ada tab dewe2, jadi kudu dibagi ke beberapa file..
  // tiap section ntar ada session ama save button dewe2, jadi lek disave nde tab iku baru isi session e dicopy ke session join data, which in the monthly file... ====> changed 1 save button
  // sediain save all button juga ====> done
  // in the end, ada tombol buat upload, itu ngambil dri session join diupload ke firestore
  const [bgData, setBgData] = useState(
      {
          name:'',
          code:'',
          price:'',
          category:'',
      }
  )
  const [listItem, setListItem] = useState([])
  const itemType = [
    {type:'bold1', label:'Title 1'},
    {type:'bold2', label:'Title 2'},
    {type:'text',label:'Paragraph'},
    {type:'pict', label:'Insert Picture'},
  ]

  function updateJoin(props,e){
    const newValue = {...join}
    newValue[props] = e.target.value 
    setJoin(newValue)
  }
  function updateBg(props,e){
    const newValue = {...bgData}
    newValue[props] = e.target.value 
    setBgData(newValue)
  }
  function updateJoinText(lang, e){
      const newValue = {...bgData}
      if(lang==='id'){
          newValue.text.id = e.target.value 
      }else{
          newValue.text.en = e.target.value 
      }
      setBgData(newValue)
  }
  function changeBase(value){
    changeActive(value)
    let newArr = []
    if(value=='bg'){
      newArr = [...BackBase]
    }else if(value=='ov'){
      newArr = [...OverBase]
    }else if(value=='fs'){
      newArr = [...FSumBase]
    }else if(value=='ts'){
      newArr = [...TSumBase]
    }else if(value=='ev'){
      newArr = [...EvalBase]
    }
    setListItem(newArr)
  }

  function addItem(type){
    const newArr = [...listItem,{itemType:type,value:(type=='pict')?null:{en:null,id:null}}]
    setListItem(newArr)
    UpdateBase(newArr)
  }

  function deleteItem(index){
    console.log(index)
    let newArr = []
    if(active=='bg'){
      newArr=[...BackBase]
      newArr.splice(index,1)
      console.log(newArr)
      setBgBase(newArr)
    }else if(active=='ov'){
      newArr=[...OverBase]
      newArr.splice(index,1)
      console.log(newArr)
      setOvBase(newArr)
    }else if(active=='fs'){
      newArr=[...FSumBase]
      newArr.splice(index,1)
      console.log(newArr)
      setFsBase(newArr)
    }else if(active=='ts'){
      newArr=[...TSumBase]
      newArr.splice(index,1)
      console.log(newArr)
      setTsBase(newArr)
    }else if(active=='ev'){
      newArr=[...EvalBase]
      newArr.splice(index,1)
      console.log(newArr)
      setEvBase(newArr)
    }
    setListItem(newArr)
  }

  function changeValue(index,newValue,lg=null){
    // console.log(`change value ${index} - ${lg} : ${newValue}`)
    let newArr = []
    if(active=='bg'){
      newArr = [...BackBase]
      if (lg){
        newArr[index].value[lg] = newValue
      }else{
        newArr[index].value = newValue
      }
      setBgBase(newArr)
    }else if(active=='ov'){
      newArr = [...OverBase]
      if (lg){
        newArr[index].value[lg] = newValue
      }else{
        newArr[index].value = newValue
      }
      setOvBase(newArr)
    }else if(active=='fs'){
      newArr = [...FSumBase]
      if (lg){
        newArr[index].value[lg] = newValue
      }else{
        newArr[index].value = newValue
      }
      setFsBase(newArr)
    }else if(active=='ts'){
      newArr = [...TSumBase]
      if (lg){
        newArr[index].value[lg] = newValue
      }else{
        newArr[index].value = newValue
      }
      setTsBase(newArr)
    }else if(active=='ev'){
      newArr = [...EvalBase]
      if (lg){
        newArr[index].value[lg] = newValue
      }else{
        newArr[index].value = newValue
      }
      setEvBase(newArr)
    }
    setListItem(newArr)
  }

  function UpdateBase(arr){
    if(active=='bg'){
      setBgBase(arr)
    }else if(active=='ov'){
      setOvBase(arr)
    }else if(active=='fs'){
      setFsBase(arr)
    }else if(active=='ts'){
      setTsBase(arr)
    }else if(active=='ev'){
      setEvBase(arr)
    }
  }
  
  async function UploadPict(Arr1){
    const newObject = await Promise.all(Arr1.map(async obj =>{
      if (obj.itemType ==='pict'){
          const newurl = await addImage(obj.value,'post/monthly')
          obj.value = newurl
      }
        console.log(obj)
        return obj
    }))
    console.log(newObject)
  }

  async function Save(){
    setSaveLoad(true)
    console.log(SaveLoad)
    await UploadPict(BackBase)
    await UploadPict(OverBase)
    await UploadPict(FSumBase)
    await UploadPict(TSumBase)
    await UploadPict(EvalBase)
    let newData = {
      ...join,
      basic:{...bgData},
      background:[...BackBase],
      overview:[...OverBase],
      fSummary:[...FSumBase],
      tSummary:[...TSumBase],
      eval:[...EvalBase]
    }
    setJoin(newData)
    console.log(newData)


    addMonthlyAnalysis(newData).then((response)=>{
      history.go(0)
      console.log(response)
    })
  }

  useEffect(()=>{
    console.log(join)
  },[join])
  
  return (
    <Grid container justifyContent='center'>
      <Grid item xs={11}>
        <Grid container justifyContent='space-between'>
            <Grid item md={1} xs={3}>
              <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                  Title:
              </Grid>
            </Grid>
            <Grid item md={7} xs={9}>
              <Grid container className={cardclass.singleKeyValue}>
                  <TextField maxRow={3} fullWidth value={join.title} onChange={(event)=>updateJoin('title',event)}/>
              </Grid>
            </Grid>
            <Grid item md={2} xs={6}>
              <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                  {join.time}
              </Grid>
            </Grid>
            <Grid item md={2} xs={6}>
              <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                  By {join.by}
              </Grid>
            </Grid>
        </Grid>
        <Grid container justifyContent='space-between'>
            <Grid item md={1} xs={3}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                    Name:
                </Grid>
            </Grid>
            <Grid item md={2} xs={9}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                    <TextField multiline maxRow={3} fullWidth value={bgData.name} onChange={(event)=>updateBg('name',event)}/>
                </Grid>
            </Grid>
            <Grid item md={1} xs={3}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                    Code:
                </Grid>
            </Grid>
            <Grid item md={2} xs={9}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                    <TextField multiline maxRow={3} fullWidth value={bgData.code} onChange={(event)=>updateBg('code',event)}/>
                </Grid>
            </Grid>
            <Grid item md={1} xs={3}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                    Price:
                </Grid>
            </Grid>
            <Grid item md={2} xs={9}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                    <TextField multiline maxRow={3} fullWidth value={bgData.price} onChange={(event)=>updateBg('price',event)}/>
                </Grid>
            </Grid>
            <Grid item md={1} xs={3}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                    Category:
                </Grid>
            </Grid>
            <Grid item md={2} xs={9}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                    <TextField multiline maxRow={3} fullWidth value={bgData.category} onChange={(event)=>updateBg('category',event)}/>
                </Grid>
            </Grid>
        </Grid>
        <Grid container direction='row'>
          <Grid item md={1} xs={3} className={classes.contentSection}>
            {menuItems.map((menu)=>{
              return(
                <Grid container justifyContent='flex-end'>
                  <Grid container justifyContent='center' alignContent='center' onClick={()=>{changeBase(menu.value)}} className={classes.optionButton}>
                    {menu.text}
                  </Grid>
                </Grid>
              )
            })}
            <Grid container justifyContent='center' alignContent='center' onClick={()=>{Save()}} className={classes.saveButton}>
              {SaveLoad?<LoadingIcon />:'Save'}
            </Grid>
          </Grid>
          <Grid item xs className={[classes.contentSection,classes.middleSection]}>
            <Grid container justifyContent='center' className={classes.headIndi}>
              {active=='bg'?(
                'Background'
              ):active=='ov'?(
                'Overview'
              ):active=='fs'?(
                'Financial Summary'
              ):active=='ts'?(
                'Tech Summary'
              ):(
                'Evaluation'
              )}

            </Grid>
            {listItem.map((item,index)=>{
              return(
                <Input 
                  key={index}
                  type={item.itemType} 
                  value={item.value} 
                  index={index}
                  update={changeValue}
                  delete={deleteItem}
                />
              )
            })}
          </Grid>
          <Grid item md={1} xs={3} className={classes.contentSection}>
            {itemType.map((item)=>{
              return(
                  <Grid container justifyContent='center' alignContent='center' onClick={()=>{addItem(item.type)}} className={classes.optionButton}> 
                    {item.label} 
                  </Grid>
              )
            })}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}
