import React from 'react'
import {TextField, Grid} from '@mui/material'
import { FaTrashAlt as Trash} from 'react-icons/fa'
import useStyle from '../../Resources/css/Monthly'

export default function InputComponent(props) {
    const classes = useStyle()
    return (
        props.type=='text'?(

            <Grid container justifyContent='center' className={classes.itemBorder}>
                <Grid item xs={11} className={classes.itemTitle}>
                    Paragraph text
                </Grid>
                <Grid item xs={1}>
                    <Grid container justifyContent='center' alignContent='center' alignItems='center'>
                        <Trash className={classes.trashcan} onClick={()=>{props.delete(props.index)}}/>
                    </Grid>
                </Grid>
                <Grid item md={1} xs={2}>
                    <Grid container justifyContent='center'>
                        En
                    </Grid>
                </Grid>
                <Grid item md={5} xs={10}>
                    <TextField multiline value={props.value.en} className={classes.fullWidth} onChange={(e)=>{props.update(props.index,e.target.value,'en')}}>{props.value.en}</TextField>
                </Grid>
                <Grid item md={1} xs={2}>
                    <Grid container justifyContent='center'>
                        Id
                    </Grid>
                </Grid>
                <Grid item md={5} xs={10}>
                    <TextField multiline value={props.value.id} className={classes.fullWidth} onChange={(e)=>{props.update(props.index,e.target.value,'id')}}>{props.value.en}</TextField>
                </Grid>
            </Grid>

        ):props.type=='pict'?(
            props.value?(

                <Grid container justifyContent='center' className={classes.itemBorder}>
                    <Grid item xs={11}>
                        <input type='file' onChange={(e)=>{props.update(props.index,e.target.files[0])}}/>
                        <Grid container justifyContent='center' className={classes.imageUploadPreview}>
                            {console.log(props.value)}
                            <img src={props.value.name?URL.createObjectURL(props.value):props.value} className={classes.image}/>
                        </Grid>
                        {/* test */}
                    </Grid>
                    <Grid item xs={1}>
                        <Trash className={classes.trashcan} onClick={()=>{props.delete(props.index)}}/>
                    </Grid>
                </Grid>

            ):(

                <Grid container justifyContent='center' className={classes.itemBorder}>
                    <Grid item xs>
                        <input type='file' onChange={(e)=>{props.update(props.index,e.target.files[0])}}/>
                    </Grid>
                    <Grid item xs={1}>
                        <Trash className={classes.trashcan} onClick={()=>{props.delete(props.index)}}/>
                    </Grid>
                </Grid>

            )
        ):props.type=='bold1'?(

            <Grid container justifyContent='center' className={classes.itemBorder}>
                <Grid item xs={11} className={classes.itemTitle}>
                    Title
                </Grid>
                <Grid item xs={1}>
                    <Grid container justifyContent='center' alignContent='center' alignItems='center'>
                        <Trash className={classes.trashcan} onClick={()=>{props.delete(props.index)}}/>
                    </Grid>
                </Grid>
                <Grid item md={1} xs={2}>
                    <Grid container justifyContent='center'>
                        En
                    </Grid>
                </Grid>
                <Grid item md={5} xs={10}>
                    <TextField multiline value={props.value.en} className={classes.fullWidth} onChange={(e)=>{props.update(props.index,e.target.value,'en')}}>{props.value.en}</TextField>
                </Grid>
                <Grid item md={1} xs={2}>
                    <Grid container justifyContent='center'>
                        Id
                    </Grid>
                </Grid>
                <Grid item md={5} xs={10}>
                    <TextField multiline value={props.value.id} className={classes.fullWidth} onChange={(e)=>{props.update(props.index,e.target.value,'id')}}>{props.value.en}</TextField>
                </Grid>
            </Grid>

        ):(

            <Grid container justifyContent='center' className={classes.itemBorder}>
                <Grid item xs={11} className={classes.itemTitle}>
                    Subtitle
                </Grid>
                <Grid item xs={1}>
                    <Grid container justifyContent='center' alignContent='center' alignItems='center'>
                        <Trash className={classes.trashcan} onClick={()=>{props.delete(props.index)}}/>
                    </Grid>
                </Grid>
                <Grid item md={1} xs={2}>
                    <Grid container justifyContent='center'>
                        En
                    </Grid>
                </Grid>
                <Grid item md={5} xs={10}>
                    <TextField multiline value={props.value.en} className={classes.fullWidth} onChange={(e)=>{props.update(props.index,e.target.value,'en')}}>{props.value.en}</TextField>
                </Grid>
                <Grid item md={1} xs={2}>
                    <Grid container justifyContent='center'>
                        Id
                    </Grid>
                </Grid>
                <Grid item md={5} xs={10}>
                    <TextField multiline value={props.value.id} className={classes.fullWidth} onChange={(e)=>{props.update(props.index,e.target.value,'id')}}>{props.value.en}</TextField>
                </Grid>
            </Grid>
            
        )
    )
}
