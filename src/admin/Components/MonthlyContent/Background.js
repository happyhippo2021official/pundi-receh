import React, {useEffect, useState} from 'react'
import {Grid} from '@material-ui/core'
import { TextField, Select, MenuItem, FormControl, FormControlLabel, RadioGroup, Radio } from '@mui/material'
import cardStyle from '../../Resources/css/StockCards'
import useStyle from '../../Resources/css/Weekly'
import Input from './inputComponent'
import moment from 'moment'
import {useHistory, useLocation, useParams, Redirect} from 'react-router-dom'

import {addImage} from '../../../databases/Add'
import {addWeeklyAnalysis} from '../../../databases/Add'
import {GetOneAnalysis} from '../../../databases/Read'
import {UpdateAnalysis} from '../../../databases/Update'
import { FaTrashAlt as Trash} from 'react-icons/fa'


export default function Background() {
  let datum = {}
  const [loading, setLoad] = useState(true)
  const history = useHistory()
  const cardclass = cardStyle()
  const weeklyclass = useStyle()
  const [joinData, setJoinData] = useState(
      {
          title:'',
          time: moment().format('D MMMM YYYY'),
          name:'',
          code:'',
          price:'',
          category:'',

      }
  )
  const [listItem, setListItem] = useState([])
  const itemType = [
    {type:'bold1', label:'Title'},
    {type:'bold2', label:'Title'},
    {type:'text',label:'Text Box'},
    {type:'pict', label:'Insert Picture'},
  ]
  const defaultstock = {}
  const [stocks,setStocks] = useState([defaultstock])
  const [pImage,setPImage] = useState([null])

  function updatejoin(props,e){
    const newValue = {...joinData}
    newValue[props] = e.target.value 
    setJoinData(newValue)
  }
  function updateJoinText(lang, e){
      const newValue = {...joinData}
      if(lang==='id'){
          newValue.text.id = e.target.value 
      }else{
          newValue.text.en = e.target.value 
      }
      setJoinData(newValue)
  }

  function addItem(type){
    const newArr = [...listItem,{type:type,value:null}]
    console.log(newArr)
    console.log(joinData)
    setListItem(newArr)
  }
  
  return (
    <Grid container justifyContent='center'>
      <Grid item xs={11}>
        <Grid container justifyContent='space-between'>
            <Grid item md={1} xs={3}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                    Title:
                </Grid>
            </Grid>
            <Grid item md={8} xs={9}>
                <Grid container className={cardclass.singleKeyValue}>
                    <TextField multiline maxRow={3} fullWidth value={joinData.title} onChange={(event)=>updatejoin('title',event)}/>
                </Grid>
            </Grid>
            <Grid item md={3} xs={12}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                    {joinData.time}
                </Grid>
            </Grid>
        </Grid>
        <Grid container justifyContent='space-between'>
            <Grid item xs={1}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                    Name:
                </Grid>
            </Grid>
            <Grid item xs={2}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                    <TextField multiline maxRow={3} fullWidth value={joinData.name} onChange={(event)=>updatejoin('name',event)}/>
                </Grid>
            </Grid>
            <Grid item xs={1}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                    Code:
                </Grid>
            </Grid>
            <Grid item xs={2}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                    <TextField multiline maxRow={3} fullWidth value={joinData.code} onChange={(event)=>updatejoin('code',event)}/>
                </Grid>
            </Grid>
            <Grid item xs={1}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                    Price:
                </Grid>
            </Grid>
            <Grid item xs={2}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                    <TextField multiline maxRow={3} fullWidth value={joinData.price} onChange={(event)=>updatejoin('price',event)}/>
                </Grid>
            </Grid>
            
            <Grid item xs={1}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKey}>
                    Category:
                </Grid>
            </Grid>
            <Grid item xs={2}>
                <Grid container justifyContent='center' alignItems='center' className={cardclass.singleKeyValue}>
                    <TextField multiline maxRow={3} fullWidth value={joinData.category} onChange={(event)=>updatejoin('category',event)}/>
                </Grid>
            </Grid>
        </Grid>
        <Grid container direction='row'>
          <Grid item xs>
            {listItem.map((item)=>{
              return(
                <Input type={item.type} value={item.value}/>
              )
            })}
          </Grid>
          <Grid item md={2} xs={3}>
            {itemType.map((item)=>{
              return(
                <Grid container justifyContent='center'>
                  <button onClick={()=>{addItem(item.type)}}>{item.label}</button>
                </Grid>
              )
            })}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}
