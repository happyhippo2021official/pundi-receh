import React, { useState} from 'react'
import {ThemeProvider} from '@material-ui/core'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import customTheme from './customTheme'
import AdminPage from './admin/Routing'
import UserRouting from './UserRouting'

function App() {
  return (
    <ThemeProvider theme={customTheme} >
      <BrowserRouter>
        <Switch>
          <Route path='/admin' component={AdminPage} />
          <Route path='/' component={UserRouting} />
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
