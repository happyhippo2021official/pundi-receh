import { createTheme } from '@material-ui/core/Styles'
import { grey, teal, blue } from '@material-ui/core/colors'
import color from './resources/css/Color'


const customTheme = createTheme({
    palette:{
        primary:{
            main: color.white,
        },
        secondary:{
            main: teal[400],
        },
        success:{
            main: blue[400],
            color: grey[50],
            hover: blue[700]
        },
        // background:{
        //     default: grey[900]
        // },
        // text:{
        //     primary:'white'
        // }
    },
    typography: {
        fontFamily: ["AvenirMedium",'Playfair'].join(','), // specifying a new font
    },
    overrides:{
        MuiButton:{
            text:{
                color: 'white'
            }
        }
    }
})

export default customTheme;