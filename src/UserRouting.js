import React, {useState, useEffect} from 'react'
import './App.css';
import './App.scss';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {Grid} from '@material-ui/core'
import customStyles from './resources/css/Styles'


import Header from './layout/Header'
import TickerTape  from './components/TickerTape/TickerTape';
import ScrollButton from './components/ScrollButton'
import Footer from './layout/Footer'
import SignPage from './components/Sign/SignPage'
import VerifyEmailPage from './components/Sign/VerifyEmail'
import PlaylistPage from './components/Playlist/Playlist';
import DetailAnalysisPage from './components/Playlist/DetailAnalysis';
import ChecklistPage from './components/Checklist/Checklist'
import WeeklyPage from './components/Weekly/Weekly'
import AboutusPage from './components/Aboutus/Aboutus'
import MyprofilePage from './components/Myprofile/Myprofile'
import HeatmapPage from './components/HeatmapList/HeatmapList'
import HeatmapForexPage from './components/HeatmapForexList/HeatmapForexList'
import DashPage from './pages/Dashboard'
import Blog from './pages/Blog'
import Podcast from './pages/Podcast'
import Comic from './pages/Comic'
import DetailPage from './components/Detail'
import Paypal from './components/Paypal/Paypal'
import {ReadMemberUser} from './databases/Read'

export default function Routing() {
  const [signState, setSign] = useState({value:'1'});
  const [token, setToken] = useState(false);
  const [mobileView, setMobileView] = useState();
  const [drawerOpen, setDrawerOpen] = useState(false);
  const useStyle = customStyles();
  const { tableRef, isSticky } = TickerTape();
  useEffect(() => {
    const setResponsiveness = () => {
      return window.innerWidth < 900
        ? setMobileView(true)
        : setMobileView(false)
    };

    setResponsiveness();
    window.addEventListener("resize", () => setResponsiveness());

    return () => {
      window.removeEventListener("resize", () => setResponsiveness());
    }
  }, []);
  const setSignState = (setNewValue) => {
    setSign({...signState, value:setNewValue})
  }
  return (
    <BrowserRouter>
      <Grid container className={useStyle.globalContainer}>
        <Grid item xs={12}>
          <Header token={token} drawerOpen={drawerOpen} mobileView={mobileView} setDrawerOpen={setDrawerOpen} setToken={setToken}/>
        </Grid>
        <Grid item xs={12}>
          <TickerTape />
        </Grid>
        <Grid item xs={12}>
          <Grid container className={useStyle.contentContainer}>
            <Switch>
              <Route path='/detail/:id' component={DetailPage} />
              <Route path='/login'>
                {token ? <DashPage /> :<SignPage page='in' token={token} setToken={setToken}/>}
              </Route>
              <Route path='/signup'>
                <SignPage page='up' token={token} setToken={setToken}/>
              </Route>
              <Route path='/signin'>
                <SignPage page='in' token={token} setToken={setToken}/>
              </Route>
              <Route path='/playlist' component={PlaylistPage} />
              <Route path='/analysis/:id' component={DetailAnalysisPage} />
              <Route path='/checkList' component={ChecklistPage} />
              <Route path='/weekly' component={WeeklyPage} />
              <Route path='/aboutus' component={AboutusPage} />
              <Route path='/myprofile' component={MyprofilePage} />
              <Route path='/heatmap' component={HeatmapPage} />
              <Route path='/heatmapForex' component={HeatmapForexPage} />
              <Route path='/blog/:id' component={Blog} />
              <Route path='/blog' component={Blog} />
              <Route path='/podcast' component={Podcast} />
              <Route path='/comic' component={Comic} />
              <Route path='/paypal' component={Paypal} />
              <Route path='/verifyemail/:id' component={VerifyEmailPage} />
              <Route path='/' component={DashPage} token={token}/>
            </Switch>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Footer />
        </Grid>
        <ScrollButton />
      </Grid>
    </BrowserRouter>
  );
}
