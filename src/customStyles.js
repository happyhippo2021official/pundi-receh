import { makeStyles } from '@material-ui/core/styles'


const customStyles = makeStyles(theme=>({
    headerStyle:{
        minHeight:'8vh',
        '&.scrolled':{
            opacity: '0',
            transition: 'all 1s ease',
            '&:hover':{
                opacity: '1',
            }
        },
        background: 'linear-gradient(to right bottom, #3d0075, #1c0008)'
    },
    headerButtonGroupBig:{
        [theme.breakpoints.down('sm')]: {
            display: 'none',
        },
    },
    mobileMenuItem:{
        color: 'black',
        width: '20vw',
        minWidth: '200px',
        maxWidth: '350px',
        height: '10vh',
    },
    imageLogo:{
        height: '100%',
        width: '75px',
        margin: '5px 10px 5px 10px',
    },
    toolBarTitle:{
        flexGrow:1,
    },
   cardMedia:{
       height:'20vh'
   } 
}))

export default customStyles